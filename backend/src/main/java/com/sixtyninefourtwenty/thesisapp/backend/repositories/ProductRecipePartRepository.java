package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRecipePartRepository extends CrudRepository<ProductRecipePartEntity, Long> {

    @Query(
            value = "SELECT * FROM product_recipe_parts WHERE product_id = :id",
            nativeQuery = true
    )
    Iterable<ProductRecipePartEntity> getRecipeForProductId(@Param("id") long productId);

    default Iterable<ProductRecipePartEntity> getRecipeForProduct(ProductEntity product) {
        return getRecipeForProductId(product.id());
    }

    @Query(
            value = "SELECT * FROM product_recipe_parts WHERE material_id = :id",
            nativeQuery = true
    )
    Iterable<ProductRecipePartEntity> getRecipePartsContainingMaterialId(@Param("id") long materialId);

    default Iterable<ProductRecipePartEntity> getRecipePartsContainingMaterial(MaterialEntity material) {
        return getRecipePartsContainingMaterialId(material.id());
    }

}
