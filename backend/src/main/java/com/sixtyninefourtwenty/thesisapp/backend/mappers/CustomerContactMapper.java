package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerContactEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CustomerContactMapper {

    CustomerContactEntity toCustomerContact(CustomerContact customerContactDto);

    CustomerContact fromCustomerContact(CustomerContactEntity customerContact);

}
