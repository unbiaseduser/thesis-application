package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.ProductSaleMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductSaleService;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class ProductSaleController {

    private final ProductSaleService productSaleService;
    private final ProductSaleMapper productSaleMapper;

    @GetMapping(path = "/productsales")
    public List<ProductSale> getSalesForOneOrAllProducts(@RequestParam(required = false) Long productId) {
        final var sales = productId != null ? productSaleService.getSalesForProductId(productId) :
                productSaleService.findAll();
        return StreamSupport.stream(sales.spliterator(), false)
                .map(productSaleMapper::fromProductSale)
                .toList();
    }

    @GetMapping(path = "/productsales/{id}")
    public ResponseEntity<ProductSale> findById(@PathVariable("id") long id) {
        return productSaleService.findById(id)
                .map(sale -> new ResponseEntity<>(productSaleMapper.fromProductSale(sale), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/productsales")
    public ResponseEntity<Result<ProductSale>> tryCreate(@RequestBody ProductSale productSale) {
        final var entity = productSaleMapper.toProductSale(productSale);
        final var probablySavedEntity = productSaleService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<ProductSaleEntity> s -> new ResponseEntity<>(new Result.Success<>(productSaleMapper.fromProductSale(s.data())), HttpStatus.CREATED);
            case Result.Rejection<ProductSaleEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @PutMapping(path = "/productsales/{id}")
    public ResponseEntity<Result<ProductSale>> tryFullUpdate(@PathVariable("id") long id, @RequestBody ProductSale productSale) {
        if (!productSaleService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalEntity = productSale.withId(id);
        final var entity = productSaleMapper.toProductSale(finalEntity);
        final var probablySavedEntity = productSaleService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<ProductSaleEntity> s -> new ResponseEntity<>(new Result.Success<>(productSaleMapper.fromProductSale(s.data())), HttpStatus.OK);
            case Result.Rejection<ProductSaleEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @DeleteMapping(path = "/productsales/{id}")
    public ResponseEntity<ProductSale> delete(@PathVariable("id") long id) {
        productSaleService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
