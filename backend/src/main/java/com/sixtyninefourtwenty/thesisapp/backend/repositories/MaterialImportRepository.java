package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MaterialImportRepository extends CrudRepository<MaterialImportEntity, Long> {

    @Query(
            value = "SELECT * FROM material_imports WHERE material_id = :id",
            nativeQuery = true
    )
    Iterable<MaterialImportEntity> getImportsForMaterialId(@Param("id") long materialId);

    @Query(
            value = "SELECT SUM(quantity) FROM material_imports WHERE material_id = :id",
            nativeQuery = true
    )
    Optional<Long> getTotalImportQuantityForMaterialIdAsOptional(@Param("id") long materialId);

    default long getTotalImportQuantityForMaterialId(long materialId) {
        return getTotalImportQuantityForMaterialIdAsOptional(materialId).orElse(0L);
    }

    default long getTotalImportQuantityForMaterial(MaterialEntity material) {
        return getTotalImportQuantityForMaterialId(material.id());
    }

}
