package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierContactEntity;

import java.util.Optional;

public interface MaterialSupplierContactService {

    boolean existsById(Long id);

    Iterable<MaterialSupplierContactEntity> findAll();

    Iterable<MaterialSupplierContactEntity> getContactsForSupplierId(long supplierId);

    Optional<MaterialSupplierContactEntity> findById(Long id);

    MaterialSupplierContactEntity save(MaterialSupplierContactEntity materialSupplierContact);

    void delete(MaterialSupplierContactEntity materialSupplierContact);

    void deleteById(Long id);

}
