package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerContactEntity;

import java.util.Optional;

public interface CustomerContactService {

    boolean existsById(Long id);

    Iterable<CustomerContactEntity> getContactsForCustomerId(long customerId);

    Iterable<CustomerContactEntity> findAll();

    Optional<CustomerContactEntity> findById(Long id);

    CustomerContactEntity save(CustomerContactEntity customerContact);

    void delete(CustomerContactEntity customerContact);

    void deleteById(Long id);

}
