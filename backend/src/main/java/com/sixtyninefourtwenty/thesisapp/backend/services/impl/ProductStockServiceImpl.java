package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductStockService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductCreationRepository;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductSaleRepository;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Log
public class ProductStockServiceImpl implements ProductStockService {

    private final ProductCreationRepository productCreationRepository;
    private final ProductSaleRepository productSaleRepository;

    @Override
    public long calculateStockForProductId(long productId) {
        final var totalCreationQuantity = productCreationRepository.getTotalCreationQuantityForProductId(productId);
        final var totalSalesQuantity = productSaleRepository.getTotalSalesQuantityForProductId(productId);
        return totalCreationQuantity - totalSalesQuantity;
    }
}
