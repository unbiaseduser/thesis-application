package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

@Entity
@Table(name = "material_supplier_contacts")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialSupplierContactEntity {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "supplier_id", nullable = false)
    private @NonNull MaterialSupplierEntity supplier;

    @Column(name = "phone_number")
    private @Nullable String phoneNumber;

    private @Nullable String email;

    @Column(nullable = false)
    private @Nullable String address;

    @Default
    public MaterialSupplierContactEntity(
            long id, @NonNull
    MaterialSupplierEntity supplier, @Nullable
            String phoneNumber, @Nullable
            String email, @Nullable
            String address) {
        if (phoneNumber == null && email == null && address == null) {
            throw new IllegalArgumentException("One of the contact info properties must be present");
        }
        this.id = id;
        this.supplier = supplier;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }

    public long id() {
        return id;
    }

    public @NonNull MaterialSupplierEntity supplier() {
        return supplier;
    }

    public @Nullable String phoneNumber() {
        return phoneNumber;
    }

    public @Nullable String email() {
        return email;
    }

    public @Nullable String address() {
        return address;
    }

}
