package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

import java.time.LocalDateTime;

@Entity
@Table(name = "material_imports")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialImportEntity {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private @Nullable MaterialSupplierEntity supplier;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "material_id", nullable = false)
    private @NonNull MaterialEntity material;

    private int quantity;

    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private @NonNull LocalDateTime time;

    @Default
    public MaterialImportEntity(
            long id,
            @Nullable MaterialSupplierEntity supplier,
            @NonNull MaterialEntity material,
            int quantity,
            @NonNull LocalDateTime time) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must not be <= 0");
        }
        this.id = id;
        this.supplier = supplier;
        this.material = material;
        this.quantity = quantity;
        this.time = time;
    }

    public MaterialImportEntity(long id, MaterialSupplierEntity supplier, MaterialEntity material, int quantity) {
        this(id, supplier, material, quantity, LocalDateTime.now());
    }

    public long id() {
        return id;
    }

    public @Nullable MaterialSupplierEntity supplier() {
        return supplier;
    }

    public @NonNull MaterialEntity material() {
        return material;
    }

    public int quantity() {
        return quantity;
    }

    public @NonNull LocalDateTime time() {
        return time;
    }

}
