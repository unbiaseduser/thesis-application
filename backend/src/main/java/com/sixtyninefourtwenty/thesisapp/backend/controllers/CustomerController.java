package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.CustomerMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerMapper customerMapper;

    @GetMapping(path = "/customers")
    public List<Customer> findAll() {
        return StreamSupport.stream(customerService.findAll().spliterator(), false)
                .map(customerMapper::fromCustomer)
                .toList();
    }

    @GetMapping(path = "/customers/{id}")
    public ResponseEntity<Customer> findById(@PathVariable("id") long id) {
        return customerService.findById(id)
                .map(customer -> new ResponseEntity<>(customerMapper.fromCustomer(customer), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/customers")
    public ResponseEntity<Customer> create(@RequestBody Customer customer) {
        final var customerEntity = customerMapper.toCustomer(customer);
        final var savedCustomerEntity = customerService.save(customerEntity);
        return new ResponseEntity<>(customerMapper.fromCustomer(savedCustomerEntity), HttpStatus.CREATED);
    }

    @PutMapping(path = "/customers/{id}")
    public ResponseEntity<Customer> fullUpdate(@PathVariable("id") long id, @RequestBody Customer customer) {
        if (!customerService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalCustomer = customer.withId(id);
        final var customerEntity = customerMapper.toCustomer(finalCustomer);
        final var savedCustomerEntity = customerService.save(customerEntity);
        return new ResponseEntity<>(customerMapper.fromCustomer(savedCustomerEntity), HttpStatus.OK);
    }

    @DeleteMapping(path = "/customers/{id}")
    public ResponseEntity<Customer> delete(@PathVariable("id") long id) {
        customerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
