package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.CustomerService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Override
    public boolean existsById(Long id) {
        return customerRepository.existsById(id);
    }

    @Override
    public Iterable<CustomerEntity> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Optional<CustomerEntity> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public CustomerEntity save(CustomerEntity customer) {
        return customerRepository.save(customer);
    }

    @Override
    public void delete(CustomerEntity customer) {
        customerRepository.delete(customer);
    }

    @Override
    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }
}
