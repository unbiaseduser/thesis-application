package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerContactEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerContactRepository extends CrudRepository<CustomerContactEntity, Long> {

    @Query(
            value = "SELECT * FROM customer_contacts WHERE customer_id = :id",
            nativeQuery = true
    )
    Iterable<CustomerContactEntity> getContactsForCustomerId(@Param("id") long id);

    default Iterable<CustomerContactEntity> getContactsForCustomer(CustomerEntity customer) {
        return getContactsForCustomerId(customer.id());
    }

}
