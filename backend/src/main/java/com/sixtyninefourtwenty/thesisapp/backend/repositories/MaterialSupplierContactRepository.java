package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierContactEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialSupplierContactRepository extends CrudRepository<MaterialSupplierContactEntity, Long> {

    @Query(
            value = "SELECT * FROM material_supplier_contacts WHERE supplier_id = :id",
            nativeQuery = true
    )
    Iterable<MaterialSupplierContactEntity> getContactsForSupplierId(@Param("id") long id);

    default Iterable<MaterialSupplierContactEntity> getContactsForSupplier(MaterialSupplierEntity supplier) {
        return getContactsForSupplierId(supplier.id());
    }

}
