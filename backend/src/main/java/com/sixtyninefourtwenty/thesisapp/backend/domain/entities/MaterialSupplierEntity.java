package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;

@Entity
@Table(name = "material_suppliers")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(onConstructor_ = {@Default})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MaterialSupplierEntity {

    @Id
    @GeneratedValue
    private long id;

    private @NonNull String name;

    public long id() {
        return id;
    }

    public @NonNull String name() {
        return name;
    }

}
