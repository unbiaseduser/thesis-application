package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductCreationRepository extends CrudRepository<ProductCreationEntity, Long> {

    @Query(
            value = "SELECT * FROM product_creations WHERE product_id = :id",
            nativeQuery = true
    )
    Iterable<ProductCreationEntity> getCreationsForProductId(@Param("id") long productId);

    default Iterable<ProductCreationEntity> getCreationsForProduct(ProductEntity product) {
        return getCreationsForProductId(product.id());
    }

    @Query(
            value = "SELECT SUM(quantity) FROM product_creations WHERE product_id = :id",
            nativeQuery = true
    )
    Optional<Long> getTotalCreationQuantityForProductIdAsOptional(@Param("id") long productId);

    default long getTotalCreationQuantityForProductId(long productId) {
        return getTotalCreationQuantityForProductIdAsOptional(productId).orElse(0L);
    }

    default long getTotalCreationQuantityForProduct(ProductEntity product) {
        return getTotalCreationQuantityForProductId(product.id());
    }

}
