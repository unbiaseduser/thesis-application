package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;

import java.util.Optional;

public interface ProductService {

    boolean existsById(Long id);

    Iterable<ProductEntity> findAll();

    Optional<ProductEntity> findById(Long id);

    ProductEntity save(ProductEntity product);

    void delete(ProductEntity product);

    void deleteById(Long id);

}
