package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MaterialImportMapper {

    MaterialImportEntity toMaterialImport(MaterialImport materialImportDto);

    MaterialImport fromMaterialImport(MaterialImportEntity materialImport);

}
