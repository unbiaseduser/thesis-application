package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerContactEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.CustomerContactService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.CustomerContactRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerContactServiceImpl implements CustomerContactService {

    private final CustomerContactRepository customerContactRepository;

    @Override
    public boolean existsById(Long id) {
        return customerContactRepository.existsById(id);
    }

    @Override
    public Iterable<CustomerContactEntity> getContactsForCustomerId(long customerId) {
        return customerContactRepository.getContactsForCustomerId(customerId);
    }

    @Override
    public Iterable<CustomerContactEntity> findAll() {
        return customerContactRepository.findAll();
    }

    @Override
    public Optional<CustomerContactEntity> findById(Long id) {
        return customerContactRepository.findById(id);
    }

    @Override
    public CustomerContactEntity save(CustomerContactEntity customerContact) {
        return customerContactRepository.save(customerContact);
    }

    @Override
    public void delete(CustomerContactEntity customerContact) {
        customerContactRepository.delete(customerContact);
    }

    @Override
    public void deleteById(Long id) {
        customerContactRepository.deleteById(id);
    }
}
