package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialSupplierService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.MaterialSupplierRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MaterialSupplierServiceImpl implements MaterialSupplierService {

    private final MaterialSupplierRepository materialSupplierRepository;

    @Override
    public boolean existsById(Long id) {
        return materialSupplierRepository.existsById(id);
    }

    @Override
    public Iterable<MaterialSupplierEntity> findAll() {
        return materialSupplierRepository.findAll();
    }

    @Override
    public Optional<MaterialSupplierEntity> findById(Long id) {
        return materialSupplierRepository.findById(id);
    }

    @Override
    public MaterialSupplierEntity save(MaterialSupplierEntity materialSupplier) {
        return materialSupplierRepository.save(materialSupplier);
    }

    @Override
    public void delete(MaterialSupplierEntity materialSupplier) {
        materialSupplierRepository.delete(materialSupplier);
    }

    @Override
    public void deleteById(Long id) {
        materialSupplierRepository.deleteById(id);
    }
}
