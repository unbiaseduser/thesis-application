package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialStockService;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductCreationService;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductStockService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductCreationRepository;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductRecipePartRepository;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
@Log
public class ProductCreationServiceImpl implements ProductCreationService {

    private final ProductCreationRepository productCreationRepository;
    private final ProductRecipePartRepository productRecipePartRepository;
    private final MaterialStockService materialStockService;
    private final ProductStockService productStockService;

    @Override
    public boolean existsById(Long id) {
        return productCreationRepository.existsById(id);
    }

    @Override
    public Iterable<ProductCreationEntity> findAll() {
        return productCreationRepository.findAll();
    }

    @Override
    public Iterable<ProductCreationEntity> getCreationsForProductId(long id) {
        return productCreationRepository.getCreationsForProductId(id);
    }

    @Override
    public Optional<ProductCreationEntity> findById(Long id) {
        return productCreationRepository.findById(id);
    }

    @Override
    public Result<ProductCreationEntity> trySave(ProductCreationEntity productCreation) {
        if (!productCreationRepository.existsById(productCreation.id())) {
            //Adding new creation, check material stock
            final var productQuantity = productCreation.quantity();
            final var recipe = productRecipePartRepository.getRecipeForProduct(productCreation.product());
            if (!recipe.iterator().hasNext()) {
                log.log(Level.INFO, "Product " + productCreation.product() + " has no recipe.");
                return new Result.Rejection<>("Product " + productCreation.product().name() + " has no recipe.");
            }
            for (final var recipePart : recipe) {
                final var material = recipePart.material();
                final var materialStock = materialStockService.calculateStockForMaterial(material);
                final var totalMaterialsRequired = productQuantity * recipePart.materialQuantity();
                if (materialStock < totalMaterialsRequired) {
                    log.log(Level.INFO, "Not enough stock for material " + material);
                    return new Result.Rejection<>(String.format("""
                            Not enough stock for material %s.
                            Current stock: %d.
                            Stock required: %d.
                            """, material.name(), materialStock, totalMaterialsRequired));
                }
            }
        } else {
            //Updating existing creation
            final var existingCreation = productCreationRepository.findById(productCreation.id()).orElseThrow();
            if (existingCreation.product().equals(productCreation.product())) { //same product
                final var quantityDelta = existingCreation.quantity() - productCreation.quantity();
                if (quantityDelta < 0) { // increasing the number of products created, check material stock
                    final var extraQuantity = Math.abs(quantityDelta);
                    final var recipe = productRecipePartRepository.getRecipeForProduct(productCreation.product());
                    if (!recipe.iterator().hasNext()) {
                        log.log(Level.INFO, "Product " + productCreation.product() + " has no recipe.");
                        return new Result.Rejection<>("Product " + productCreation.product().id() + " has no recipe.");
                    }
                    for (final var recipePart : recipe) {
                        final var material = recipePart.material();
                        final var materialStock = materialStockService.calculateStockForMaterial(material);
                        final var totalMaterialsRequired = extraQuantity * recipePart.materialQuantity();
                        if (materialStock < totalMaterialsRequired) {
                            log.log(Level.INFO, "Not enough stock for material " + material);
                            return new Result.Rejection<>(String.format("""
                                    Not enough stock for material %s.
                                    Current stock: %d.
                                    Stock required to change: %d.
                                    """, recipePart.material().name(), materialStock, totalMaterialsRequired));
                        }
                    }
                } else { // reducing the number of products created, check product stock
                    final var productStock = productStockService.calculateStockForProduct(productCreation.product());
                    if (productStock < quantityDelta) {
                        return new Result.Rejection<>(String.format("""
                                Not enough stock for product %s.
                                Current stock: %d.
                                Stock required to change: %d.""", productCreation.product().name(), productStock, quantityDelta));
                    }
                }
            } else { //Changing product
                log.log(Level.INFO, "Changing product " + existingCreation.product() + " to " + productCreation.product());
                //First, just get an easy check out of the way and check stock of old product
                final var stockForOldProduct = productStockService.calculateStockForProduct(existingCreation.product());
                if (stockForOldProduct < existingCreation.quantity()) {
                    log.log(Level.INFO, "Not enough stock for old product " + existingCreation.product() +
                            ". Old product stock is " + stockForOldProduct);
                    return new Result.Rejection<>(String.format("""
                            Not enough stock for old product %s.
                            Current stock: %d.
                            Trying to delete: %d.""", existingCreation.product().name(), stockForOldProduct, existingCreation.quantity()));
                }

                //Get recipes for both products
                final var newProductRecipe = productRecipePartRepository.getRecipeForProduct(productCreation.product());
                if (!newProductRecipe.iterator().hasNext()) {
                    log.log(Level.INFO, "Product " + productCreation.product() + " has no recipe.");
                    return new Result.Rejection<>("Product " + productCreation.product().name() + " has no recipe.");
                }
                final var newProductRecipeList = StreamSupport.stream(newProductRecipe.spliterator(), false)
                        .toList();
                final var materialsNeededForNewProduct = StreamSupport.stream(newProductRecipe.spliterator(), false)
                        .map(ProductRecipePartEntity::material)
                        .toList();
                final var oldProductRecipe = productRecipePartRepository.getRecipeForProduct(existingCreation.product());
                if (!oldProductRecipe.iterator().hasNext()) {
                    log.log(Level.INFO, "Product " + existingCreation.product() + " has no recipe.");
                    return new Result.Rejection<>("Product " + existingCreation.product().name() + " has no recipe.");
                }
                final var oldProductRecipeList = StreamSupport.stream(oldProductRecipe.spliterator(), false)
                        .toList();
                final var materialsNeededForOldProduct = StreamSupport.stream(oldProductRecipe.spliterator(), false)
                        .map(ProductRecipePartEntity::material)
                        .toList();
                /*
                * If both products' recipes share a same material, check that material's stock vs difference between old
                * and new number required of that material. Else, just check the material's stock normally.
                */
                for (final var materialNeededForNewProduct : materialsNeededForNewProduct) {
                    if (materialsNeededForOldProduct.contains(materialNeededForNewProduct)) {
                        final var oldRecipePart = oldProductRecipeList.get(materialsNeededForOldProduct.indexOf(materialNeededForNewProduct));
                        final var newRecipePart = newProductRecipeList.get(materialsNeededForNewProduct.indexOf(materialNeededForNewProduct));
                        final var numOfMaterialsForOldCreation = oldRecipePart.materialQuantity() * existingCreation.quantity();
                        final var numOfMaterialsForNewCreation = newRecipePart.materialQuantity() * productCreation.quantity();
                        final var delta = numOfMaterialsForOldCreation - numOfMaterialsForNewCreation;
                        if (delta < 0) {
                            final var finalDelta = Math.abs(delta);
                            final var materialStock = materialStockService.calculateStockForMaterial(materialNeededForNewProduct);
                            if (materialStock < finalDelta) {
                                return new Result.Rejection<>(String.format("""
                                        Can't change product %s to %s - insufficient stock for material %s.
                                        Current stock: %d.
                                        Stock required: %d.
                                        """, existingCreation.product().name(), productCreation.product().name(), materialNeededForNewProduct.name(), materialStock, finalDelta));
                            }
                        }
                    } else {
                        final var newRecipePart = newProductRecipeList.get(materialsNeededForNewProduct.indexOf(materialNeededForNewProduct));
                        final var materialStock = materialStockService.calculateStockForMaterial(newRecipePart.material());
                        final var materialsRequired = productCreation.quantity() * newRecipePart.materialQuantity();
                        if (materialStock < materialsRequired) {
                            return new Result.Rejection<>(String.format("""
                                    Can't change product %s to %s - insufficient stock for material %s.
                                    Current stock: %d.
                                    Stock required: %d.
                                    """, existingCreation.product().name(), productCreation.product().name(), newRecipePart.material().name(), materialStock, materialsRequired));
                        }
                    }
                }
            }
        }
        return new Result.Success<>(productCreationRepository.save(productCreation));
    }

    @Override
    public Result<ProductCreationEntity> tryDelete(ProductCreationEntity productCreation) {
        return tryDeleteById(productCreation.id());
    }

    @Override
    public Result<ProductCreationEntity> tryDeleteById(Long id) {
        final var existingCreationOpt = productCreationRepository.findById(id);
        if (existingCreationOpt.isPresent()) {
            final var existingCreation = existingCreationOpt.orElseThrow();
            final var stock = productStockService.calculateStockForProduct(existingCreation.product());
            if (stock < existingCreation.quantity()) {
                return new Result.Rejection<>(String.format("""
                        Can't delete creation due to insufficient stock.
                        Current stock: %d
                        Trying to delete: %d""", stock, existingCreation.quantity()));
            } else {
                productCreationRepository.deleteById(id);
                return new Result.Success<>(null);
            }
        } else {
            productCreationRepository.deleteById(id);
            return new Result.Success<>(null);
        }
    }

}
