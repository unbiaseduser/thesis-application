package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

@Entity
@Table(name = "customer_contacts")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerContactEntity {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "customer_id", nullable = false)
    private @NonNull CustomerEntity customer;

    @Column(name = "phone_number")
    private @Nullable String phoneNumber;

    private @Nullable String email;

    private @Nullable String address;

    @Default
    public CustomerContactEntity(
            long id,
            @NonNull CustomerEntity customer,
            @Nullable String phoneNumber,
            @Nullable String email,
            @Nullable String address) {
        if (phoneNumber == null && email == null && address == null) {
            throw new IllegalArgumentException("One of the contact info properties must be present");
        }
        this.id = id;
        this.customer = customer;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }

    public long id() {
        return id;
    }

    public @NonNull CustomerEntity customer() {
        return customer;
    }

    public @Nullable String phoneNumber() {
        return phoneNumber;
    }

    public @Nullable String email() {
        return email;
    }

    public @Nullable String address() {
        return address;
    }

}
