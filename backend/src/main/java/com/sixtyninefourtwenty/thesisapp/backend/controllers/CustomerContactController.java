package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.CustomerContactMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.CustomerContactService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class CustomerContactController {

    private final CustomerContactService customerContactService;
    private final CustomerContactMapper customerContactMapper;

    @GetMapping(path = "/customercontacts")
    public List<CustomerContact> getContactsForOneOrAllCustomers(@RequestParam(required = false) Long customerId) {
        final var contacts = customerId != null ? customerContactService.getContactsForCustomerId(customerId) :
                customerContactService.findAll();
        return StreamSupport.stream(contacts.spliterator(), false)
                .map(customerContactMapper::fromCustomerContact)
                .toList();
    }

    @GetMapping(path = "/customercontacts/{id}")
    public ResponseEntity<CustomerContact> findById(@PathVariable("id") long id) {
        return customerContactService.findById(id)
                .map(customerContact -> new ResponseEntity<>(customerContactMapper.fromCustomerContact(customerContact), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/customercontacts")
    public ResponseEntity<CustomerContact> create(@RequestBody CustomerContact customerContact) {
        final var contactEntity = customerContactMapper.toCustomerContact(customerContact);
        final var savedContactEntity = customerContactService.save(contactEntity);
        return new ResponseEntity<>(customerContactMapper.fromCustomerContact(savedContactEntity), HttpStatus.CREATED);
    }

    @PutMapping(path = "/customercontacts/{id}")
    public ResponseEntity<CustomerContact> fullUpdate(@PathVariable("id") long id, @RequestBody CustomerContact customerContact) {
        if (!customerContactService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalContact = customerContact.withId(id);
        final var contactEntity = customerContactMapper.toCustomerContact(finalContact);
        final var savedContactEntity = customerContactService.save(contactEntity);
        return new ResponseEntity<>(customerContactMapper.fromCustomerContact(savedContactEntity), HttpStatus.OK);
    }

    @DeleteMapping(path = "/customercontacts/{id}")
    public ResponseEntity<CustomerContact> delete(@PathVariable("id") long id) {
        customerContactService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
