package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MaterialSupplierMapper {

    MaterialSupplierEntity toMaterialSupplier(MaterialSupplier materialSupplierDto);

    MaterialSupplier fromMaterialSupplier(MaterialSupplierEntity materialSupplier);

}
