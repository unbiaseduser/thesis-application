package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.MaterialImportMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialImportService;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class MaterialImportController {

    private final MaterialImportService materialImportService;
    private final MaterialImportMapper materialImportMapper;

    @GetMapping(path = "/materialimports")
    public List<MaterialImport> getImportsForOneOrAllMaterials(@RequestParam(required = false) Long materialId) {
        final var imports = materialId != null ? materialImportService.getImportsForMaterialId(materialId) :
                materialImportService.findAll();
        return StreamSupport.stream(imports.spliterator(), false)
                .map(materialImportMapper::fromMaterialImport)
                .toList();
    }

    @GetMapping(path = "/materialimports/{id}")
    public ResponseEntity<MaterialImport> findById(@PathVariable("id") long id) {
        return materialImportService.findById(id)
                .map(materialImport -> new ResponseEntity<>(materialImportMapper.fromMaterialImport(materialImport), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/materialimports")
    public ResponseEntity<Result<MaterialImport>> tryCreate(@RequestBody MaterialImport materialImport) {
        final var entity = materialImportMapper.toMaterialImport(materialImport);
        final var probablySavedEntity = materialImportService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<MaterialImportEntity> s -> new ResponseEntity<>(new Result.Success<>(materialImportMapper.fromMaterialImport(s.data())), HttpStatus.CREATED);
            case Result.Rejection<MaterialImportEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @PutMapping(path = "/materialimports/{id}")
    public ResponseEntity<Result<MaterialImport>> tryFullUpdate(@PathVariable("id") long id, @RequestBody MaterialImport materialImport) {
        if (!materialImportService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalObj = materialImport.withId(id);
        final var entity = materialImportMapper.toMaterialImport(finalObj);
        final var probablySavedEntity = materialImportService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<MaterialImportEntity> s -> new ResponseEntity<>(new Result.Success<>(materialImportMapper.fromMaterialImport(s.data())), HttpStatus.OK);
            case Result.Rejection<MaterialImportEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @DeleteMapping(path = "/materialimports/{id}")
    public ResponseEntity<Result<MaterialImport>> tryDelete(@PathVariable("id") long id) {
        final var deleted = materialImportService.tryDeleteById(id);
        return switch (deleted) {
            case Result.Success<MaterialImportEntity> ignored -> new ResponseEntity<>(new Result.Success<>(null), HttpStatus.NO_CONTENT);
            case Result.Rejection<MaterialImportEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

}
