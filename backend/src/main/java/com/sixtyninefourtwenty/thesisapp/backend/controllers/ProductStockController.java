package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.backend.services.ProductStockService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ProductStockController {

    private final ProductStockService productStockService;

    @GetMapping(path = "productstock/{id}")
    public long calculateStockForProductId(@PathVariable("id") long productId) {
        return productStockService.calculateStockForProductId(productId);
    }

}
