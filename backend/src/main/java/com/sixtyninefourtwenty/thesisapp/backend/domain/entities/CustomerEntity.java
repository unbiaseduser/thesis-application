package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;

@Entity
@Table(name = "customers")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(onConstructor_ = {@Default})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerEntity {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private @NonNull String name;

    public long id() {
        return id;
    }

    public @NonNull String name() {
        return name;
    }

}
