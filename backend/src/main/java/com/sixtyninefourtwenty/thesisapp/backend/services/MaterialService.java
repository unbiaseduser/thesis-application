package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;

import java.util.Optional;

public interface MaterialService {

    boolean existsById(Long id);

    Iterable<MaterialEntity> findAll();

    Optional<MaterialEntity> findById(Long id);

    MaterialEntity save(MaterialEntity material);

    void delete(MaterialEntity material);

    void deleteById(Long id);

}
