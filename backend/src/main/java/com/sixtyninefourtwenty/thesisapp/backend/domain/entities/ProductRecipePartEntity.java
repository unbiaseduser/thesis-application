package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;

@Entity
@Table(name = "product_recipe_parts")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductRecipePartEntity {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "product_id", nullable = false)
    private @NonNull ProductEntity product;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "material_id", nullable = false)
    private @NonNull MaterialEntity material;

    @Column(name = "material_quantity")
    private int materialQuantity;

    @Default
    public ProductRecipePartEntity(
            long id, @NonNull
    ProductEntity product, @NonNull
            MaterialEntity material,
            int materialQuantity) {
        if (materialQuantity <= 0) {
            throw new IllegalArgumentException("Material quantity must not be <= 0");
        }
        this.id = id;
        this.product = product;
        this.material = material;
        this.materialQuantity = materialQuantity;
    }

    public long id() {
        return id;
    }

    public @NonNull ProductEntity product() {
        return product;
    }

    public @NonNull MaterialEntity material() {
        return material;
    }

    public int materialQuantity() {
        return materialQuantity;
    }

}
