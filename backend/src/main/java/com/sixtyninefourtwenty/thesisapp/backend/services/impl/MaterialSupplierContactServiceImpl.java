package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierContactEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialSupplierContactService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.MaterialSupplierContactRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MaterialSupplierContactServiceImpl implements MaterialSupplierContactService {

    private final MaterialSupplierContactRepository materialSupplierContactRepository;

    @Override
    public boolean existsById(Long id) {
        return materialSupplierContactRepository.existsById(id);
    }

    @Override
    public Iterable<MaterialSupplierContactEntity> findAll() {
        return materialSupplierContactRepository.findAll();
    }

    @Override
    public Iterable<MaterialSupplierContactEntity> getContactsForSupplierId(long supplierId) {
        return materialSupplierContactRepository.getContactsForSupplierId(supplierId);
    }

    @Override
    public Optional<MaterialSupplierContactEntity> findById(Long id) {
        return materialSupplierContactRepository.findById(id);
    }

    @Override
    public MaterialSupplierContactEntity save(MaterialSupplierContactEntity materialSupplierContact) {
        return materialSupplierContactRepository.save(materialSupplierContact);
    }

    @Override
    public void delete(MaterialSupplierContactEntity materialSupplierContact) {
        materialSupplierContactRepository.delete(materialSupplierContact);
    }

    @Override
    public void deleteById(Long id) {
        materialSupplierContactRepository.deleteById(id);
    }
}
