package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;

public interface ProductStockService {

    default long calculateStockForProduct(ProductEntity product) {
        return calculateStockForProductId(product.id());
    }

    long calculateStockForProductId(long productId);

}
