package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.MaterialRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MaterialServiceImpl implements MaterialService {

    private final MaterialRepository materialRepository;

    @Override
    public boolean existsById(Long id) {
        return materialRepository.existsById(id);
    }

    @Override
    public Iterable<MaterialEntity> findAll() {
        return materialRepository.findAll();
    }

    @Override
    public Optional<MaterialEntity> findById(Long id) {
        return materialRepository.findById(id);
    }

    @Override
    public MaterialEntity save(MaterialEntity material) {
        return materialRepository.save(material);
    }

    @Override
    public void delete(MaterialEntity material) {
        materialRepository.delete(material);
    }

    @Override
    public void deleteById(Long id) {
        materialRepository.deleteById(id);
    }
}
