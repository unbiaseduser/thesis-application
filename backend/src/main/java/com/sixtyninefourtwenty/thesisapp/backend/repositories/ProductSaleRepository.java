package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductSaleRepository extends CrudRepository<ProductSaleEntity, Long> {

    @Query(
            value = "SELECT * FROM product_sales WHERE product_id = :id",
            nativeQuery = true
    )
    Iterable<ProductSaleEntity> getSalesForProductId(@Param("id") long productId);

    default Iterable<ProductSaleEntity> getSalesForProduct(ProductEntity product) {
        return getSalesForProductId(product.id());
    }

    @Query(
            value = "SELECT SUM(product_quantity) FROM product_sales WHERE product_id = :id",
            nativeQuery = true
    )
    Optional<Long> getTotalSalesQuantityForProductIdAsOptional(@Param("id") long productId);

    default long getTotalSalesQuantityForProductId(long productId) {
        return getTotalSalesQuantityForProductIdAsOptional(productId).orElse(0L);
    }

    default long getTotalSalesQuantityForProduct(ProductEntity product) {
        return getTotalSalesQuantityForProductId(product.id());
    }

}
