package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerEntity;

import java.util.Optional;

public interface CustomerService {

    boolean existsById(Long id);

    Iterable<CustomerEntity> findAll();

    Optional<CustomerEntity> findById(Long id);

    CustomerEntity save(CustomerEntity customer);

    void delete(CustomerEntity customer);

    void deleteById(Long id);

}
