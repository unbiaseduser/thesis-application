package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.MaterialMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class MaterialController {

    private final MaterialService materialService;
    private final MaterialMapper materialMapper;

    @GetMapping(path = "/materials")
    public List<Material> findAll() {
        return StreamSupport.stream(materialService.findAll().spliterator(), false)
                .map(materialMapper::fromMaterial)
                .toList();
    }

    @GetMapping(path = "/materials/{id}")
    public ResponseEntity<Material> findById(@PathVariable("id") long id) {
        return materialService.findById(id)
                .map(material -> new ResponseEntity<>(materialMapper.fromMaterial(material), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/materials")
    public ResponseEntity<Material> create(@RequestBody Material material) {
        final var entity = materialMapper.toMaterial(material);
        final var savedEntity = materialService.save(entity);
        return new ResponseEntity<>(materialMapper.fromMaterial(savedEntity), HttpStatus.CREATED);
    }

    @PutMapping(path = "/materials/{id}")
    public ResponseEntity<Material> fullUpdate(@PathVariable("id") long id, @RequestBody Material material) {
        if (!materialService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalMaterial = material.withId(id);
        final var materialEntity = materialMapper.toMaterial(finalMaterial);
        final var savedMaterialEntity = materialService.save(materialEntity);
        return new ResponseEntity<>(materialMapper.fromMaterial(savedMaterialEntity), HttpStatus.OK);
    }

    @DeleteMapping(path = "/materials/{id}")
    public ResponseEntity<Material> delete(@PathVariable("id") long id) {
        materialService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
