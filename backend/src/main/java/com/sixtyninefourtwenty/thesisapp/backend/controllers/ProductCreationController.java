package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.ProductCreationMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductCreationService;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class ProductCreationController {

    private final ProductCreationService productCreationService;
    private final ProductCreationMapper productCreationMapper;

    @GetMapping(path = "/productcreations")
    public List<ProductCreation> getCreationsForOneOrAllProducts(@RequestParam(required = false) Long productId) {
        final var creations = productId != null ? productCreationService.getCreationsForProductId(productId) :
                productCreationService.findAll();
        return StreamSupport.stream(creations.spliterator(), false)
                .map(productCreationMapper::fromProductCreation)
                .toList();
    }

    @GetMapping(path = "/productcreations/{id}")
    public ResponseEntity<ProductCreation> findById(@PathVariable("id") long id) {
        return productCreationService.findById(id)
                .map(creation -> new ResponseEntity<>(productCreationMapper.fromProductCreation(creation), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/productcreations")
    public ResponseEntity<Result<ProductCreation>> tryCreate(@RequestBody ProductCreation productCreation) {
        final var entity = productCreationMapper.toProductCreation(productCreation);
        final var probablySavedEntity = productCreationService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<ProductCreationEntity> s -> new ResponseEntity<>(new Result.Success<>(productCreationMapper.fromProductCreation(s.data())), HttpStatus.CREATED);
            case Result.Rejection<ProductCreationEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @PutMapping(path = "/productcreations/{id}")
    public ResponseEntity<Result<ProductCreation>> tryFullUpdate(@PathVariable("id") long id, @RequestBody ProductCreation productCreation) {
        if (!productCreationService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalObj = productCreation.withId(id);
        final var entity = productCreationMapper.toProductCreation(finalObj);
        final var probablySavedEntity = productCreationService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<ProductCreationEntity> s -> new ResponseEntity<>(new Result.Success<>(productCreationMapper.fromProductCreation(s.data())), HttpStatus.OK);
            case Result.Rejection<ProductCreationEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @DeleteMapping(path = "/productcreations/{id}")
    public ResponseEntity<Result<ProductCreation>> tryDelete(@PathVariable("id") long id) {
        final var deleted = productCreationService.tryDeleteById(id);
        return switch (deleted) {
            case Result.Success<ProductCreationEntity> s -> new ResponseEntity<>(new Result.Success<>(null), HttpStatus.NO_CONTENT);
            case Result.Rejection<ProductCreationEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

}
