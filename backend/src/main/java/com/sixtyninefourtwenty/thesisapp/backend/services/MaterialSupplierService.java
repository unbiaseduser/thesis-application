package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialSupplierEntity;

import java.util.Optional;

public interface MaterialSupplierService {

    boolean existsById(Long id);

    Iterable<MaterialSupplierEntity> findAll();

    Optional<MaterialSupplierEntity> findById(Long id);

    MaterialSupplierEntity save(MaterialSupplierEntity materialSupplier);

    void delete(MaterialSupplierEntity materialSupplier);

    void deleteById(Long id);

}
