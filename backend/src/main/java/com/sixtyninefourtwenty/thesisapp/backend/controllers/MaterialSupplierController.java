package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.MaterialSupplierMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialSupplierService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class MaterialSupplierController {

    private final MaterialSupplierService materialSupplierService;
    private final MaterialSupplierMapper materialSupplierMapper;

    @GetMapping(path = "/materialsuppliers")
    public List<MaterialSupplier> findAll() {
        return StreamSupport.stream(materialSupplierService.findAll().spliterator(), false)
                .map(materialSupplierMapper::fromMaterialSupplier)
                .toList();
    }

    @GetMapping(path = "/materialsuppliers/{id}")
    public ResponseEntity<MaterialSupplier> findById(@PathVariable("id") long id) {
        return materialSupplierService.findById(id)
                .map(supplier -> new ResponseEntity<>(materialSupplierMapper.fromMaterialSupplier(supplier), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/materialsuppliers")
    public ResponseEntity<MaterialSupplier> create(@RequestBody MaterialSupplier supplier) {
        final var entity = materialSupplierMapper.toMaterialSupplier(supplier);
        final var savedEntity = materialSupplierService.save(entity);
        return new ResponseEntity<>(materialSupplierMapper.fromMaterialSupplier(savedEntity), HttpStatus.CREATED);
    }

    @PutMapping(path = "/materialsuppliers/{id}")
    public ResponseEntity<MaterialSupplier> fullUpdate(@PathVariable("id") long id, @RequestBody MaterialSupplier supplier) {
        if (!materialSupplierService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalObj = supplier.withId(id);
        final var entity = materialSupplierMapper.toMaterialSupplier(finalObj);
        final var savedEntity = materialSupplierService.save(entity);
        return new ResponseEntity<>(materialSupplierMapper.fromMaterialSupplier(savedEntity), HttpStatus.OK);
    }

    @DeleteMapping(path = "/materialsuppliers/{id}")
    public ResponseEntity<MaterialSupplier> delete(@PathVariable("id") long id) {
        materialSupplierService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
