package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductRecipePart;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.ProductRecipePartMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductRecipePartService;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class ProductRecipePartController {

    private final ProductRecipePartService productRecipePartService;
    private final ProductRecipePartMapper productRecipePartMapper;

    @GetMapping(path = "/productrecipeparts")
    public List<ProductRecipePart> getRecipeForOneOrAllProducts(@RequestParam(required = false) Long productId) {
        final var parts = productId != null ? productRecipePartService.getRecipeForProductId(productId) :
                productRecipePartService.findAll();
        return StreamSupport.stream(parts.spliterator(), false)
                .map(productRecipePartMapper::fromProductRecipePart)
                .toList();
    }

    @GetMapping(path = "/productrecipeparts/{id}")
    public ResponseEntity<ProductRecipePart> findById(@PathVariable("id") long id) {
        return productRecipePartService.findById(id)
                .map(part -> new ResponseEntity<>(productRecipePartMapper.fromProductRecipePart(part), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/productrecipeparts")
    public ResponseEntity<Result<ProductRecipePart>> tryCreate(@RequestBody ProductRecipePart productRecipePart) {
        final var entity = productRecipePartMapper.toProductRecipePart(productRecipePart);
        final var probablySavedEntity = productRecipePartService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<ProductRecipePartEntity> s -> new ResponseEntity<>(new Result.Success<>(productRecipePartMapper.fromProductRecipePart(s.data())), HttpStatus.CREATED);
            case Result.Rejection<ProductRecipePartEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @PutMapping(path = "/productrecipeparts/{id}")
    public ResponseEntity<Result<ProductRecipePart>> tryFullUpdate(@PathVariable("id") long id, @RequestBody ProductRecipePart productRecipePart) {
        if (!productRecipePartService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalObj = productRecipePart.withId(id);
        final var entity = productRecipePartMapper.toProductRecipePart(finalObj);
        final var probablySavedEntity = productRecipePartService.trySave(entity);
        return switch (probablySavedEntity) {
            case Result.Success<ProductRecipePartEntity> s -> new ResponseEntity<>(new Result.Success<>(productRecipePartMapper.fromProductRecipePart(s.data())), HttpStatus.OK);
            case Result.Rejection<ProductRecipePartEntity> r -> new ResponseEntity<>(new Result.Rejection<>(r.message()), HttpStatus.ACCEPTED);
        };
    }

    @DeleteMapping(path = "/productrecipeparts/{id}")
    public ResponseEntity<ProductRecipePart> delete(@PathVariable("id") long id) {
        productRecipePartService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
