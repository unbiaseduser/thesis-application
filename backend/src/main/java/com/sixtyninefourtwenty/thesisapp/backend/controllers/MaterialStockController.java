package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialStockService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class MaterialStockController {

    private final MaterialStockService materialStockService;

    @GetMapping(path = "materialstock/{id}")
    public long calculateStockForMaterial(@PathVariable("id") long materialId) {
        return materialStockService.calculateStockForMaterialId(materialId);
    }

}
