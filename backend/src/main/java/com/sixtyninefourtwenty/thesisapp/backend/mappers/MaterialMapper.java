package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MaterialMapper {

    MaterialEntity toMaterial(Material materialDto);

    Material fromMaterial(MaterialEntity material);

}
