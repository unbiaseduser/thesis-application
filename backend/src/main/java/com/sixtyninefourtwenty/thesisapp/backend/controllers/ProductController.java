package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.ProductMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @GetMapping(path = "/products")
    public List<Product> findAll() {
        return StreamSupport.stream(productService.findAll().spliterator(), false)
                .map(productMapper::fromProduct)
                .toList();
    }

    @GetMapping(path = "/products/{id}")
    public ResponseEntity<Product> findById(@PathVariable("id") long id) {
        return productService.findById(id)
                .map(product -> new ResponseEntity<>(productMapper.fromProduct(product), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/products")
    public ResponseEntity<Product> create(@RequestBody Product product) {
        final var entity = productMapper.toProduct(product);
        final var savedEntity = productService.save(entity);
        return new ResponseEntity<>(productMapper.fromProduct(savedEntity), HttpStatus.CREATED);
    }

    @PutMapping(path = "/products/{id}")
    public ResponseEntity<Product> fullUpdate(@PathVariable("id") long id, @RequestBody Product product) {
        if (!productService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalProduct = product.withId(id);
        final var entity = productMapper.toProduct(finalProduct);
        final var savedEntity = productService.save(entity);
        return new ResponseEntity<>(productMapper.fromProduct(savedEntity), HttpStatus.OK);
    }

    @DeleteMapping(path = "/products/{id}")
    public ResponseEntity<Product> delete(@PathVariable("id") long id) {
        productService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
