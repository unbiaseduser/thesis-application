package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductCreationMapper {

    ProductCreationEntity toProductCreation(ProductCreation productCreationDto);

    ProductCreation fromProductCreation(ProductCreationEntity productCreation);

}
