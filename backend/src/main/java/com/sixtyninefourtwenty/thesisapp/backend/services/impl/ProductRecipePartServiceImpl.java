package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductRecipePartService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductRecipePartRepository;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.logging.Level;

@Service
@AllArgsConstructor
@Log
public class ProductRecipePartServiceImpl implements ProductRecipePartService {

    private final ProductRecipePartRepository productRecipePartRepository;

    @Override
    public boolean existsById(Long id) {
        return productRecipePartRepository.existsById(id);
    }

    @Override
    public Iterable<ProductRecipePartEntity> getRecipeForProductId(long id) {
        return productRecipePartRepository.getRecipeForProductId(id);
    }

    @Override
    public Iterable<ProductRecipePartEntity> findAll() {
        return productRecipePartRepository.findAll();
    }

    @Override
    public Optional<ProductRecipePartEntity> findById(Long id) {
        return productRecipePartRepository.findById(id);
    }

    @Override
    public Result<ProductRecipePartEntity> trySave(ProductRecipePartEntity productRecipePart) {
        final var currentProductRecipe = productRecipePartRepository.getRecipeForProduct(productRecipePart.product());
        for (final var recipePart : currentProductRecipe) {
            final var sameMaterial = recipePart.material().equals(productRecipePart.material());
            if (sameMaterial) {
                final var isDifferentId = recipePart.id() != productRecipePart.id();
                if (isDifferentId) {
                    log.log(Level.INFO, "Found existing product recipe record with same product and material: " +
                            recipePart + ". Please update that one using its id instead.");
                    return new Result.Rejection<>("""
                            Found existing product recipe record with same product and material: %d.
                            Please update that one using its id instead.
                            """.formatted(recipePart.id()));
                } else {
                    break;
                }
            }
        }
        return new Result.Success<>(productRecipePartRepository.save(productRecipePart));
    }

    @Override
    public void delete(ProductRecipePartEntity productRecipePart) {
        productRecipePartRepository.delete(productRecipePart);
    }

    @Override
    public void deleteById(Long id) {
        productRecipePartRepository.deleteById(id);
    }
}
