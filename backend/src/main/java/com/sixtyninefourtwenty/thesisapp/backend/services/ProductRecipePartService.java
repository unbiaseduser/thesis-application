package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;

import java.util.Optional;

public interface ProductRecipePartService {

    boolean existsById(Long id);

    Iterable<ProductRecipePartEntity> getRecipeForProductId(long id);

    Iterable<ProductRecipePartEntity> findAll();

    Optional<ProductRecipePartEntity> findById(Long id);

    Result<ProductRecipePartEntity> trySave(ProductRecipePartEntity productRecipePart);

    void delete(ProductRecipePartEntity productRecipePart);

    void deleteById(Long id);

}
