package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductMapper {

    ProductEntity toProduct(Product productDto);

    Product fromProduct(ProductEntity product);

}
