package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;

public interface MaterialStockService {

    default long calculateStockForMaterial(MaterialEntity material) {
        return calculateStockForMaterialId(material.id());
    }

    long calculateStockForMaterialId(long materialId);

}
