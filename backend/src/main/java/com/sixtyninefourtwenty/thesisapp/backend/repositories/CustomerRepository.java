package com.sixtyninefourtwenty.thesisapp.backend.repositories;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<CustomerEntity, Long> {
}
