package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductSaleMapper {

    ProductSaleEntity toProductSale(ProductSale productSaleDto);

    ProductSale fromProductSale(ProductSaleEntity productSale);

}
