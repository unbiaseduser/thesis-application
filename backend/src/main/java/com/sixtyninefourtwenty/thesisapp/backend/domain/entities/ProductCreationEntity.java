package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;

import java.time.LocalDateTime;

@Entity
@Table(name = "product_creations")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductCreationEntity {
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "product_id", nullable = false)
    private @NonNull ProductEntity product;

    private int quantity;

    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private @NonNull LocalDateTime time;

    @Default
    public ProductCreationEntity(
            long id, @NonNull
    ProductEntity product, int quantity, @NonNull
            LocalDateTime time) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must not be <= 0");
        }
        this.id = id;
        this.product = product;
        this.quantity = quantity;
        this.time = time;
    }

    public ProductCreationEntity(long id, ProductEntity product, int quantity) {
        this(id, product, quantity, LocalDateTime.now());
    }

    public long id() {
        return id;
    }

    public @NonNull ProductEntity product() {
        return product;
    }

    public int quantity() {
        return quantity;
    }

    public @NonNull LocalDateTime time() {
        return time;
    }

}
