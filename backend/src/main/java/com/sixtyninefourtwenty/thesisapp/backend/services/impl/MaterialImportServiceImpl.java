package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialImportService;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialStockService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.MaterialImportRepository;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MaterialImportServiceImpl implements MaterialImportService {

    private final MaterialImportRepository materialImportRepository;
    private final MaterialStockService materialStockService;

    @Override
    public boolean existsById(Long id) {
        return materialImportRepository.existsById(id);
    }

    @Override
    public Iterable<MaterialImportEntity> getImportsForMaterialId(long materialId) {
        return materialImportRepository.getImportsForMaterialId(materialId);
    }

    @Override
    public Iterable<MaterialImportEntity> findAll() {
        return materialImportRepository.findAll();
    }

    @Override
    public Optional<MaterialImportEntity> findById(Long id) {
        return materialImportRepository.findById(id);
    }

    @Override
    public Result<MaterialImportEntity> trySave(MaterialImportEntity materialImport) {
        if (materialImportRepository.existsById(materialImport.id())) {
            //Updating existing import
            final var existingImport = materialImportRepository.findById(materialImport.id()).orElseThrow();
            if (existingImport.material().equals(materialImport.material())) { //same material
                final var quantityDelta = existingImport.quantity() - materialImport.quantity();
                if (quantityDelta > 0) { // reducing the number of materials, check material stock
                    final var stock = materialStockService.calculateStockForMaterial(existingImport.material());
                    if (stock < quantityDelta) {
                        return new Result.Rejection<>(String.format("""
                                Insufficient stock for material %s.
                                Current stock: %d.
                                Stock required: %d.
                                """, existingImport.material().name(), stock, quantityDelta));
                    }
                }
            } else { //Changing material, check stock of the old one
                final var stock = materialStockService.calculateStockForMaterial(existingImport.material());
                if (stock < existingImport.quantity()) {
                    return new Result.Rejection<>(String.format("""
                            Insufficient stock for material %s.
                            Current stock: %d.
                            Trying to delete: %d.
                            """, existingImport.material().name(), stock, existingImport.quantity()));
                }
            }
        }

        return new Result.Success<>(materialImportRepository.save(materialImport));
    }

    @Override
    public Result<MaterialImportEntity> tryDelete(MaterialImportEntity materialImport) {
        return tryDeleteById(materialImport.id());
    }

    @Override
    public Result<MaterialImportEntity> tryDeleteById(Long id) {
        final var existingImportOpt = materialImportRepository.findById(id);
        if (existingImportOpt.isPresent()) {
            final var existingImport = existingImportOpt.orElseThrow();
            final var stock = materialStockService.calculateStockForMaterial(existingImport.material());
            if (stock < existingImport.quantity()) {
                return new Result.Rejection<>(String.format("""
                        Insufficient stock for material %d.
                        Current stock: %d.
                        Trying to delete: %d.
                        """, existingImport.material().id(), stock, existingImport.quantity()));
            } else {
                materialImportRepository.deleteById(id);
                return new Result.Success<>(null);
            }
        } else {
            materialImportRepository.deleteById(id);
            return new Result.Success<>(null);
        }
    }
}
