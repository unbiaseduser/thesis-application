package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;

import java.util.Optional;

public interface ProductSaleService {

    boolean existsById(Long id);

    Iterable<ProductSaleEntity> findAll();

    Iterable<ProductSaleEntity> getSalesForProductId(long productId);

    Optional<ProductSaleEntity> findById(Long id);

    Result<ProductSaleEntity> trySave(ProductSaleEntity productSale);

    void delete(ProductSaleEntity productSale);

    void deleteById(Long id);

}
