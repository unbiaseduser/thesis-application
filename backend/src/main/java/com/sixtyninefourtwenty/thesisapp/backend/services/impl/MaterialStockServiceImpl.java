package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialStockService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.MaterialImportRepository;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductCreationRepository;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductRecipePartRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MaterialStockServiceImpl implements MaterialStockService {

    private final MaterialImportRepository materialImportRepository;
    private final ProductRecipePartRepository productRecipePartRepository;
    private final ProductCreationRepository productCreationRepository;

    @Override
    public long calculateStockForMaterialId(long materialId) {
        var result = materialImportRepository.getTotalImportQuantityForMaterialId(materialId);
        final var recipeParts = productRecipePartRepository.getRecipePartsContainingMaterialId(materialId);
        for (final var recipePart : recipeParts) {
            final var productCreations = productCreationRepository.getCreationsForProduct(recipePart.product());
            for (final var productCreation : productCreations) {
                final var quantityUsed = recipePart.materialQuantity() * productCreation.quantity();
                result -= quantityUsed;
            }
        }
        return result;
    }
}
