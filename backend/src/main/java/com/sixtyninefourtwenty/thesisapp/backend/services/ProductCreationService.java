package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;

import java.util.Optional;

public interface ProductCreationService {

    boolean existsById(Long id);

    Iterable<ProductCreationEntity> findAll();

    Iterable<ProductCreationEntity> getCreationsForProductId(long id);

    Optional<ProductCreationEntity> findById(Long id);

    Result<ProductCreationEntity> trySave(ProductCreationEntity productCreation);

    Result<ProductCreationEntity> tryDelete(ProductCreationEntity productCreation);

    Result<ProductCreationEntity> tryDeleteById(Long id);

}
