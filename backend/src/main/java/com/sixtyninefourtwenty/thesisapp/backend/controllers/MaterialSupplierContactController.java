package com.sixtyninefourtwenty.thesisapp.backend.controllers;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.backend.mappers.MaterialSupplierContactMapper;
import com.sixtyninefourtwenty.thesisapp.backend.services.MaterialSupplierContactService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class MaterialSupplierContactController {

    private final MaterialSupplierContactService materialSupplierContactService;
    private final MaterialSupplierContactMapper materialSupplierContactMapper;

    @GetMapping(path = "/materialsuppliercontacts")
    public List<MaterialSupplierContact> getContactsForOneOrAllSuppliers(@RequestParam(required = false) Long supplierId) {
        final var contacts = supplierId != null ? materialSupplierContactService.getContactsForSupplierId(supplierId) :
                materialSupplierContactService.findAll();
        return StreamSupport.stream(contacts.spliterator(), false)
                .map(materialSupplierContactMapper::fromMaterialSupplierContact)
                .toList();
    }

    @GetMapping(path = "/materialsuppliercontacts/{id}")
    public ResponseEntity<MaterialSupplierContact> findById(@PathVariable("id") long id) {
        return materialSupplierContactService.findById(id)
                .map(contact -> new ResponseEntity<>(materialSupplierContactMapper.fromMaterialSupplierContact(contact), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(path = "/materialsuppliercontacts")
    public ResponseEntity<MaterialSupplierContact> create(@RequestBody MaterialSupplierContact contact) {
        final var entity = materialSupplierContactMapper.toMaterialSupplierContact(contact);
        final var savedEntity = materialSupplierContactService.save(entity);
        return new ResponseEntity<>(materialSupplierContactMapper.fromMaterialSupplierContact(savedEntity), HttpStatus.CREATED);
    }

    @PutMapping(path = "/materialsuppliercontacts/{id}")
    public ResponseEntity<MaterialSupplierContact> fullUpdate(@PathVariable("id") long id, @RequestBody MaterialSupplierContact contact) {
        if (!materialSupplierContactService.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final var finalContact = contact.withId(id);
        final var entity = materialSupplierContactMapper.toMaterialSupplierContact(finalContact);
        final var savedEntity = materialSupplierContactService.save(entity);
        return new ResponseEntity<>(materialSupplierContactMapper.fromMaterialSupplierContact(savedEntity), HttpStatus.OK);
    }

    @DeleteMapping(path = "/materialsuppliercontacts/{id}")
    public ResponseEntity<MaterialSupplierContact> delete(@PathVariable("id") long id) {
        materialSupplierContactService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
