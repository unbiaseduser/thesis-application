package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

import java.time.LocalDateTime;

@Entity
@Table(name = "product_sales")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductSaleEntity {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "product_id", nullable = false)
    private @NonNull ProductEntity product;

    @Column(name = "product_quantity")
    private int productQuantity;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "customer_id")
    private @Nullable CustomerEntity customer;

    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private @NonNull LocalDateTime time;

    @Default
    public ProductSaleEntity(
            long id, @NonNull
    ProductEntity product,
            int productQuantity, @Nullable
            CustomerEntity customer, @NonNull
            LocalDateTime time) {
        if (productQuantity <= 0) {
            throw new IllegalArgumentException("Product quantity must not be <= 0");
        }
        this.id = id;
        this.product = product;
        this.productQuantity = productQuantity;
        this.customer = customer;
        this.time = time;
    }

    public ProductSaleEntity(long id, ProductEntity product, int productQuantity, CustomerEntity customer) {
        this(id, product, productQuantity, customer, LocalDateTime.now());
    }

    public long id() {
        return id;
    }

    public @NonNull ProductEntity product() {
        return product;
    }

    public int productQuantity() {
        return productQuantity;
    }

    public @Nullable CustomerEntity customer() {
        return customer;
    }

    public @NonNull LocalDateTime time() {
        return time;
    }

}
