package com.sixtyninefourtwenty.thesisapp.backend.mappers;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.CustomerEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CustomerMapper {

    CustomerEntity toCustomer(Customer customerDto);

    Customer fromCustomer(CustomerEntity customer);

}
