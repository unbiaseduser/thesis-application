package com.sixtyninefourtwenty.thesisapp.backend.services.impl;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductSaleService;
import com.sixtyninefourtwenty.thesisapp.backend.services.ProductStockService;
import com.sixtyninefourtwenty.thesisapp.backend.repositories.ProductSaleRepository;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Supplier;

@Service
@AllArgsConstructor
@Log
public class ProductSaleServiceImpl implements ProductSaleService {

    private final ProductSaleRepository productSaleRepository;
    private final ProductStockService productStockService;

    @Override
    public boolean existsById(Long id) {
        return productSaleRepository.existsById(id);
    }

    @Override
    public Iterable<ProductSaleEntity> findAll() {
        return productSaleRepository.findAll();
    }

    @Override
    public Iterable<ProductSaleEntity> getSalesForProductId(long productId) {
        return productSaleRepository.getSalesForProductId(productId);
    }

    @Override
    public Optional<ProductSaleEntity> findById(Long id) {
        return productSaleRepository.findById(id);
    }

    @Override
    public Result<ProductSaleEntity> trySave(ProductSaleEntity productSale) {
        if (!productSaleRepository.existsById(productSale.id())) {
            //Adding new sale, check product stock
            final var stock = productStockService.calculateStockForProduct(productSale.product());
            if (stock < productSale.productQuantity()) {
                return new Result.Rejection<>(String.format("""
                        Not enough stock for product %s.
                        Current stock: %d.
                        Stock required: %d.
                        """, productSale.product().name(), stock, productSale.productQuantity()));
            }
        } else {
            //Updating existing sale
            final var existingSale = productSaleRepository.findById(productSale.id()).orElseThrow();
            if (existingSale.product().equals(productSale.product())) { //same product
                final var quantityDelta = existingSale.productQuantity() - productSale.productQuantity();
                if (quantityDelta < 0) { // increasing the number of sales, check stock vs difference between old and new quantity
                    final var extraQuantity = Math.abs(quantityDelta);
                    final var stock = productStockService.calculateStockForProduct(productSale.product());
                    if (stock < extraQuantity) {
                        return new Result.Rejection<>(String.format("""
                                Not enough stock for product %s.
                                Current stock: %d.
                                Stock required: %d.
                                """, productSale.product().name(), stock, extraQuantity));
                    }
                }
            } else {
                //Changing product. Just do the same as if adding a new sale, don't care about the old one.
                final var stock = productStockService.calculateStockForProduct(productSale.product());
                if (stock < productSale.productQuantity()) {
                    return new Result.Rejection<>(String.format("""
                        Not enough stock for product %s.
                        Current stock: %d.
                        Stock required: %d.
                        """, productSale.product().name(), stock, productSale.productQuantity()));
                }
            }
        }
        return new Result.Success<>(productSaleRepository.save(productSale));
    }

    @Override
    public void delete(ProductSaleEntity productSale) {
        productSaleRepository.delete(productSale);
    }

    @Override
    public void deleteById(Long id) {
        productSaleRepository.deleteById(id);
    }
}
