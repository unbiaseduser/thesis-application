package com.sixtyninefourtwenty.thesisapp.backend.domain.entities;

import com.sixtyninefourtwenty.thesisapp.backend.annotations.Default;
import jakarta.persistence.*;
import lombok.*;
import org.jspecify.annotations.NonNull;

@Entity
@Table(name = "products")
@With
@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(onConstructor_ = {@Default})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductEntity {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private @NonNull String name;

    @Column(nullable = false)
    private @NonNull String unit;

    public long id() {
        return id;
    }

    public @NonNull String name() {
        return name;
    }

    public @NonNull String unit() {
        return unit;
    }

}
