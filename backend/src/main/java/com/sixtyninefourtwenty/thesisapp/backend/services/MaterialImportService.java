package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import org.jspecify.annotations.Nullable;

import java.util.Optional;

public interface MaterialImportService {

    boolean existsById(Long id);

    Iterable<MaterialImportEntity> getImportsForMaterialId(long materialId);

    Iterable<MaterialImportEntity> findAll();

    Optional<MaterialImportEntity> findById(Long id);

    Result<MaterialImportEntity> trySave(MaterialImportEntity materialImport);

    Result<MaterialImportEntity> tryDelete(MaterialImportEntity materialImport);

    Result<MaterialImportEntity> tryDeleteById(Long id);

}
