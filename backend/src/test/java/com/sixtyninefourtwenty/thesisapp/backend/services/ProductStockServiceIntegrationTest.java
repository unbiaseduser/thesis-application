package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.backend.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductStockServiceIntegrationTest {

    private final ProductStockService underTest;
    private final MaterialImportService materialImportService;
    private final ProductRecipePartService productRecipePartService;
    private final ProductCreationService productCreationService;
    private final ProductSaleService productSaleService;

    @Autowired
    public ProductStockServiceIntegrationTest(
            ProductStockService underTest,
            MaterialImportService materialImportService,
            ProductRecipePartService productRecipePartService,
            ProductCreationService productCreationService,
            ProductSaleService productSaleService) {
        this.underTest = underTest;
        this.materialImportService = materialImportService;
        this.productRecipePartService = productRecipePartService;
        this.productCreationService = productCreationService;
        this.productSaleService = productSaleService;
    }

    @Test
    void calcStockForNonExistentProductReturns0() {
        final var product = TestUtils.createTestProduct();
        assertEquals(0, underTest.calculateStockForProduct(product));
    }

    @Test
    void calcStockForProductCreatedButNeverSoldReturnsNumberCreated() {
        final var material = TestUtils.createTestMaterial();
        final var product = TestUtils.createTestProduct();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE));
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 1));
        final var productsCreated = 10;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));

        assertEquals(productsCreated, underTest.calculateStockForProduct(product));
    }

    @Test
    void calcStockForProductCreatedAndSold() {
        final var material = TestUtils.createTestMaterial();
        final var product = TestUtils.createTestProduct();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE));
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 1));
        final var productsCreated = 10;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        final var productsSold = 3;
        productSaleService.trySave(new ProductSaleEntity(1L, product, productsSold, null));

        assertEquals(productsCreated - productsSold, underTest.calculateStockForProduct(product));
    }

}
