package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.*;
import com.sixtyninefourtwenty.thesisapp.backend.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaterialStockServiceIntegrationTest {

    private final MaterialStockService underTest;
    private final MaterialImportService materialImportService;
    private final ProductRecipePartService productRecipePartService;
    private final ProductCreationService productCreationService;

    @Autowired
    public MaterialStockServiceIntegrationTest(
            MaterialStockService underTest,
            MaterialImportService materialImportService,
            ProductRecipePartService productRecipePartService,
            ProductCreationService productCreationService) {
        this.underTest = underTest;
        this.materialImportService = materialImportService;
        this.productRecipePartService = productRecipePartService;
        this.productCreationService = productCreationService;
    }

    private MaterialEntity createTestMaterial() {
        return TestUtils.createTestMaterial();
    }

    @Test
    void calcStockForNonExistentMaterialReturns0() {
        final var material = createTestMaterial();
        assertEquals(0, underTest.calculateStockForMaterial(material));
    }

    @Test
    void calcStockForMaterialImportedButNeverUsedReturnsNumberImported() {
        final var material = createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, 10));
        assertEquals(10, underTest.calculateStockForMaterial(material));
    }

    @Test
    void calcStockForMaterialImportedAndUsed() {
        final var material = createTestMaterial();
        final var materialImportQuantity = 10;
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, materialImportQuantity));
        final var product = new ProductEntity(1L, "baz", "blah");
        final var materialsRequiredToBuildEachProduct = 2;
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 2;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        assertEquals(
                materialImportQuantity - materialsRequiredToBuildEachProduct * productsCreated,
                underTest.calculateStockForMaterial(material)
        );
    }

}
