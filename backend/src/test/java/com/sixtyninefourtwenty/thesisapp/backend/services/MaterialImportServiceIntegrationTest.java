package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.backend.utils.TestUtils;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaterialImportServiceIntegrationTest {

    private final MaterialImportService underTest;
    private final ProductRecipePartService productRecipePartService;
    private final ProductCreationService productCreationService;

    @Autowired
    public MaterialImportServiceIntegrationTest(
            MaterialImportService underTest,
            ProductRecipePartService productRecipePartService,
            ProductCreationService productCreationService) {
        this.underTest = underTest;
        this.productRecipePartService = productRecipePartService;
        this.productCreationService = productCreationService;
    }

    @Test
    void changeMaterialWhileInsufficientStockGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);
        underTest.trySave(new MaterialImportEntity(2L, null, material, 2));
        // we have 12 material units
        final var materialsRequiredToBuildEachProduct = 4;
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 3;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        // 12 - 12 = 0 left

        // modify the record with 10 units with new material. we now have 0 - 10 = -10 units, so illegal data
        final var result = underTest.trySave(materialImport.withMaterial(TestUtils.createTestMaterial2()));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void changeMaterialWhileSufficientStockGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);
        underTest.trySave(new MaterialImportEntity(2L, null, material, 12));
        // we have 22 material units
        final var materialsRequiredToBuildEachProduct = 4;
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 3;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        // 22 - 12 = 10 left

        // modify the record with 10 units with new material. we now have 10 - 10 = 0 units, so legal data
        final var result = underTest.trySave(materialImport.withMaterial(TestUtils.createTestMaterial2()));
        assertInstanceOf(Result.Success.class, result);
    }

    @Test
    void reduceNumberOfSameMaterialWhileInsufficientStockGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);
        underTest.trySave(new MaterialImportEntity(2L, null, material, 2));
        // we have 12 material units
        final var materialsRequiredToBuildEachProduct = 4;
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 3;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        // spent 12 material units, we have 0 left

        // modify the record with 10 units with 1 unit. we now have 0 - 9 = -9 units, so illegal data
        final var result = underTest.trySave(materialImport.withQuantity(1));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void reduceNumberOfSameMaterialWhileSufficientStockGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);
        underTest.trySave(new MaterialImportEntity(2L, null, material, 6));
        // we have 16 material units
        final var materialsRequiredToBuildEachProduct = 4;
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 3;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        // spent 12 material units, we have 4 left

        // modify the record with 10 units with 6 units. we now have 4 - 4 = 0 units, so legal data
        final var result = underTest.trySave(materialImport.withQuantity(6));
        assertInstanceOf(Result.Success.class, result);
    }

    @Test
    void increaseNumberOfMaterialGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);

        final var result = underTest.trySave(materialImport.withQuantity(50));
        assertInstanceOf(Result.Success.class, result);
    }

    @Test
    void deleteMaterialImportWhileInsufficientStockGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);
        underTest.trySave(new MaterialImportEntity(2L, null, material, 2));
        // we have 12 material units
        final var materialsRequiredToBuildEachProduct = 4;
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 3;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        // spent 12 material units, we have 0 left

        // delete the record with 10 units. we now have 0 - 10 = -10 units, so illegal data
        final var deleted = underTest.tryDelete(materialImport);
        assertInstanceOf(Result.Rejection.class, deleted);
    }

    @Test
    void deleteMaterialImportWhileSufficientStockGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        final var materialImportQuantity = 10;
        final var materialImport = new MaterialImportEntity(1L, null, material, materialImportQuantity);
        underTest.trySave(materialImport);
        underTest.trySave(new MaterialImportEntity(2L, null, material, 6));
        // we have 16 material units
        final var materialsRequiredToBuildEachProduct = 4;
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, materialsRequiredToBuildEachProduct));
        final var productsCreated = 1;
        productCreationService.trySave(new ProductCreationEntity(1L, product, productsCreated));
        // spent 4 material units, we have 12 left

        // delete the record with 10 units. we now have 12 - 10 = 2 units, so legal data
        final var deleted = underTest.tryDelete(materialImport);
        assertInstanceOf(Result.Success.class, deleted);
    }

}
