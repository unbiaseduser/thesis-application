package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.backend.utils.TestUtils;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductCreationServiceIntegrationTest {

    private final ProductCreationService underTest;
    private final ProductRecipePartService productRecipePartService;
    private final MaterialImportService materialImportService;
    private final ProductSaleService productSaleService;

    @Autowired
    public ProductCreationServiceIntegrationTest(
            ProductCreationService underTest,
            ProductRecipePartService productRecipePartService,
            MaterialImportService materialImportService,
            ProductSaleService productSaleService) {
        this.underTest = underTest;
        this.productRecipePartService = productRecipePartService;
        this.materialImportService = materialImportService;
        this.productSaleService = productSaleService;
    }

    @Test
    void createCreationForProductWithNoRecipeGetsRejected() {
        final var product = TestUtils.createTestProduct();
        final var result = underTest.trySave(new ProductCreationEntity(1L, product, 1));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void changeProductWhileInsufficientProductStockGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE));
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));
        final var product2 = TestUtils.createTestProduct2();
        productRecipePartService.trySave(new ProductRecipePartEntity(2L, product2, material, 3));

        final var creation = new ProductCreationEntity(1L, product, 1);
        underTest.trySave(creation);
        // 1 product #1
        productSaleService.trySave(new ProductSaleEntity(1L, product, 1, null));
        // 1 - 1 = 0 product #1

        // modify product #1 creation x1 to product #2. product #1 has 0 - 1 = -1 left, so illegal data
        final var result = underTest.trySave(creation.withProduct(product2));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void changeProductWhileSufficientProductStockGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE));
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));
        final var product2 = TestUtils.createTestProduct2();
        productRecipePartService.trySave(new ProductRecipePartEntity(2L, product2, material, 3));

        final var creation = new ProductCreationEntity(1L, product, 1);
        underTest.trySave(creation);
        underTest.trySave(creation.withId(2L).withQuantity(2));
        // 3 product #1
        productSaleService.trySave(new ProductSaleEntity(1L, product, 1, null));
        // 3 - 1 = 2 product #1

        // modify product #1 creation x1 to product #2. product #1 has 2 - 1 = 1 left, so legal data
        final var result = underTest.trySave(creation.withProduct(product2));
        assertInstanceOf(Result.Success.class, result);
    }

    @Test
    void changeProductWhileInsufficientMaterialStockGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, 10));
        // 10 material units

        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));
        final var product2 = TestUtils.createTestProduct2();
        productRecipePartService.trySave(new ProductRecipePartEntity(2L, product2, material, 3));

        final var creation = new ProductCreationEntity(1L, product, 5);
        assertInstanceOf(Result.Success.class, underTest.trySave(creation));
        // 10 - 10 = 0 left

        // modify record from product #1 to product #2. it takes 10 materials to make 5 product #1
        // and 15 to make 5 product #2. After taking back the 10 materials, we don't have
        // the 15 required to make 5 product #2, so illegal data
        final var result = underTest.trySave(creation.withProduct(product2));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void changeProductWhileSufficientMaterialStockGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, 20));
        // 20 material units

        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));
        final var product2 = TestUtils.createTestProduct2();
        productRecipePartService.trySave(new ProductRecipePartEntity(2L, product2, material, 3));

        final var creation = new ProductCreationEntity(1L, product, 5);
        assertInstanceOf(Result.Success.class, underTest.trySave(creation));
        // 20 - 10 = 10 left

        // modify record from product #1 to product #2. it takes 10 materials to make 5 product #1
        // and 15 to make 5 product #2. After taking back the 10 materials, we have the 15 required
        // to make 5 product #2 (10 + 10 = 20), so legal data
        final var result = underTest.trySave(creation.withProduct(product2));
        assertInstanceOf(Result.Success.class, result);
    }

    @Test
    void increaseNumberOfSameProductWhileInsufficientMaterialsGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, 10));
        // we have 10 material units

        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));
        final var creation = new ProductCreationEntity(1L, product, 3);
        underTest.trySave(creation);
        // we have 10 - 6 = 4 left

        // modify record from 3 to 20 products requiring additional 34 material units. we obviously don't have enough
        final var result = underTest.trySave(creation.withQuantity(20));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void increaseNumberOfSameProductWhileSufficientMaterialsGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, 10));
        // we have 10 material units

        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));
        final var creation = new ProductCreationEntity(1L, product, 3);
        underTest.trySave(creation);
        // we have 10 - 6 = 4 left

        // modify record from 3 to 20 products requiring additional 2 material units. we have enough
        final var result = underTest.trySave(creation.withQuantity(4));
        assertInstanceOf(Result.Success.class, result);
    }

    @Test
    void deleteProductWhileInsufficientProductStockGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE));
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));

        final var creation = new ProductCreationEntity(1L, product, 1);
        underTest.trySave(creation);
        // 1 product #1
        productSaleService.trySave(new ProductSaleEntity(1L, product, 1, null));
        // 1 - 1 = 0 product #1

        // delete 1x creation. product has 0 - 1 = -1 left, so illegal data
        final var deleted = underTest.tryDelete(creation);
        assertInstanceOf(Result.Rejection.class, deleted);
    }

    @Test
    void deleteProductWhileSufficientProductStockGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        materialImportService.trySave(new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE));
        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 2));

        final var creation = new ProductCreationEntity(1L, product, 1);
        underTest.trySave(creation);
        underTest.trySave(new ProductCreationEntity(2L, product, 2));
        // 3 product #1
        productSaleService.trySave(new ProductSaleEntity(1L, product, 1, null));
        // 3 - 1 = 2 product #1

        // delete 1x creation. product has 2 - 1 = 1 left, so legal data
        final var deleted = underTest.tryDelete(creation);
        assertInstanceOf(Result.Success.class, deleted);
    }

}
