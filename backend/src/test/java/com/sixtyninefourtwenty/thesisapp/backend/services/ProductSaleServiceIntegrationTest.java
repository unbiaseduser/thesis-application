package com.sixtyninefourtwenty.thesisapp.backend.services;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialImportEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductCreationEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductRecipePartEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductSaleEntity;
import com.sixtyninefourtwenty.thesisapp.backend.utils.TestUtils;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductSaleServiceIntegrationTest {

    private final ProductSaleService underTest;
    private final MaterialImportService materialImportService;
    private final ProductRecipePartService productRecipePartService;
    private final ProductCreationService productCreationService;

    @Autowired
    public ProductSaleServiceIntegrationTest(
            ProductSaleService underTest,
            MaterialImportService materialImportService,
            ProductRecipePartService productRecipePartService,
            ProductCreationService productCreationService) {
        this.underTest = underTest;
        this.materialImportService = materialImportService;
        this.productRecipePartService = productRecipePartService;
        this.productCreationService = productCreationService;
    }

    @Test
    void changeProductWhileInsufficientStockForNewProductGetsRejected() {
        final var material = TestUtils.createTestMaterial();
        final var materialImport = new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE);
        materialImportService.trySave(materialImport);

        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 3));
        final var product2 = TestUtils.createTestProduct2();
        productRecipePartService.trySave(new ProductRecipePartEntity(2L, product2, material, 4));
        productCreationService.trySave(new ProductCreationEntity(1L, product, 3));
        productCreationService.trySave(new ProductCreationEntity(2L, product2, 1));
        // 1 product #2

        // sold 3 product #1
        final var sale = new ProductSaleEntity(1L, product, 3, null);
        underTest.trySave(sale);

        // change product #1 to #2. we have 1 - 3 = -2, so illegal data
        final var result = underTest.trySave(sale.withProduct(product2));
        assertInstanceOf(Result.Rejection.class, result);
    }

    @Test
    void changeProductWhileSufficientStockForNewProductGetsAccepted() {
        final var material = TestUtils.createTestMaterial();
        final var materialImport = new MaterialImportEntity(1L, null, material, Integer.MAX_VALUE);
        materialImportService.trySave(materialImport);

        final var product = TestUtils.createTestProduct();
        productRecipePartService.trySave(new ProductRecipePartEntity(1L, product, material, 3));
        final var product2 = TestUtils.createTestProduct2();
        productRecipePartService.trySave(new ProductRecipePartEntity(2L, product2, material, 4));
        productCreationService.trySave(new ProductCreationEntity(1L, product, 3));
        productCreationService.trySave(new ProductCreationEntity(2L, product2, 15));
        // 15 product #2

        // sold 3 product #1
        final var sale = new ProductSaleEntity(1L, product, 3, null);
        underTest.trySave(sale);

        // change product #1 to #2. we have 15 - 3 = 12, so legal data
        final var result = underTest.trySave(sale.withProduct(product2));
        assertInstanceOf(Result.Success.class, result);
    }

}
