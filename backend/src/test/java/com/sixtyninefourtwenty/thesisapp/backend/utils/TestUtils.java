package com.sixtyninefourtwenty.thesisapp.backend.utils;

import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.MaterialEntity;
import com.sixtyninefourtwenty.thesisapp.backend.domain.entities.ProductEntity;

public final class TestUtils {

    private TestUtils() { throw new AssertionError(); }

    public static MaterialEntity createTestMaterial() {
        return new MaterialEntity(1L, "foo", "bar");
    }

    public static MaterialEntity createTestMaterial2() {
        return new MaterialEntity(2L, "baz", "blah");
    }

    public static ProductEntity createTestProduct() {
        return new ProductEntity(1L, "lorem", "ipsum");
    }

    public static ProductEntity createTestProduct2() {
        return new ProductEntity(2L, "dolor", "sit");
    }

}
