pluginManagement {
    repositories {
        gradlePluginPortal()
        maven(url = "https://sandec.jfrog.io/artifactory/repo")
    }
}

rootProject.name = "thesis-application"
include("frontend")
include("backend")
include("common")
