plugins {
    id("java")
}

group = "com.sixtyninefourtwenty.thesisapp"
version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jspecify:jspecify:0.3.0")
    compileOnly("org.projectlombok:lombok:1.18.30")
    annotationProcessor("org.projectlombok:lombok:1.18.30")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.16.0")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}