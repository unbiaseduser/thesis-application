package com.sixtyninefourtwenty.thesisapp.common.data;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Result.Success.class, name = "success"),
        @JsonSubTypes.Type(value = Result.Rejection.class, name = "rejection")
})
public sealed interface Result<T> {

    record Success<T>(T data) implements Result<T> {
    }

    record Rejection<T>(String message) implements Result<T> {
    }

}
