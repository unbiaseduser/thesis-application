package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

import java.time.LocalDateTime;

@With
public record ProductSale(
        long id,
        @NonNull
        Product product,
        int productQuantity,
        @Nullable
        Customer customer,
        @NonNull
        LocalDateTime time
) {

    public ProductSale {
        if (productQuantity <= 0) {
            throw new IllegalArgumentException("Product quantity must not be <= 0");
        }
    }

    public ProductSale(long id, Product product, int productQuantity, Customer customer) {
        this(id, product, productQuantity, customer, LocalDateTime.now());
    }

}
