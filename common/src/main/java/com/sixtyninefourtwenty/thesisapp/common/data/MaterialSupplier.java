package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;

@With
public record MaterialSupplier(
        long id,
        @NonNull
        String name
) {
}
