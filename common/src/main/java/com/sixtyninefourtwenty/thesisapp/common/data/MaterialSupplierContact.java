package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

@With
public record MaterialSupplierContact(
        long id,
        @NonNull
        MaterialSupplier supplier,
        @Nullable
        String phoneNumber,
        @Nullable
        String email,
        @Nullable
        String address
) {

    public MaterialSupplierContact {
        if (phoneNumber == null && email == null && address == null) {
            throw new IllegalArgumentException("One of the contact info properties must be present");
        }
    }

}
