package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;

@With
public record Product(
        long id,
        @NonNull
        String name,
        @NonNull
        String unit
) {
}
