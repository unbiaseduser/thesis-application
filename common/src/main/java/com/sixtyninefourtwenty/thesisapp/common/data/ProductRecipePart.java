package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;

@With
public record ProductRecipePart(
        long id,
        @NonNull
        Product product,
        @NonNull
        Material material,
        int materialQuantity
) {

    public ProductRecipePart {
        if (materialQuantity <= 0) {
            throw new IllegalArgumentException("Material quantity must not be <= 0");
        }
    }

}
