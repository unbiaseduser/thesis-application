package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

@With
public record CustomerContact(
        long id,
        @NonNull
        Customer customer,
        @Nullable
        String phoneNumber,
        @Nullable
        String email,
        @Nullable
        String address
) {

    public CustomerContact {
        if (phoneNumber == null && email == null && address == null) {
            throw new IllegalArgumentException("One of the contact info properties must be present");
        }
    }

}
