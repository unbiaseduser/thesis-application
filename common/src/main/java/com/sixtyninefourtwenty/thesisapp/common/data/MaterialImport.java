package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;

import java.time.LocalDateTime;

@With
public record MaterialImport(
        long id,
        @Nullable
        MaterialSupplier supplier,
        @NonNull
        Material material,
        int quantity,
        @NonNull
        LocalDateTime time
) {

    public MaterialImport {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must not be <= 0");
        }
    }

    public MaterialImport(long id, MaterialSupplier supplier, Material material, int quantity) {
        this(id, supplier, material, quantity, LocalDateTime.now());
    }

}
