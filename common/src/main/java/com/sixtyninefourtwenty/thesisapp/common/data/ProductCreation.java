package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;

import java.time.LocalDateTime;

@With
public record ProductCreation(
        long id,
        @NonNull
        Product product,
        int quantity,
        @NonNull
        LocalDateTime time
) {

    public ProductCreation {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must not be <= 0");
        }
    }

    public ProductCreation(long id, Product product, int quantity) {
        this(id, product, quantity, LocalDateTime.now());
    }

}
