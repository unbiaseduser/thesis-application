package com.sixtyninefourtwenty.thesisapp.common.data;

import lombok.With;
import org.jspecify.annotations.NonNull;

@With
public record Customer(
        long id,
        @NonNull
        String name
) {
}
