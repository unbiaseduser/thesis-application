Spring Boot application I wrote as part of my thesis on building a production & sales website at the [National Economics University](https://neu.edu.vn), IT major.

This repository's only purpose is for history preservation - once it's up, it will *never* be modified again (except for this readme).