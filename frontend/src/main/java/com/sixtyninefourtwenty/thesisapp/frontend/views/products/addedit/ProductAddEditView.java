package com.sixtyninefourtwenty.thesisapp.frontend.views.products.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class ProductAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final Product product;
    private final Consumer<Product> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    @FXML
    private TextField unitTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    protected void initialize() {
        if (product != null) {
            nameTextField.setText(product.name());
            unitTextField.setText(product.unit());
        }
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var nameText = ViewUtils.getTextOrEmpty(nameTextField);
        if (nameText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Name can't be blank",
                    popup::close
            ).getRoot());
            return;
        }
        final var unitText = ViewUtils.getTextOrEmpty(unitTextField);
        if (unitText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Unit can't be blank",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonAction.accept(new Product(product != null ? product.id() : 0, nameText, unitText));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
