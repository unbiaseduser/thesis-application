package com.sixtyninefourtwenty.thesisapp.frontend.views.products.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductsViewModel extends CommonViewModel<Product> {

    private final ProductRepository productRepository;

    public ProductsViewModel(ProductRepository productRepository, List<Product> items) {
        super(items);
        this.productRepository = productRepository;
    }

    public void create(
            Product product,
            Consumer<Product> onResponse,
            Runnable onFailure
    ) {
        productRepository.create(product).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                preFilteredList.add(requireNonNull(response.body()));
                onResponse.accept(requireNonNull(response.body()));
            }

            @Override
            public void onFailure(Call<Product> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void fullUpdate(
            Product product,
            long id,
            Consumer<Product> onResponse,
            Runnable onFailure
    ) {
        productRepository.fullUpdate(product, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                final var newItem = requireNonNull(response.body());
                preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                onResponse.accept(newItem);
            }

            @Override
            public void onFailure(Call<Product> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void delete(
            Product product,
            Runnable onResponse,
            Runnable onFailure,
            Runnable onFailureResponse
    ) {
        productRepository.delete(product.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    preFilteredList.remove(product);
                    onResponse.run();
                } else {
                    onFailureResponse.run();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
