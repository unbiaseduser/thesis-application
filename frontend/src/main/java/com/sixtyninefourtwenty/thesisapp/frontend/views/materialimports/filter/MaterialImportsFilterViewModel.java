package com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

import java.time.LocalDate;

@Setter
@Getter
public class MaterialImportsFilterViewModel {
    @Nullable
    private Material materialSearchQuery;
    @Nullable
    private MaterialSupplier materialSupplierSearchQuery;
    private int quantitySearchQuery = 0;
    @Nullable
    private LocalDate dateSearchQuery;
}
