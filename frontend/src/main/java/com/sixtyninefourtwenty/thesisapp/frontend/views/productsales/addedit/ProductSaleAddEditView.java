package com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.time.LocalDate;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class ProductSaleAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final ProductSale productSale;
    private final Consumer<ProductSale> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<ProductSaleAddEditView> onChooseProductButtonAction;
    private final Consumer<ProductSaleAddEditView> onChooseCustomerButtonAction;
    @Nullable
    private Product pickedProduct;
    @Nullable
    private Customer pickedCustomer;

    @FXML
    private DatePicker datePicker;
    @FXML
    private TextArea customerTextArea;
    @FXML
    private Button chooseCustomerButton;
    @FXML
    private Spinner<Integer> quantitySpinner;
    @FXML
    private Button chooseProductButton;
    @FXML
    private TextArea productTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedProduct(Product product) {
        this.pickedProduct = product;
        productTextArea.setText(product.name());
    }

    public void setPickedCustomer(@Nullable Customer customer) {
        this.pickedCustomer = customer;
        customerTextArea.setText(customer != null ? customer.name() : null);
    }

    @FXML
    protected void initialize() {
        if (productSale != null) {
            setPickedProduct(productSale.product());
            setPickedCustomer(productSale.customer());
            quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, productSale.productQuantity()));
            datePicker.setValue(productSale.time().toLocalDate());
        } else {
            quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE));
            datePicker.setValue(LocalDate.now());
        }
        chooseProductButton.setOnAction(event -> onChooseProductButtonAction.accept(this));
        chooseCustomerButton.setOnAction(event -> onChooseCustomerButtonAction.accept(this));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        if (pickedProduct == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Product not picked",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonAction.accept(new ProductSale(
                productSale != null ? productSale.id() : 0,
                pickedProduct,
                quantitySpinner.getValue(),
                pickedCustomer,
                datePicker.getValue().atStartOfDay()
        ));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
