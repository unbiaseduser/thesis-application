package com.sixtyninefourtwenty.thesisapp.frontend.stockgetter;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductStockGetter {

    @GET("productstock/{id}")
    Call<Long> calculateStockForProductId(@Path("id") long productId);

    static ProductStockGetter create() {
        return Utils.createRetrofitBackedRepository(ProductStockGetter.class);
    }

}
