package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class MaterialSupplierContactsFilterView extends CommonDialogView {

    private final MaterialSupplierContactsFilterViewModel materialSupplierContactsFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<MaterialSupplierContact>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<MaterialSupplierContactsFilterView> onChooseSupplierButtonAction;
    @FXML
    private Button clearSupplierButton;
    @Nullable
    private MaterialSupplier pickedSupplier;

    public static MaterialSupplierContactsFilterView create(
            MaterialSupplierContactsFilterViewModel materialSupplierContactsFilterViewModel,
            Consumer<? super Collection<? extends Predicate<MaterialSupplierContact>>> onOkButtonAction,
            Runnable onCancelButtonAction,
            Consumer<MaterialSupplierContactsFilterView> onChooseSupplierButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_supplier_contact_filter");
        fxmlLoader.setControllerFactory(clazz -> new MaterialSupplierContactsFilterView(materialSupplierContactsFilterViewModel, onOkButtonAction, onCancelButtonAction, onChooseSupplierButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private Button chooseSupplierButton;
    @FXML
    private TextArea supplierTextArea;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField phoneNumberTextField;
    @FXML
    private AnchorPane root;

    public void setPickedSupplier(@Nullable MaterialSupplier materialSupplier) {
        this.pickedSupplier = materialSupplier;
        supplierTextArea.setText(materialSupplier != null ? materialSupplier.name() : null);
    }

    @FXML
    private void initialize() {
        setPickedSupplier(materialSupplierContactsFilterViewModel.getSupplierSearchQuery());
        addressTextField.setText(materialSupplierContactsFilterViewModel.getAddressSearchQuery());
        emailTextField.setText(materialSupplierContactsFilterViewModel.getEmailSearchQuery());
        phoneNumberTextField.setText(materialSupplierContactsFilterViewModel.getPhoneNumberQuery());
        chooseSupplierButton.setOnAction(event -> onChooseSupplierButtonAction.accept(this));
        clearSupplierButton.setOnAction(event -> setPickedSupplier(null));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {

        final BiPredicate<@Nullable String, @Nullable String> f = (arg1, arg2) -> {
            if (arg1 == null && arg2 == null) {
                return true;
            }
            if (arg1 == null || arg2 == null) {
                return false;
            }
            return arg1.contains(arg2);
        };

        final var filters = new ArrayList<Predicate<MaterialSupplierContact>>(4);
        if (pickedSupplier != null) {
            filters.add(s -> pickedSupplier.equals(s.supplier()));
        }
        final var addressText = addressTextField.getText();
        if (!Utils.isNullOrBlank(addressText)) {
            filters.add(s -> f.test(s.address(), addressText));
        }
        final var emailText = emailTextField.getText();
        if (!Utils.isNullOrBlank(emailText)) {
            filters.add(s -> f.test(s.email(), emailText));
        }
        final var phoneNumberText = phoneNumberTextField.getText();
        if (!Utils.isNullOrBlank(phoneNumberText)) {
            filters.add(s -> f.test(s.phoneNumber(), phoneNumberText));
        }
        materialSupplierContactsFilterViewModel.setSupplierSearchQuery(pickedSupplier);
        materialSupplierContactsFilterViewModel.setAddressSearchQuery(addressText);
        materialSupplierContactsFilterViewModel.setEmailSearchQuery(emailText);
        materialSupplierContactsFilterViewModel.setPhoneNumberQuery(phoneNumberText);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
