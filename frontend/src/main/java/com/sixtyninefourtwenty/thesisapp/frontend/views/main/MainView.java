package com.sixtyninefourtwenty.thesisapp.frontend.views.main;

import com.sixtyninefourtwenty.thesisapp.common.data.*;
import com.sixtyninefourtwenty.thesisapp.frontend.*;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.*;
import com.sixtyninefourtwenty.thesisapp.frontend.stockgetter.MaterialStockGetter;
import com.sixtyninefourtwenty.thesisapp.frontend.stockgetter.ProductStockGetter;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import com.sixtyninefourtwenty.thesisapp.frontend.views.SimpleMessageDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.data.CustomerContactsView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.data.CustomerContactsViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.filter.CustomerContactsFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customers.data.CustomersView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customers.data.CustomersViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customers.filter.CustomerFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces.View;
import com.sixtyninefourtwenty.thesisapp.frontend.views.login.LoginValidator;
import com.sixtyninefourtwenty.thesisapp.frontend.views.login.LoginView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.data.MaterialImportsView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.data.MaterialImportsViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.filter.MaterialImportsFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materials.data.MaterialsView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materials.data.MaterialsViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materials.filter.MaterialsFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.data.MaterialSupplierContactsView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.data.MaterialSupplierContactsViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.filter.MaterialSupplierContactsFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.data.MaterialSuppliersView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.data.MaterialSuppliersViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.filter.MaterialSuppliersFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.data.ProductCreationsView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.data.ProductCreationsViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.filter.ProductCreationsFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.data.ProductRecipesView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.data.ProductRecipesViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.filter.ProductRecipesFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.products.data.ProductsView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.products.data.ProductsViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.products.filter.ProductsFilterViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.data.ProductSalesView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.data.ProductSalesViewModel;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.filter.ProductSalesFilterViewModel;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static java.util.Objects.requireNonNull;

@RequiredArgsConstructor
public class MainView implements View {

    private final ExcelExporter excelExporter;
    private final LoginValidator loginValidator;
    private final ScreenSwitcher screenSwitcher;
    private final MainViewModel mainViewModel;
    private final CustomerRepository customerRepository;
    private final CustomerContactRepository customerContactRepository;
    private final MaterialRepository materialRepository;
    private final MaterialImportRepository materialImportRepository;
    private final MaterialSupplierRepository materialSupplierRepository;
    private final MaterialSupplierContactRepository materialSupplierContactRepository;
    private final ProductRepository productRepository;
    private final ProductRecipePartRepository productRecipePartRepository;
    private final ProductCreationRepository productCreationRepository;
    private final ProductSaleRepository productSaleRepository;
    private final MaterialStockGetter materialStockGetter;
    private final ProductStockGetter productStockGetter;
    private PopupHandler popupHandler;

    @FXML
    private VBox leftView;
    @FXML
    private VBox topView;
    @FXML
    private Button logoutButton;
    @FXML
    private Text titleText;
    @FXML
    private Pane root;
    @FXML
    private StackPane content;
    @FXML
    private Button productSalesButton;
    @FXML
    private Button productRecipesButton;
    @FXML
    private Button productCreationsButton;
    @FXML
    private Button productsButton;
    @FXML
    private Button materialSupplierContactsButton;
    @FXML
    private Button materialSuppliersButton;
    @FXML
    private Button materialImportsButton;
    @FXML
    private Button materialsButton;
    @FXML
    private Button customerContactsButton;
    @FXML
    private Button customersButton;

    @FXML
    private void initialize() {
        popupHandler = new PopupHandler(content);
        logoutButton.setOnAction(event -> {
            popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                    "Do you want to log out?",
                    () -> mainViewModel.setScreen(Screen.LOGIN),
                    popup::close
            ).getRoot());
        });
        customersButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.CUSTOMERS));
        customerContactsButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.CUSTOMER_CONTACTS));
        materialsButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.MATERIALS));
        materialImportsButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.MATERIAL_IMPORTS));
        materialSuppliersButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.MATERIAL_SUPPLIERS));
        materialSupplierContactsButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.MATERIAL_SUPPLIER_CONTACTS));
        productsButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.PRODUCTS));
        productCreationsButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.PRODUCT_CREATIONS));
        productRecipesButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.PRODUCT_RECIPES));
        productSalesButton.setOnAction(event -> screenSwitcher.switchScreen(Screen.PRODUCT_SALES));
        mainViewModel.setScreenListener(screen -> {
            if (screen == Screen.LOGIN) {
                topView.setVisible(false);
                leftView.setVisible(false);
            } else {
                topView.setVisible(true);
                leftView.setVisible(true);
            }
            switch (screen) {
                case LOGIN -> {
                    final var fxmlLoader = Utils.fxmlLoader("login");
                    fxmlLoader.setControllerFactory(clazz -> new LoginView(popupHandler, loginValidator, () -> screenSwitcher.switchScreen(Screen.HOME)));
                    Utils.loadQuietly(fxmlLoader);
                    loadAndDisplayView("Login", fxmlLoader.<LoginView>getController());
                }
                case HOME -> {
                    final var text = new Text("Please choose an item from the sidebar.");
                    text.setFont(new Font(14));
                    loadAndDisplayView("Menu", text);
                }
                case CUSTOMERS -> {
                    customerRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Customers", new CustomersView(excelExporter, items, popupHandler, new CustomersViewModel(customerRepository, items), new CustomerFilterViewModel(), customerContactRepository));
                        }

                        @Override
                        public void onFailure(Call<List<Customer>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case CUSTOMER_CONTACTS -> {
                    customerContactRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<CustomerContact>> call, Response<List<CustomerContact>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Customer contacts", new CustomerContactsView(excelExporter, items, popupHandler, customerRepository, new CustomerContactsViewModel(customerContactRepository, items), new CustomerContactsFilterViewModel()));
                        }

                        @Override
                        public void onFailure(Call<List<CustomerContact>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case MATERIALS -> {
                    materialRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<Material>> call, Response<List<Material>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Materials", new MaterialsView(excelExporter, items, popupHandler, materialStockGetter, new MaterialsViewModel(materialRepository, items), new MaterialsFilterViewModel()));
                        }

                        @Override
                        public void onFailure(Call<List<Material>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case MATERIAL_IMPORTS -> {
                    materialImportRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<MaterialImport>> call, Response<List<MaterialImport>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Material imports", new MaterialImportsView(excelExporter, items, popupHandler, new MaterialImportsViewModel(materialImportRepository, items), new MaterialImportsFilterViewModel(), materialRepository, materialSupplierRepository));
                        }

                        @Override
                        public void onFailure(Call<List<MaterialImport>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case MATERIAL_SUPPLIERS -> {
                    materialSupplierRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<MaterialSupplier>> call, Response<List<MaterialSupplier>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Material suppliers", new MaterialSuppliersView(excelExporter, items, popupHandler, new MaterialSuppliersViewModel(materialSupplierRepository, items), new MaterialSuppliersFilterViewModel(), materialSupplierContactRepository));
                        }

                        @Override
                        public void onFailure(Call<List<MaterialSupplier>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case MATERIAL_SUPPLIER_CONTACTS -> {
                    materialSupplierContactRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<MaterialSupplierContact>> call, Response<List<MaterialSupplierContact>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Material supplier contacts", new MaterialSupplierContactsView(excelExporter, items, popupHandler, new MaterialSupplierContactsViewModel(materialSupplierContactRepository, items), new MaterialSupplierContactsFilterViewModel(), materialSupplierRepository));
                        }

                        @Override
                        public void onFailure(Call<List<MaterialSupplierContact>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case PRODUCTS -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Products", new ProductsView(excelExporter, items, popupHandler, productStockGetter, new ProductsViewModel(productRepository, items), new ProductsFilterViewModel()));
                        }

                        @Override
                        public void onFailure(Call<List<Product>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case PRODUCT_RECIPES -> {
                    productRecipePartRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<ProductRecipePart>> call, Response<List<ProductRecipePart>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Product recipes", new ProductRecipesView(excelExporter, items, popupHandler, new ProductRecipesViewModel(productRecipePartRepository, items), new ProductRecipesFilterViewModel(), productRepository, materialRepository));
                        }

                        @Override
                        public void onFailure(Call<List<ProductRecipePart>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case PRODUCT_CREATIONS -> {
                    productCreationRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<ProductCreation>> call, Response<List<ProductCreation>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Product creations", new ProductCreationsView(excelExporter, items, popupHandler, new ProductCreationsViewModel(productCreationRepository, items), new ProductCreationsFilterViewModel(), productRepository));
                        }

                        @Override
                        public void onFailure(Call<List<ProductCreation>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
                case PRODUCT_SALES -> {
                    productSaleRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(Call<List<ProductSale>> call, Response<List<ProductSale>> response) {
                            final var items = requireNonNull(response.body());
                            loadAndDisplayView("Product sales", new ProductSalesView(excelExporter, items, popupHandler, new ProductSalesViewModel(productSaleRepository, items), new ProductSalesFilterViewModel(), productRepository, customerRepository));
                        }

                        @Override
                        public void onFailure(Call<List<ProductSale>> call, Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
            }
        });
        screenSwitcher.addListener(mainViewModel::setScreen);
    }

    private void loadAndDisplayView(String title, Node node) {
        if (popupHandler.isLatestPopupShowing()) {
            return;
        }
        Platform.runLater(() -> {
            titleText.setText(title);
            ViewUtils.resetChildren(content, node);
        });
    }

    private void loadAndDisplayView(String title, View view) {
        loadAndDisplayView(title, view.getRoot());
    }

    private void showServerFailurePopup() {
        Platform.runLater(popupHandler::createCantConnectToServerPopupAndShow);
    }

    @Override
    public Node getRoot() {
        return root;
    }
}
