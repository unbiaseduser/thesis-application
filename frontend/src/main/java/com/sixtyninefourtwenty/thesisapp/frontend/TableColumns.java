package com.sixtyninefourtwenty.thesisapp.frontend;

import com.sixtyninefourtwenty.thesisapp.common.data.*;
import javafx.scene.control.TableColumn;

import java.util.List;

import static java.util.Objects.requireNonNullElse;

public final class TableColumns {

    private TableColumns() {}

    public static List<TableColumn<CustomerContact, ?>> forCustomerContact() {
        return List.of(
                ViewUtils.createTableColumn("ID", CustomerContact::id),
                ViewUtils.createTableColumn("Customer name", cc -> cc.customer().name()),
                ViewUtils.createTableColumn("Phone number", cc -> requireNonNullElse(cc.phoneNumber(), "N/A")),
                ViewUtils.createTableColumn("Email", cc -> requireNonNullElse(cc.email(), "N/A")),
                ViewUtils.createTableColumn("Address", cc -> requireNonNullElse(cc.address(), "N/A"))
        );
    }

    public static List<TableColumn<Customer, ?>> forCustomer() {
        return List.of(
                ViewUtils.createTableColumn("ID", Customer::id),
                ViewUtils.createTableColumn("Name", Customer::name)
        );
    }

    public static List<TableColumn<Material, ?>> forMaterial() {
        return List.of(
                ViewUtils.createTableColumn("ID", Material::id),
                ViewUtils.createTableColumn("Name", Material::name),
                ViewUtils.createTableColumn("Unit", Material::unit)
        );
    }

    public static List<TableColumn<MaterialImport, ?>> forMaterialImport() {
        return List.of(
                ViewUtils.createTableColumn("ID", MaterialImport::id),
                ViewUtils.createTableColumn("Supplier name", m -> {
                    final var supplier = m.supplier();
                    return supplier != null ? supplier.name() : "N/A";
                }),
                ViewUtils.createTableColumn("Material name", m -> m.material().name()),
                ViewUtils.createTableColumn("Quantity", MaterialImport::quantity),
                ViewUtils.createTableColumn("Date", m -> m.time().toLocalDate())
        );
    }

    public static List<TableColumn<MaterialSupplier, ?>> forMaterialSupplier() {
        return List.of(
                ViewUtils.createTableColumn("ID", MaterialSupplier::id),
                ViewUtils.createTableColumn("Name", MaterialSupplier::name)
        );
    }

    public static List<TableColumn<MaterialSupplierContact, ?>> forMaterialSupplierContact() {
        return List.of(
                ViewUtils.createTableColumn("ID", MaterialSupplierContact::id),
                ViewUtils.createTableColumn("Supplier name", c -> c.supplier().name()),
                ViewUtils.createTableColumn("Phone number", c -> requireNonNullElse(c.phoneNumber(), "N/A")),
                ViewUtils.createTableColumn("Email", c -> requireNonNullElse(c.email(), "N/A")),
                ViewUtils.createTableColumn("Address", c -> requireNonNullElse(c.address(), "N/A"))
        );
    }

    public static List<TableColumn<Product, ?>> forProduct() {
        return List.of(
                ViewUtils.createTableColumn("ID", Product::id),
                ViewUtils.createTableColumn("Name", Product::name),
                ViewUtils.createTableColumn("Unit", Product::unit)
        );
    }

    public static List<TableColumn<ProductCreation, ?>> forProductCreation() {
        return List.of(
                ViewUtils.createTableColumn("ID", ProductCreation::id),
                ViewUtils.createTableColumn("Product name", c -> c.product().name()),
                ViewUtils.createTableColumn("Quantity", ProductCreation::quantity),
                ViewUtils.createTableColumn("Date", c -> c.time().toLocalDate())
        );
    }

    public static List<TableColumn<ProductRecipePart, ?>> forProductRecipe() {
        return List.of(
                ViewUtils.createTableColumn("ID", ProductRecipePart::id),
                ViewUtils.createTableColumn("Product name", p -> p.product().name()),
                ViewUtils.createTableColumn("Material name", p -> p.material().name()),
                ViewUtils.createTableColumn("Material quantity", ProductRecipePart::materialQuantity)
        );
    }

    public static List<TableColumn<ProductSale, ?>> forProductSale() {
        return List.of(
                ViewUtils.createTableColumn("ID", ProductSale::id),
                ViewUtils.createTableColumn("Product name", ps -> ps.product().name()),
                ViewUtils.createTableColumn("Quantity", ProductSale::productQuantity),
                ViewUtils.createTableColumn("Customer name", ps -> {
                    final var customer = ps.customer();
                    return customer != null ? customer.name() : "N/A";
                }),
                ViewUtils.createTableColumn("Date", ps -> ps.time().toLocalDate())
        );
    }

}
