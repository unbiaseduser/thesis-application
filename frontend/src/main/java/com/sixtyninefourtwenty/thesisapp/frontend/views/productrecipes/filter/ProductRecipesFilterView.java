package com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductRecipePart;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class ProductRecipesFilterView extends CommonDialogView {

    private final ProductRecipesFilterViewModel productRecipesFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<ProductRecipePart>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<? super ProductRecipesFilterView> onChooseProductButtonAction;
    private final Consumer<? super ProductRecipesFilterView> onChooseMaterialButtonAction;
    @FXML
    private Button clearMaterialButton;
    @FXML
    private Button clearProductButton;
    @Nullable
    private Product pickedProduct;
    @Nullable
    private Material pickedMaterial;

    public static ProductRecipesFilterView create(
            ProductRecipesFilterViewModel productRecipesFilterViewModel,
            Consumer<? super Collection<? extends Predicate<ProductRecipePart>>> onOkButtonAction,
            Runnable onCancelButtonAction,
            Consumer<? super ProductRecipesFilterView> onChooseProductButtonAction,
            Consumer<? super ProductRecipesFilterView> onChooseMaterialButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_recipe_filter");
        fxmlLoader.setControllerFactory(clazz -> new ProductRecipesFilterView(productRecipesFilterViewModel, onOkButtonAction, onCancelButtonAction, onChooseProductButtonAction, onChooseMaterialButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private Spinner<Integer> materialQuantitySpinner;
    @FXML
    private Button chooseMaterialButton;
    @FXML
    private TextArea materialTextArea;
    @FXML
    private Button chooseProductButton;
    @FXML
    private TextArea productTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedProduct(@Nullable Product product) {
        this.pickedProduct = product;
        productTextArea.setText(product != null ? product.name() : null);
    }

    public void setPickedMaterial(@Nullable Material material) {
        this.pickedMaterial = material;
        materialTextArea.setText(material != null ? material.name() : null);
    }

    @FXML
    private void initialize() {
        setPickedMaterial(productRecipesFilterViewModel.getMaterialSearchQuery());
        setPickedProduct(productRecipesFilterViewModel.getProductSearchQuery());
        materialQuantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, productRecipesFilterViewModel.getMaterialQuantitySearchQuery()));
        chooseMaterialButton.setOnAction(event -> onChooseMaterialButtonAction.accept(this));
        chooseProductButton.setOnAction(event -> onChooseProductButtonAction.accept(this));
        clearProductButton.setOnAction(event -> setPickedProduct(null));
        clearMaterialButton.setOnAction(event -> setPickedMaterial(null));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<ProductRecipePart>>(3);
        if (pickedMaterial != null) {
            filters.add(p -> pickedMaterial.equals(p.material()));
        }
        if (pickedProduct != null) {
            filters.add(p -> pickedProduct.equals(p.product()));
        }
        final var quantity = materialQuantitySpinner.getValue();
        if (quantity > 0) {
            filters.add(p -> p.materialQuantity() == quantity);
        }
        productRecipesFilterViewModel.setMaterialSearchQuery(pickedMaterial);
        productRecipesFilterViewModel.setProductSearchQuery(pickedProduct);
        productRecipesFilterViewModel.setMaterialQuantitySearchQuery(quantity);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
