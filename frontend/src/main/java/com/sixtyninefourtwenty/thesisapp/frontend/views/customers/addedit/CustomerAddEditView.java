package com.sixtyninefourtwenty.thesisapp.frontend.views.customers.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class CustomerAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final Customer customer;
    private final Consumer<Customer> onOkButtonClick;
    private final Runnable onCancelButtonClick;

    @FXML
    private AnchorPane root;
    @FXML
    private TextField nameTextField;

    @FXML
    protected void initialize() {
        if (customer != null) {
            nameTextField.setText(customer.name());
        }
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var nameText = ViewUtils.getTextOrEmpty(nameTextField);
        if (nameText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Name can't be blank",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonClick.accept(new Customer(customer != null ? customer.id() : 0, nameText));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }

}
