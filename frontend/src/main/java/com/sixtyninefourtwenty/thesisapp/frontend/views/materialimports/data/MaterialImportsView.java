package com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialSupplierRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.addedit.MaterialImportAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.filter.MaterialImportsFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.filter.MaterialImportsFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class MaterialImportsView extends CommonDataView<MaterialImport> {

    private final MaterialImportsViewModel materialImportsViewModel;
    private final MaterialImportsFilterViewModel materialImportsFilterViewModel;
    private final MaterialRepository materialRepository;
    private final MaterialSupplierRepository materialSupplierRepository;

    public MaterialImportsView(
            ExcelExporter excelExporter,
            List<MaterialImport> items,
            PopupHandler popupHandler,
            MaterialImportsViewModel materialImportsViewModel,
            MaterialImportsFilterViewModel materialImportsFilterViewModel,
            MaterialRepository materialRepository,
            MaterialSupplierRepository materialSupplierRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forMaterialImport(),
                items
        );
        this.materialImportsViewModel = materialImportsViewModel;
        this.materialImportsFilterViewModel = materialImportsFilterViewModel;
        this.materialRepository = materialRepository;
        this.materialSupplierRepository = materialSupplierRepository;
        materialImportsViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showMaterialImportAddEditView(null, materialImport -> {
            materialImportsViewModel.tryCreate(
                    materialImport,
                    i -> {
                    },
                    message -> {
                        final var popup = popupHandler.createPopup();
                        popup.setContentView(GenericErrorView.create(
                                message, popup::close
                        ).getRoot());
                        popup.show();
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, MaterialImport item) {
        showMaterialImportAddEditView(item, materialImport -> {
            materialImportsViewModel.tryFullUpdate(
                    materialImport,
                    materialImport.id(),
                    i -> {
                    },
                    message -> {
                        final var popup = popupHandler.createPopup();
                        popup.setContentView(GenericErrorView.create(
                                message, popup::close
                        ).getRoot());
                        popup.show();
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showMaterialImportAddEditView(
            @Nullable MaterialImport materialImport,
            Consumer<MaterialImport> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_import_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new MaterialImportAddEditView(popupHandler,
                materialImport,
                onOkButtonClick,
                this::clearBottomPanel,
                view -> {
                    materialRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Material>> call, @NonNull Response<List<Material>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterial(),
                                    (event, material) -> {
                                        view.setPickedMaterial(material);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Material>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                },
                view -> {
                    materialSupplierRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<MaterialSupplier>> call, @NonNull Response<List<MaterialSupplier>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterialSupplier(),
                                    (event, materialSupplier) -> {
                                        view.setPickedMaterialSupplier(materialSupplier);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<MaterialSupplier>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<MaterialImportAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, MaterialImport item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    materialImportsViewModel.tryDelete(
                            item,
                            () -> {
                            },
                            msg -> {
                                Platform.runLater(() -> {
                                    final var popup2 = popupHandler.createPopup();
                                    popup2.setContentView(GenericErrorView.create(
                                            msg, popup2::close
                                    ).getRoot());
                                    popup2.show();
                                });
                            },
                            () -> {
                            },
                            this::showServerFailurePopup
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = MaterialImportsFilterView.create(
                materialImportsFilterViewModel,
                filters -> {
                    materialImportsViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel,
                view -> {
                    materialRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Material>> call, @NonNull Response<List<Material>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterial(),
                                    (event, material) -> {
                                        view.setPickedMaterial(material);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Material>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                },
                view -> {
                    materialSupplierRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<MaterialSupplier>> call, @NonNull Response<List<MaterialSupplier>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterialSupplier(),
                                    (event, materialSupplier) -> {
                                        view.setPickedMaterialSupplier(materialSupplier);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<MaterialSupplier>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<MaterialImport> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "material_imports_" + System.currentTimeMillis(),
                                "MaterialImports",
                                List.of("ID", "Supplier name", "Material name", "Quantity", "Date"),
                                finalItems,
                                item -> {
                                    final var supplier = item.supplier();
                                    return List.of(
                                            Long.toString(item.id()),
                                            supplier != null ? supplier.name() : "N/A",
                                            item.material().name(),
                                            Integer.toString(item.quantity()),
                                            item.time().toLocalDate().toString()
                                    );
                                }
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
