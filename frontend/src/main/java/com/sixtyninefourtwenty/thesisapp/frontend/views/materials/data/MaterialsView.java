package com.sixtyninefourtwenty.thesisapp.frontend.views.materials.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.frontend.stockgetter.MaterialStockGetter;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.data.MaterialAndStock;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materials.filter.MaterialsFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materials.addedit.MaterialAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materials.filter.MaterialsFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import org.jspecify.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class MaterialsView extends CommonDataView<Material> {

    private final MaterialStockGetter materialStockGetter;
    private final MaterialsViewModel materialsViewModel;
    private final MaterialsFilterViewModel materialsFilterViewModel;

    public MaterialsView(
            ExcelExporter excelExporter,
            List<Material> items,
            PopupHandler popupHandler,
            MaterialStockGetter materialStockGetter,
            MaterialsViewModel materialsViewModel,
            MaterialsFilterViewModel materialsFilterViewModel
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forMaterial(),
                items
        );
        this.materialStockGetter = materialStockGetter;
        this.materialsViewModel = materialsViewModel;
        this.materialsFilterViewModel = materialsFilterViewModel;
        materialsViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));

        final var stockButton = new Button("Stock");
        stockButton.setOnAction(event -> {
            if (getItems().isEmpty()) {
                popupHandler.createNoItemsPopupAndShow();
                return;
            }
            final var selectedItems = getSelectedItems();
            final var finalItems = selectedItems.isEmpty() ? getItems() : selectedItems;
            final var itemsWithStock = new ArrayList<MaterialAndStock>(finalItems.size());
            for (int i = 0; i < finalItems.size(); i++) {
                final var selectedItem = finalItems.get(i);
                try {
                    final var stockResponse = materialStockGetter.calculateStockForMaterialId(selectedItem.id()).execute();
                    itemsWithStock.add(new MaterialAndStock(selectedItem, requireNonNull(stockResponse.body())));
                    if (i == finalItems.size() - 1) {
                        final var popup = popupHandler.createPopup();
                        final var dialog = DataDialog.create(
                                List.of(
                                        ViewUtils.createTableColumn("ID", m -> m.material().id()),
                                        ViewUtils.createTableColumn("Name", m -> m.material().name()),
                                        ViewUtils.createTableColumn("Unit", m -> m.material().unit()),
                                        ViewUtils.createTableColumn("Stock", MaterialAndStock::stock)
                                ),
                                itemsWithStock,
                                popup::close
                        );
                        final var reportButton = new Button("Report");
                        reportButton.setOnAction(ae -> {
                            popup.close();
                            popupHandler.createPopupAndShow(popup1 -> new SimpleMessageDialogView(
                                    "Do you want to save a report of these items?",
                                    () -> {
                                        popup1.close();
                                        try {
                                            excelExporter.exportExcel(
                                                    "materials_and_stock_" + System.currentTimeMillis(),
                                                    "MaterialsAndStock",
                                                    List.of("ID", "Name", "Unit", "Stock"),
                                                    dialog.getItems(),
                                                    item -> List.of(
                                                            Long.toString(item.material().id()),
                                                            item.material().name(),
                                                            item.material().unit(),
                                                            Long.toString(item.stock())
                                                    )
                                            );
                                        } catch (IOException e) {
                                            popupHandler.createPopupAndShow(popup2 -> GenericErrorView.create(
                                                    "Can't create report.",
                                                    popup2::close
                                            ).getRoot());
                                        }
                                    },
                                    popup1::close
                            ).getRoot());
                        });
                        dialog.addButtons(reportButton);
                        popup.setContentView(dialog.getRoot());
                        popup.show();
                    }
                } catch (IOException ignored) {

                }
            }
        });
        addButton(stockButton);
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showAddEditMaterialView(null, material -> {
            materialsViewModel.create(
                    material,
                    m -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, Material item) {
        showAddEditMaterialView(item, material -> {
            materialsViewModel.fullUpdate(
                    material,
                    material.id(),
                    m -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showAddEditMaterialView(@Nullable Material material, Consumer<Material> onOkButtonAction) {
        final var fxmlLoader = Utils.fxmlLoader("material_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new MaterialAddEditView(popupHandler, material, onOkButtonAction, this::clearBottomPanel));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<MaterialAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, Material item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    materialsViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup,
                            () -> {
                                Platform.runLater(() -> {
                                    final var popup2 = popupHandler.createPopup();
                                    popup2.setContentView(GenericErrorView.create(
                                            "Can't delete this item.\nIt might have some data associated with it.",
                                            popup2::close
                                    ).getRoot());
                                    popup2.show();
                                });
                            }
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = MaterialsFilterView.create(
                materialsFilterViewModel,
                filters -> {
                    materialsViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<Material> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "materials_" + System.currentTimeMillis(),
                                "Materials",
                                List.of("ID", "Name", "Unit"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.name(),
                                        item.unit()
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
