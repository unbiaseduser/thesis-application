package com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.data;

import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.CustomerContactRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class CustomerContactsViewModel extends CommonViewModel<CustomerContact> {

    private final CustomerContactRepository customerContactRepository;

    public CustomerContactsViewModel(
            CustomerContactRepository customerContactRepository,
            List<CustomerContact> items
    ) {
        super(items);
        this.customerContactRepository = customerContactRepository;
    }

    public void create(
            CustomerContact contact,
            Consumer<CustomerContact> onResponse,
            Runnable onFailure
    ) {
        customerContactRepository.create(contact).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<CustomerContact> call, Response<CustomerContact> response) {
                preFilteredList.add(requireNonNull(response.body()));
                onResponse.accept(requireNonNull(response.body()));
            }

            @Override
            public void onFailure(Call<CustomerContact> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void fullUpdate(
            CustomerContact contact,
            long id,
            Consumer<CustomerContact> onResponse,
            Runnable onFailure
    ) {
        customerContactRepository.fullUpdate(contact, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<CustomerContact> call, Response<CustomerContact> response) {
                final var newItem = requireNonNull(response.body());
                preFilteredList.replaceAll(c -> c.id() != newItem.id() ? c : newItem);
                onResponse.accept(newItem);
            }

            @Override
            public void onFailure(Call<CustomerContact> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void delete(
            CustomerContact contact,
            Runnable onResponse,
            Runnable onFailure
    ) {
        customerContactRepository.delete(contact.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                preFilteredList.remove(contact);
                onResponse.run();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
