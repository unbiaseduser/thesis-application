package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class CommonViewModel<T> {

    public CommonViewModel(List<T> items) {
        preFilteredList.addListener((ListChangeListener<T>) c -> {
            final var l = Utils.<T>cast(c.getList());
            finalList.setAll(Utils.filterList(l, filters.getValue()));
        });
        filters.addListener((ListChangeListener<Predicate<T>>) c -> {
            final var l = Utils.<Predicate<T>>cast(c.getList());
            finalList.setAll(Utils.filterList(preFilteredList.getValue(), l));
        });
        preFilteredList.setAll(items);
    }

    protected final ListProperty<T> preFilteredList = new SimpleListProperty<>(FXCollections.observableArrayList());

    protected final ListProperty<Predicate<T>> filters = new SimpleListProperty<>(FXCollections.observableArrayList());

    protected final ListProperty<T> finalList = new SimpleListProperty<>(FXCollections.observableArrayList());

    public void addDataListener(ListChangeListener<? super T> listener) {
        finalList.addListener(listener);
    }

    public void setFilters(Collection<? extends Predicate<T>> filters) {
        this.filters.setAll(filters);
    }

    public void clearFilters() {
        setFilters(List.of());
    }

}
