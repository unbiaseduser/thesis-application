package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.data;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialSupplierRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.addedit.MaterialSupplierContactAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.filter.MaterialSupplierContactsFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.filter.MaterialSupplierContactsFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;

public class MaterialSupplierContactsView extends CommonDataView<MaterialSupplierContact> {

    private final MaterialSupplierContactsViewModel materialSupplierContactsViewModel;
    private final MaterialSupplierContactsFilterViewModel materialSupplierContactsFilterViewModel;
    private final MaterialSupplierRepository materialSupplierRepository;

    public MaterialSupplierContactsView(
            ExcelExporter excelExporter,
            List<MaterialSupplierContact> items,
            PopupHandler popupHandler,
            MaterialSupplierContactsViewModel materialSupplierContactsViewModel,
            MaterialSupplierContactsFilterViewModel materialSupplierContactsFilterViewModel,
            MaterialSupplierRepository materialSupplierRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forMaterialSupplierContact(),
                items
        );
        this.materialSupplierContactsViewModel = materialSupplierContactsViewModel;
        this.materialSupplierContactsFilterViewModel = materialSupplierContactsFilterViewModel;
        this.materialSupplierRepository = materialSupplierRepository;
        materialSupplierContactsViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showMaterialSupplierContactAddEditView(null, contact -> {
            materialSupplierContactsViewModel.create(
                    contact,
                    c -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, MaterialSupplierContact item) {
        showMaterialSupplierContactAddEditView(item, contact -> {
            materialSupplierContactsViewModel.fullUpdate(
                    contact,
                    contact.id(),
                    c -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showMaterialSupplierContactAddEditView(
            @Nullable MaterialSupplierContact contact,
            Consumer<MaterialSupplierContact> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_supplier_contact_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new MaterialSupplierContactAddEditView(popupHandler, contact, onOkButtonClick,
                this::clearBottomPanel,
                view -> {
                    materialSupplierRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<MaterialSupplier>> call, @NonNull Response<List<MaterialSupplier>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterialSupplier(),
                                    (event, materialSupplier) -> {
                                        view.setPickedSupplier(materialSupplier);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            Platform.runLater(() -> setRightPanelContentView(v.getRoot()));
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<MaterialSupplier>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<MaterialSupplierContactAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, MaterialSupplierContact item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    materialSupplierContactsViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = MaterialSupplierContactsFilterView.create(
                materialSupplierContactsFilterViewModel,
                filters -> {
                    materialSupplierContactsViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel,
                view -> {
                    materialSupplierRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<MaterialSupplier>> call, @NonNull Response<List<MaterialSupplier>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterialSupplier(),
                                    (event, materialSupplier) -> {
                                        view.setPickedSupplier(materialSupplier);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            Platform.runLater(() -> setRightPanelContentView(v.getRoot()));
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<MaterialSupplier>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<MaterialSupplierContact> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "material_supplier_contacts_" + System.currentTimeMillis(),
                                "MaterialSupplierContacts",
                                List.of("ID", "Supplier name", "Phone number", "Email", "Address"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.supplier().name(),
                                        requireNonNullElse(item.phoneNumber(), "N/A"),
                                        requireNonNullElse(item.email(), "N/A"),
                                        requireNonNullElse(item.address(), "N/A")
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
