package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface MaterialSupplierContactRepository {

    @GET("materialsuppliercontacts")
    Call<List<MaterialSupplierContact>> findAll();

    @GET("materialsuppliercontacts")
    Call<List<MaterialSupplierContact>> getContactsForMaterialSupplier(@Query("supplierId") long supplierId);

    @GET("materialsuppliercontacts/{id}")
    Call<MaterialSupplierContact> findById(@Path("id") long id);

    @POST("materialsuppliercontacts")
    Call<MaterialSupplierContact> create(@Body MaterialSupplierContact materialSupplierContact);

    @PUT("materialsuppliercontacts/{id}")
    Call<MaterialSupplierContact> fullUpdate(@Body MaterialSupplierContact materialSupplierContact, @Path("id") long id);

    @DELETE("materialsuppliercontacts/{id}")
    Call<Void> delete(@Path("id") long id);

    static MaterialSupplierContactRepository create() {
        return Utils.createRetrofitBackedRepository(MaterialSupplierContactRepository.class);
    }

}
