package com.sixtyninefourtwenty.thesisapp.frontend.views;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Function;

@RequiredArgsConstructor
public class PopupHandler {

    private final Pane context;
    @Nullable
    private Popup latestPopup;

    private Popup assignAndReturn(Popup popup) {
        latestPopup = popup;
        return popup;
    }

    public boolean isLatestPopupShowing() {
        if (latestPopup == null) {
            return false;
        }
        return latestPopup.isShowing();
    }

    public Popup createPopup() {
        return assignAndReturn(new Popup(context));
    }

    public Popup createPopup(Node content) {
        return assignAndReturn(new Popup(context, content));
    }

    public Popup createPopup(Function<? super Popup, ? extends Node> content) {
        return assignAndReturn(new Popup(context, content));
    }

    public Popup createPopupAndShow(Function<? super Popup, ? extends Node> content) {
        return assignAndReturn(Popup.createAndShow(context, content));
    }

    public Popup createNoItemsPopupAndShow() {
        return createPopupAndShow(popup -> GenericErrorView.noItems(popup::close).getRoot());
    }

    public Popup createCantConnectToServerPopupAndShow() {
        return createPopupAndShow(popup -> GenericErrorView.cantConnectToServer(popup::close).getRoot());
    }

    public Popup createPleaseChooseOneItemPopupAndShow() {
        return createPopupAndShow(popup -> GenericErrorView.pleaseChooseOneItem(popup::close).getRoot());
    }

}
