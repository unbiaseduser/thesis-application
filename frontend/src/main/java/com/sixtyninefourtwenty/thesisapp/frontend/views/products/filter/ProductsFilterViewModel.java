package com.sixtyninefourtwenty.thesisapp.frontend.views.products.filter;

import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Getter
@Setter
public class ProductsFilterViewModel {
    @Nullable
    private String nameSearchQuery;
    @Nullable
    private String unitSearchQuery;
}
