package com.sixtyninefourtwenty.thesisapp.frontend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import lombok.SneakyThrows;
import org.jspecify.annotations.Nullable;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.List;
import java.util.function.Predicate;

public final class Utils {

    private Utils() { throw new UnsupportedOperationException(); }

    public static String orEmpty(@Nullable String s) {
        return s != null ? s : "";
    }

    public static void runOnApplicationThread(Runnable runnable) {
        if (Platform.isFxApplicationThread()) {
            runnable.run();
        } else {
            Platform.runLater(runnable);
        }
    }

    public static boolean isNullOrBlank(@Nullable String s) {
        return s == null || s.isBlank();
    }

    public static FXMLLoader fxmlLoader(String fileNameWithoutExtension) {
        return new FXMLLoader(MyApplication.class.getResource("layout/" + fileNameWithoutExtension + ".fxml"));
    }

    @SneakyThrows
    public static void loadQuietly(FXMLLoader fxmlLoader) {
        fxmlLoader.load();
    }

    public static <T> T createRetrofitBackedRepository(Class<T> clazz) {
        return new Retrofit.Builder()
                .baseUrl("http://localhost:8080")
                .addConverterFactory(JacksonConverterFactory.create(new ObjectMapper().registerModule(new JavaTimeModule())))
                .build()
                .create(clazz);
    }

    public static <T> List<T> cast(List<? extends T> list) {
        return list.stream()
                .map(e -> (T) e)
                .toList();
    }

    public static <T> List<T> filterList(List<T> list, Iterable<? extends Predicate<T>> predicates) {
        final var iter = predicates.iterator();
        if (!iter.hasNext()) {
            return list;
        }
        var finalPredicate = iter.next();
        while (iter.hasNext()) {
            finalPredicate = finalPredicate.and(iter.next());
        }
        return list.stream()
                .filter(finalPredicate)
                .toList();
    }

}
