package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class MaterialSupplierAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final MaterialSupplier materialSupplier;
    private final Consumer<MaterialSupplier> onOkButtonClick;
    private final Runnable onCancelButtonClick;

    @FXML
    private TextField nameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    protected void initialize() {
        if (materialSupplier != null) {
            nameTextField.setText(materialSupplier.name());
        }
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var nameText = ViewUtils.getTextOrEmpty(nameTextField);
        if (nameText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Name can't be blank",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonClick.accept(new MaterialSupplier(materialSupplier != null ? materialSupplier.id() : 0, nameText));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }

}
