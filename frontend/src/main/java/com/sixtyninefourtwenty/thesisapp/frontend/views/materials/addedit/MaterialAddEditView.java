package com.sixtyninefourtwenty.thesisapp.frontend.views.materials.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class MaterialAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final Material material;
    private final Consumer<Material> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    @FXML
    private TextField unitTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    protected void initialize() {
        if (material != null) {
            nameTextField.setText(material.name());
            unitTextField.setText(material.unit());
        }
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var nameText = ViewUtils.getTextOrEmpty(nameTextField);
        if (nameText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Name can't be blank",
                    popup::close
            ).getRoot());
            return;
        }
        final var unitText = ViewUtils.getTextOrEmpty(unitTextField);
        if (unitText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Unit can't be blank",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonAction.accept(new Material(material != null ? material.id() : 0, nameText, unitText));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
