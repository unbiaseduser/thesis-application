package com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.data;

import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductCreationRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductCreationsViewModel extends CommonViewModel<ProductCreation> {

    private final ProductCreationRepository productCreationRepository;

    public ProductCreationsViewModel(ProductCreationRepository productCreationRepository, List<ProductCreation> items) {
        super(items);
        this.productCreationRepository = productCreationRepository;
    }

    public void tryCreate(
            ProductCreation creation,
            Consumer<ProductCreation> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productCreationRepository.tryCreate(creation).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductCreation>> call, Response<Result<ProductCreation>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductCreation> s -> {
                        preFilteredList.add(s.data());
                        onSuccessResponse.accept(s.data());
                    }
                    case Result.Rejection<ProductCreation> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductCreation>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void tryFullUpdate(
            ProductCreation creation,
            long id,
            Consumer<ProductCreation> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productCreationRepository.tryFullUpdate(creation, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductCreation>> call, Response<Result<ProductCreation>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductCreation> s -> {
                        final var newItem = s.data();
                        preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<ProductCreation> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductCreation>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void tryDelete(
            ProductCreation creation,
            Runnable onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productCreationRepository.tryDelete(creation.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductCreation>> call, Response<Result<ProductCreation>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductCreation> s -> {
                        preFilteredList.remove(creation);
                        onSuccessResponse.run();
                    }
                    case Result.Rejection<ProductCreation> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductCreation>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

}
