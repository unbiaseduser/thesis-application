package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface MaterialRepository {

    @GET("materials")
    Call<List<Material>> findAll();

    @GET("materials/{id}")
    Call<Material> findById(@Path("id") long id);

    @POST("materials")
    Call<Material> create(@Body Material material);

    @PUT("materials/{id}")
    Call<Material> fullUpdate(@Body Material material, @Path("id") long id);

    @DELETE("materials/{id}")
    Call<Void> delete(@Path("id") long id);

    static MaterialRepository create() {
        return Utils.createRetrofitBackedRepository(MaterialRepository.class);
    }

}
