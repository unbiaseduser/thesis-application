package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class MaterialSuppliersFilterView extends CommonDialogView {

    private final MaterialSuppliersFilterViewModel materialSuppliersFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<MaterialSupplier>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    public static MaterialSuppliersFilterView create(
            MaterialSuppliersFilterViewModel materialSuppliersFilterViewModel,
            Consumer<? super Collection<? extends Predicate<MaterialSupplier>>> onOkButtonAction,
            Runnable onCancelButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_supplier_filter");
        fxmlLoader.setControllerFactory(clazz -> new MaterialSuppliersFilterView(materialSuppliersFilterViewModel, onOkButtonAction, onCancelButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private TextField nameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    private void initialize() {
        nameTextField.setText(materialSuppliersFilterViewModel.getNameSearchQuery());
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<MaterialSupplier>>(1);
        final var nameText = nameTextField.getText();
        if (!Utils.isNullOrBlank(nameText)) {
            filters.add(s -> s.name().contains(nameText));
        }
        materialSuppliersFilterViewModel.setNameSearchQuery(nameText);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
