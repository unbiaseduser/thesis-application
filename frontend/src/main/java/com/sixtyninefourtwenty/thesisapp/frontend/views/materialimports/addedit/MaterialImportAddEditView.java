package com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.time.LocalDate;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class MaterialImportAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final MaterialImport materialImport;
    private final Consumer<MaterialImport> onOkButtonClick;
    private final Runnable onCancelButtonClick;
    private final Consumer<MaterialImportAddEditView> onChooseMaterialButtonClick;
    private final Consumer<MaterialImportAddEditView> onChooseMaterialSupplierButtonClick;
    @Nullable
    private Material pickedMaterial;
    @Nullable
    private MaterialSupplier pickedMaterialSupplier;

    @FXML
    private DatePicker datePicker;
    @FXML
    private Spinner<Integer> quantitySpinner;
    @FXML
    private TextArea materialTextArea;
    @FXML
    private Button chooseMaterialButton;
    @FXML
    private Button chooseMaterialSupplierButton;
    @FXML
    private TextArea materialSupplierTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedMaterial(Material material) {
        this.pickedMaterial = material;
        materialTextArea.setText(material.name());
    }

    public void setPickedMaterialSupplier(@Nullable MaterialSupplier materialSupplier) {
        this.pickedMaterialSupplier = materialSupplier;
        materialSupplierTextArea.setText(materialSupplier != null ? materialSupplier.name() : null);
    }

    @FXML
    protected void initialize() {
        if (materialImport != null) {
            setPickedMaterial(materialImport.material());
            setPickedMaterialSupplier(materialImport.supplier());
            quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, materialImport.quantity()));
            datePicker.setValue(materialImport.time().toLocalDate());
        } else {
            quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE));
            datePicker.setValue(LocalDate.now());
        }
        chooseMaterialButton.setOnAction(event -> onChooseMaterialButtonClick.accept(this));
        chooseMaterialSupplierButton.setOnAction(event -> onChooseMaterialSupplierButtonClick.accept(this));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        if (pickedMaterial == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Material not picked",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonClick.accept(new MaterialImport(
                materialImport != null ? materialImport.id() : 0,
                pickedMaterialSupplier,
                pickedMaterial,
                quantitySpinner.getValue(),
                datePicker.getValue().atStartOfDay()
            )
        );
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }

}
