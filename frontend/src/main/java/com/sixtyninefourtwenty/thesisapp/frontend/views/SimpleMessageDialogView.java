package com.sixtyninefourtwenty.thesisapp.frontend.views;

import javafx.event.ActionEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.awt.*;

public class SimpleMessageDialogView extends CommonDialogView {

    private final Runnable onOkButtonAction;
    private final Runnable onCancelButtonAction;

    public SimpleMessageDialogView(String message, Runnable onOkButtonAction, Runnable onCancelButtonAction) {
        this.onOkButtonAction = onOkButtonAction;
        this.onCancelButtonAction = onCancelButtonAction;
        final var text = new Text(message);
        text.setFont(new Font(14));
        setContentView(text);
    }

    public static SimpleMessageDialogView deleteItem(Runnable onOkButtonAction, Runnable onCancelButtonAction) {
        return new SimpleMessageDialogView(
                "Do you want to delete this item?", onOkButtonAction, onCancelButtonAction
        );
    }

    public static SimpleMessageDialogView deleteItems(int numOfItems, Runnable onOkButtonAction, Runnable onCancelButtonAction) {
        return new SimpleMessageDialogView(
                "Do you want to delete " + numOfItems + " items?", onOkButtonAction, onCancelButtonAction
        );
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        onOkButtonAction.run();
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
