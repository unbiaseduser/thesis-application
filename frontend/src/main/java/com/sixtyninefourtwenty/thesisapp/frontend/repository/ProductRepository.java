package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ProductRepository {

    @GET("products")
    Call<List<Product>> findAll();

    @GET("products/{id}")
    Call<Product> findById(@Path("id") long id);

    @POST("products")
    Call<Product> create(@Body Product product);

    @PUT("products/{id}")
    Call<Product> fullUpdate(@Body Product product, @Path("id") long id);

    @DELETE("products/{id}")
    Call<Void> delete(@Path("id") long id);

    static ProductRepository create() {
        return Utils.createRetrofitBackedRepository(ProductRepository.class);
    }

}
