package com.sixtyninefourtwenty.thesisapp.frontend.views.products.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.frontend.stockgetter.ProductStockGetter;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.data.ProductAndStock;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.products.addedit.ProductAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.products.filter.ProductsFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.products.filter.ProductsFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import org.jspecify.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductsView extends CommonDataView<Product> {

    private final ProductStockGetter productStockGetter;
    private final ProductsViewModel productsViewModel;
    private final ProductsFilterViewModel productsFilterViewModel;

    public ProductsView(
            ExcelExporter excelExporter,
            List<Product> items,
            PopupHandler popupHandler,
            ProductStockGetter productStockGetter,
            ProductsViewModel productsViewModel,
            ProductsFilterViewModel productsFilterViewModel
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forProduct(),
                items
        );
        this.productStockGetter = productStockGetter;
        this.productsViewModel = productsViewModel;
        this.productsFilterViewModel = productsFilterViewModel;
        productsViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));

        final var stockButton = new Button("Stock");
        stockButton.setOnAction(event -> {
            if (getItems().isEmpty()) {
                popupHandler.createNoItemsPopupAndShow();
                return;
            }
            final var selectedItems = getSelectedItems();
            final var finalItems = selectedItems.isEmpty() ? getItems() : selectedItems;
            final var itemsWithStock = new ArrayList<ProductAndStock>(finalItems.size());
            for (int i = 0; i < finalItems.size(); i++) {
                final var selectedItem = finalItems.get(i);
                try {
                    final var stockResponse = productStockGetter.calculateStockForProductId(selectedItem.id()).execute();
                    itemsWithStock.add(new ProductAndStock(selectedItem, requireNonNull(stockResponse.body())));
                    if (i == finalItems.size() - 1) {
                        final var popup = popupHandler.createPopup();
                        final var dialog = DataDialog.create(
                                List.of(
                                        ViewUtils.createTableColumn("ID", m -> m.product().id()),
                                        ViewUtils.createTableColumn("Name", m -> m.product().name()),
                                        ViewUtils.createTableColumn("Unit", m -> m.product().unit()),
                                        ViewUtils.createTableColumn("Stock", ProductAndStock::stock)
                                ),
                                itemsWithStock,
                                popup::close
                        );
                        final var reportButton = new Button("Report");
                        reportButton.setOnAction(ae -> {
                            popup.close();
                            popupHandler.createPopupAndShow(popup1 -> new SimpleMessageDialogView(
                                    "Do you want to save a report of these items?",
                                    () -> {
                                        popup1.close();
                                        try {
                                            excelExporter.exportExcel(
                                                    "products_and_stock_" + System.currentTimeMillis(),
                                                    "ProductsAndStock",
                                                    List.of("ID", "Name", "Unit", "Stock"),
                                                    dialog.getItems(),
                                                    item -> List.of(
                                                            Long.toString(item.product().id()),
                                                            item.product().name(),
                                                            item.product().unit(),
                                                            Long.toString(item.stock())
                                                    )
                                            );
                                        } catch (IOException e) {
                                            popupHandler.createPopupAndShow(popup2 -> GenericErrorView.create(
                                                    "Can't create report.",
                                                    popup2::close
                                            ).getRoot());
                                        }
                                    },
                                    popup1::close
                            ).getRoot());
                        });
                        dialog.addButtons(reportButton);
                        popup.setContentView(dialog.getRoot());
                        popup.show();
                    }
                } catch (IOException ignored) {

                }
            }
        });
        addButton(stockButton);
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showProductAddEditView(null, product -> {
            productsViewModel.create(
                    product,
                    p -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, Product item) {
        showProductAddEditView(item, product -> {
            productsViewModel.fullUpdate(
                    product,
                    product.id(),
                    p -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showProductAddEditView(
            @Nullable Product product,
            Consumer<Product> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new ProductAddEditView(popupHandler, product, onOkButtonClick, this::clearBottomPanel));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<ProductAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, Product item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    productsViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup,
                            () -> {
                                Platform.runLater(() -> {
                                    final var popup2 = popupHandler.createPopup();
                                    popup2.setContentView(GenericErrorView.create(
                                            "Can't delete this item.\nIt might have some data associated with it.",
                                            popup2::close
                                    ).getRoot());
                                    popup2.show();
                                });
                            }
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = ProductsFilterView.create(
                productsFilterViewModel,
                filters -> {
                    productsViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<Product> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "products_" + System.currentTimeMillis(),
                                "Products",
                                List.of("ID", "Name", "Unit"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.name(),
                                        item.unit()
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
