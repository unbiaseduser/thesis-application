package com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class CustomerContactAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final CustomerContact customerContact;
    private final Consumer<CustomerContact> onOkButtonClick;
    private final Runnable onCancelButtonClick;
    private final Consumer<CustomerContactAddEditView> onChooseCustomerButtonClick;
    private Customer pickedCustomer;

    @FXML
    private AnchorPane root;
    @FXML
    private Button chooseCustomerButton;
    @FXML
    private TextArea customerTextArea;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField phoneNumberTextField;

    public void setPickedCustomer(Customer customer) {
        this.pickedCustomer = customer;
        customerTextArea.setText(customer.name());
    }

    @FXML
    protected void initialize() {
        if (customerContact != null) {
            setPickedCustomer(customerContact.customer());
            phoneNumberTextField.setText(customerContact.phoneNumber());
            emailTextField.setText(customerContact.email());
            addressTextField.setText(customerContact.address());
        }
        chooseCustomerButton.setOnAction(event -> onChooseCustomerButtonClick.accept(this));
        setContentView(root);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        if (pickedCustomer == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Customer not picked",
                    popup::close
            ).getRoot());
            return;
        }
        final var phoneNumberText = ViewUtils.getTextOrEmpty(phoneNumberTextField);
        final var emailText = ViewUtils.getTextOrEmpty(emailTextField);
        final var addressText = ViewUtils.getTextOrEmpty(addressTextField);
        if (phoneNumberText.isBlank() && emailText.isBlank() && addressText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "One of phone number, email and address must be present",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonClick.accept(new CustomerContact(
                customerContact != null ? customerContact.id() : 0,
                pickedCustomer,
                phoneNumberText,
                emailText,
                addressText
        ));
    }

}
