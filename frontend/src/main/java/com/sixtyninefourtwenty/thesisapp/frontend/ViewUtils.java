package com.sixtyninefourtwenty.thesisapp.frontend;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public final class ViewUtils {

    private ViewUtils() { throw new UnsupportedOperationException(); }

    public static <T, U> TableColumn<T, U> createTableColumn(String text, Callback<T, U> cellValueFactory) {
        final var tableColumn = new TableColumn<T, U>(text);
        tableColumn.setCellValueFactory(param -> new SimpleObjectProperty<>(cellValueFactory.call(param.getValue())));
        return tableColumn;
    }

    public static String getTextOrEmpty(TextInputControl control) {
        final var text = control.getText();
        return text != null ? text : "";
    }

    public static void resetChildren(Pane pane, Node... children) {
        final var currentChildren = pane.getChildren();
        currentChildren.clear();
        currentChildren.addAll(children);
    }

    public static Node findNodeById(Parent parent, String id) {
        final var childrenWhoAreParents = new ArrayList<Parent>();
        for (final var node : parent.getChildrenUnmodifiable()) {
            if (id.equals(node.getId())) {
                return node;
            } else if (node instanceof Parent p) {
                childrenWhoAreParents.add(p);
            }
        }
        for (final var p : childrenWhoAreParents) {
            try {
                return findNodeById(p, id);
            } catch (NoSuchElementException ignored) {
            }
        }
        throw new NoSuchElementException("No child has id " + id);
    }

}
