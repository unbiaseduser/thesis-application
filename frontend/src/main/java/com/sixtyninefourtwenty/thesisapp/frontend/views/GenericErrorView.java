package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces.View;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class GenericErrorView implements View {

    private final String errorMsg;
    private final Runnable onOkButtonAction;

    @FXML
    private Text errorText;

    @FXML
    private Button okButton;

    @FXML
    private Node root;

    @FXML
    private void initialize() {
        okButton.setOnAction(event -> onOkButtonAction.run());
        errorText.setText(errorMsg);
    }

    @Override
    public Node getRoot() {
        return root;
    }

    public static GenericErrorView create(String errorMsg, Runnable onOkButtonAction) {
        final var fxmlLoader = Utils.fxmlLoader("generic_error2");
        fxmlLoader.setControllerFactory(clazz -> new GenericErrorView(
                errorMsg,
                onOkButtonAction
        ));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    public static GenericErrorView noItems(Runnable onOkButtonAction) {
        return create("There are no items", onOkButtonAction);
    }

    public static GenericErrorView cantConnectToServer(Runnable onOkButtonAction) {
        return create("Can't connect to server", onOkButtonAction);
    }

    public static GenericErrorView pleaseChooseOneItem(Runnable onOkButtonAction) {
        return create("Please choose one item", onOkButtonAction);
    }

}
