package com.sixtyninefourtwenty.thesisapp.frontend.exporter;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;

public abstract class ExcelExporter {

    public <T> void exportExcel(
            String fileNameWithoutExtension,
            String sheetName,
            List<String> headerCellValues,
            List<? extends T> items,
            Function<? super T, ? extends List<String>> cellValuesFunction
    ) throws IOException {
        if (items.isEmpty()) {
            throw new IllegalArgumentException("Empty list");
        }
        try (final var workbook = new XSSFWorkbook()) {
            final var sheet = workbook.createSheet(sheetName);
            final var headerRow = sheet.createRow(0);
            final var headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            for (int i = 0; i < headerCellValues.size(); i++) {
                final var headerCellValue = headerCellValues.get(i);
                final var headerCell = headerRow.createCell(i);
                headerCell.setCellValue(headerCellValue);
                headerCell.setCellStyle(headerStyle);
            }
            for (int i = 0; i < items.size(); i++) {
                final var row = sheet.createRow(i + 1);
                final var item = items.get(i);
                final var cellValues = cellValuesFunction.apply(item);
                for (int j = 0; j < cellValues.size(); j++) {
                    final var cellValue = cellValues.get(j);
                    final var cell = row.createCell(j);
                    cell.setCellValue(cellValue);
                }
            }
            final var outputPath = Path.of(fileNameWithoutExtension + ".xlsx");
            try (final var outputFileStream = new BufferedOutputStream(Files.newOutputStream(outputPath))) {
                workbook.write(outputFileStream);
            }
            handleSavingFile(outputPath);
        }
    }

    protected abstract void handleSavingFile(Path path) throws IOException;

}
