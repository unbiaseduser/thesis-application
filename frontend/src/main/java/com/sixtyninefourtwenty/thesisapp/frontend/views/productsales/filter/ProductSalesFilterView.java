package com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class ProductSalesFilterView extends CommonDialogView {

    private final ProductSalesFilterViewModel productSalesFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<ProductSale>>> onOkButtonClick;
    private final Runnable onCancelButtonClick;
    private final Consumer<? super ProductSalesFilterView> onChooseProductButtonAction;
    private final Consumer<? super ProductSalesFilterView> onChooseCustomerButtonAction;
    @FXML
    private Button clearDateButton;
    @FXML
    private Button clearCustomerButton;
    @FXML
    private Button clearProductButton;
    @Nullable
    private Product pickedProduct;
    @Nullable
    private Customer pickedCustomer;

    public static ProductSalesFilterView create(
            ProductSalesFilterViewModel productSalesFilterViewModel,
            Consumer<? super Collection<? extends Predicate<ProductSale>>> onOkButtonClick,
            Runnable onCancelButtonClick,
            Consumer<? super ProductSalesFilterView> onChooseProductButtonAction,
            Consumer<? super ProductSalesFilterView> onChooseCustomerButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_sale_filter");
        fxmlLoader.setControllerFactory(clazz -> new ProductSalesFilterView(productSalesFilterViewModel, onOkButtonClick, onCancelButtonClick, onChooseProductButtonAction, onChooseCustomerButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private DatePicker datePicker;
    @FXML
    private TextArea customerTextArea;
    @FXML
    private Button chooseCustomerButton;
    @FXML
    private Spinner<Integer> quantitySpinner;
    @FXML
    private Button chooseProductButton;
    @FXML
    private TextArea productTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedProduct(@Nullable Product product) {
        this.pickedProduct = product;
        productTextArea.setText(product != null ? product.name() : null);
    }

    public void setPickedCustomer(@Nullable Customer customer) {
        this.pickedCustomer = customer;
        customerTextArea.setText(customer != null ? customer.name() : null);
    }

    @FXML
    private void initialize() {
        setPickedCustomer(productSalesFilterViewModel.getCustomerSearchQuery());
        setPickedProduct(productSalesFilterViewModel.getProductSearchQuery());
        quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, productSalesFilterViewModel.getQuantitySearchQuery()));
        datePicker.setValue(productSalesFilterViewModel.getDateSearchQuery());
        chooseProductButton.setOnAction(event -> onChooseProductButtonAction.accept(this));
        chooseCustomerButton.setOnAction(event -> onChooseCustomerButtonAction.accept(this));
        clearProductButton.setOnAction(event -> setPickedProduct(null));
        clearCustomerButton.setOnAction(event -> setPickedCustomer(null));
        clearDateButton.setOnAction(event -> datePicker.setValue(null));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<ProductSale>>(4);
        if (pickedProduct != null) {
            filters.add(s -> pickedProduct.equals(s.product()));
        }
        if (pickedCustomer != null) {
            filters.add(s -> pickedCustomer.equals(s.customer()));
        }
        final var quantity = quantitySpinner.getValue();
        if (quantity > 0) {
            filters.add(s -> s.productQuantity() == quantity);
        }
        final var date = datePicker.getValue();
        if (date != null) {
            filters.add(s -> s.time().toLocalDate().isEqual(date));
        }
        productSalesFilterViewModel.setProductSearchQuery(pickedProduct);
        productSalesFilterViewModel.setCustomerSearchQuery(pickedCustomer);
        productSalesFilterViewModel.setQuantitySearchQuery(quantity);
        productSalesFilterViewModel.setDateSearchQuery(date);
        onOkButtonClick.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }
}
