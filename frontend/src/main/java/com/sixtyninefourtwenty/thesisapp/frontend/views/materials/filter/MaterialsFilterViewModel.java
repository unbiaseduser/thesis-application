package com.sixtyninefourtwenty.thesisapp.frontend.views.materials.filter;

import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Setter
@Getter
public class MaterialsFilterViewModel {
    @Nullable
    private String nameSearchQuery;
    @Nullable
    private String unitSearchQuery;
}
