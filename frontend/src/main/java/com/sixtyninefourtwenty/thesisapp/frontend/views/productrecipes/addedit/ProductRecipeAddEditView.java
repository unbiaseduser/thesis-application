package com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductRecipePart;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class ProductRecipeAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final ProductRecipePart productRecipePart;
    private final Consumer<ProductRecipePart> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<ProductRecipeAddEditView> onChooseProductButtonAction;
    private final Consumer<ProductRecipeAddEditView> onChooseMaterialButtonAction;
    @Nullable
    private Product pickedProduct;
    @Nullable
    private Material pickedMaterial;

    @FXML
    private Spinner<Integer> materialQuantitySpinner;
    @FXML
    private Button chooseMaterialButton;
    @FXML
    private TextArea materialTextArea;
    @FXML
    private Button chooseProductButton;
    @FXML
    private TextArea productTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedProduct(Product product) {
        this.pickedProduct = product;
        productTextArea.setText(product.name());
    }

    public void setPickedMaterial(Material material) {
        this.pickedMaterial = material;
        materialTextArea.setText(material.name());
    }

    @FXML
    protected void initialize() {
        if (productRecipePart != null) {
            setPickedProduct(productRecipePart.product());
            setPickedMaterial(productRecipePart.material());
            materialQuantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, productRecipePart.materialQuantity()));
        } else {
            materialQuantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE));
        }
        chooseProductButton.setOnAction(event -> onChooseProductButtonAction.accept(this));
        chooseMaterialButton.setOnAction(event -> onChooseMaterialButtonAction.accept(this));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        if (pickedProduct == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Product not picked",
                    popup::close
            ).getRoot());
            return;
        }
        if (pickedMaterial == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Material not picked",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonAction.accept(new ProductRecipePart(
                productRecipePart != null ? productRecipePart.id() : 0,
                pickedProduct,
                pickedMaterial,
                materialQuantitySpinner.getValue()
        ));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
