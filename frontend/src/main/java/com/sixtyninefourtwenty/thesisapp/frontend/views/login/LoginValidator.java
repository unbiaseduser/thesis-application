package com.sixtyninefourtwenty.thesisapp.frontend.views.login;

public interface LoginValidator {
    boolean isLoginValid(String userName, String password);
}
