package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.function.BiConsumer;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class SingleChooserDialogView<T> extends CommonDialogView {

    private final List<T> items;
    private final List<TableColumn<T, ?>> tableColumns;
    private final BiConsumer<ActionEvent, T> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    @FXML
    private TableView<T> table;

    public static <T> SingleChooserDialogView<T> create(
            List<T> items,
            List<TableColumn<T, ?>> tableColumns,
            BiConsumer<ActionEvent, T> onOkButtonAction,
            Runnable onCancelButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("common_single_chooser");
        fxmlLoader.setControllerFactory(clazz -> new SingleChooserDialogView<>(
                items, tableColumns, onOkButtonAction, onCancelButtonAction
        ));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private void initialize() {
        table.getColumns().addAll(tableColumns);
        table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        table.setItems(FXCollections.observableList(items));
        setContentView(table);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var selectedItem = table.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        onOkButtonAction.accept(event, selectedItem);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
