package com.sixtyninefourtwenty.thesisapp.frontend.views.login;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces.View;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LoginView implements View {

    private final PopupHandler popupHandler;
    private final LoginValidator loginValidator;
    private final Runnable onLoginSuccess;

    @FXML
    private Button loginButton;
    @FXML
    private TextField passwordTextField;
    @FXML
    private TextField usernameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    private void initialize() {
        loginButton.setOnAction(actionEvent -> {
            final var username = usernameTextField.getText();
            if (Utils.isNullOrBlank(username)) {
                popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                        "Username not entered",
                        popup::close
                ).getRoot());
                return;
            }
            final var password = passwordTextField.getText();
            if (Utils.isNullOrBlank(password)) {
                popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                        "Password not entered",
                        popup::close
                ).getRoot());
                return;
            }
            if (loginValidator.isLoginValid(username, password)) {
                onLoginSuccess.run();
            } else {
                popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                        "Wrong login credentials",
                        popup::close
                ).getRoot());
            }
        });
    }

    @Override
    public AnchorPane getRoot() {
        return root;
    }
}
