package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces.View;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public abstract class CommonDialogView implements View {

    protected CommonDialogView() {
        final var fxmlLoader = Utils.fxmlLoader("common_dialog");
        Utils.loadQuietly(fxmlLoader);
        final var root = fxmlLoader.<Parent>getRoot();
        content = (Pane) ViewUtils.findNodeById(root, "content");
        okButton = (Button) ViewUtils.findNodeById(root, "okButton");
        cancelButton = (Button) ViewUtils.findNodeById(root, "cancelButton");
        okButton.setOnAction(this::onOkButtonAction);
        cancelButton.setOnAction(this::onCancelButtonAction);
        this.dialogRoot = root;
    }

    private final Pane content;
    private final Button okButton;
    private final Button cancelButton;
    private final Node dialogRoot;

    protected final void setContentView(Node contentView) {
        ViewUtils.resetChildren(content, contentView);
    }

    protected abstract void onOkButtonAction(ActionEvent event);
    protected abstract void onCancelButtonAction(ActionEvent event);

    @Override
    public final Node getRoot() {
        return dialogRoot;
    }
}
