package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.data;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialSupplierRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class MaterialSuppliersViewModel extends CommonViewModel<MaterialSupplier> {

    private final MaterialSupplierRepository materialSupplierRepository;

    public MaterialSuppliersViewModel(MaterialSupplierRepository materialSupplierRepository, List<MaterialSupplier> items) {
        super(items);
        this.materialSupplierRepository = materialSupplierRepository;
    }

    public void create(
            MaterialSupplier supplier,
            Consumer<MaterialSupplier> onResponse,
            Runnable onFailure
    ) {
        materialSupplierRepository.create(supplier).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<MaterialSupplier> call, Response<MaterialSupplier> response) {
                preFilteredList.add(requireNonNull(response.body()));
                onResponse.accept(requireNonNull(response.body()));
            }

            @Override
            public void onFailure(Call<MaterialSupplier> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void fullUpdate(
            MaterialSupplier supplier,
            long id,
            Consumer<MaterialSupplier> onResponse,
            Runnable onFailure
    ) {
        materialSupplierRepository.fullUpdate(supplier, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<MaterialSupplier> call, Response<MaterialSupplier> response) {
                final var newItem = requireNonNull(response.body());
                preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                onResponse.accept(newItem);
            }

            @Override
            public void onFailure(Call<MaterialSupplier> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void delete(
            MaterialSupplier supplier,
            Runnable onResponse,
            Runnable onFailure,
            Runnable onFailureResponse
    ) {
        materialSupplierRepository.delete(supplier.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    preFilteredList.remove(supplier);
                    onResponse.run();
                } else {
                    onFailureResponse.run();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
