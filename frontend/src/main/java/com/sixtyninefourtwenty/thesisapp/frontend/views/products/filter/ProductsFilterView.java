package com.sixtyninefourtwenty.thesisapp.frontend.views.products.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class ProductsFilterView extends CommonDialogView {

    private final ProductsFilterViewModel productsFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<Product>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    public static ProductsFilterView create(
            ProductsFilterViewModel productsFilterViewModel,
            Consumer<? super Collection<? extends Predicate<Product>>> onOkButtonAction,
            Runnable onCancelButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_filter");
        fxmlLoader.setControllerFactory(clazz -> new ProductsFilterView(productsFilterViewModel, onOkButtonAction, onCancelButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private TextField unitTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    private void initialize() {
        nameTextField.setText(productsFilterViewModel.getNameSearchQuery());
        unitTextField.setText(productsFilterViewModel.getUnitSearchQuery());
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<Product>>(2);
        final var nameText = nameTextField.getText();
        if (!Utils.isNullOrBlank(nameText)) {
            filters.add(p -> p.name().contains(nameText));
        }
        final var unitText = unitTextField.getText();
        if (!Utils.isNullOrBlank(unitText)) {
            filters.add(p -> p.unit().contains(unitText));
        }
        productsFilterViewModel.setNameSearchQuery(nameText);
        productsFilterViewModel.setUnitSearchQuery(unitText);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
