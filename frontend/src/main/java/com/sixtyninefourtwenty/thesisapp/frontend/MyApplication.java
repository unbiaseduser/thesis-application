package com.sixtyninefourtwenty.thesisapp.frontend;

import com.jpro.webapi.WebAPI;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.impl.LocalExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.impl.WebExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.*;
import com.sixtyninefourtwenty.thesisapp.frontend.stockgetter.MaterialStockGetter;
import com.sixtyninefourtwenty.thesisapp.frontend.stockgetter.ProductStockGetter;
import com.sixtyninefourtwenty.thesisapp.frontend.views.login.LoginValidator;
import com.sixtyninefourtwenty.thesisapp.frontend.views.login.impl.LoginValidatorImpl;
import com.sixtyninefourtwenty.thesisapp.frontend.views.main.MainView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.main.MainViewModel;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class MyApplication extends Application {

    public static void main(String[] args) {
        launch(MyApplication.class, args);
    }

    private LoginValidator loginValidator;
    private ScreenSwitcher screenSwitcher;
    private MainViewModel mainViewModel;
    private CustomerRepository customerRepository;
    private CustomerContactRepository customerContactRepository;
    private MaterialRepository materialRepository;
    private MaterialImportRepository materialImportRepository;
    private MaterialSupplierRepository materialSupplierRepository;
    private MaterialSupplierContactRepository materialSupplierContactRepository;
    private ProductRepository productRepository;
    private ProductCreationRepository productCreationRepository;
    private ProductRecipePartRepository productRecipePartRepository;
    private ProductSaleRepository productSaleRepository;
    private MaterialStockGetter materialStockGetter;
    private ProductStockGetter productStockGetter;

    private ExcelExporter excelExporter;

    @Override
    public void init() {
        loginValidator = new LoginValidatorImpl();
        screenSwitcher = new ScreenSwitcher();
        mainViewModel = new MainViewModel();
        customerRepository = CustomerRepository.create();
        customerContactRepository = CustomerContactRepository.create();
        materialRepository = MaterialRepository.create();
        materialImportRepository = MaterialImportRepository.create();
        materialSupplierRepository = MaterialSupplierRepository.create();
        materialSupplierContactRepository = MaterialSupplierContactRepository.create();
        productRepository = ProductRepository.create();
        productCreationRepository = ProductCreationRepository.create();
        productRecipePartRepository = ProductRecipePartRepository.create();
        productSaleRepository = ProductSaleRepository.create();
        materialStockGetter = MaterialStockGetter.create();
        productStockGetter = ProductStockGetter.create();
    }

    @Override
    public void start(Stage stage) {
        excelExporter = WebAPI.isBrowser() ? new WebExcelExporter(WebAPI.getWebAPI(stage)) : new LocalExcelExporter(stage);
        final var fxmlLoader = Utils.fxmlLoader("main2");
        fxmlLoader.setControllerFactory(clazz -> new MainView(
                excelExporter,
                loginValidator,
                screenSwitcher,
                mainViewModel,
                customerRepository,
                customerContactRepository,
                materialRepository,
                materialImportRepository,
                materialSupplierRepository,
                materialSupplierContactRepository,
                productRepository,
                productRecipePartRepository,
                productCreationRepository,
                productSaleRepository,
                materialStockGetter,
                productStockGetter
        ));
        Utils.loadQuietly(fxmlLoader);
        final var root = fxmlLoader.<Parent>getRoot();
        stage.setScene(new Scene(root));
        stage.show();
    }

}
