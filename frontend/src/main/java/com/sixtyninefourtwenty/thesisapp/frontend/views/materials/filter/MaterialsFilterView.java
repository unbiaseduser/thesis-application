package com.sixtyninefourtwenty.thesisapp.frontend.views.materials.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class MaterialsFilterView extends CommonDialogView {

    private final MaterialsFilterViewModel materialsFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<Material>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    public static MaterialsFilterView create(
            MaterialsFilterViewModel materialsFilterViewModel,
            Consumer<? super Collection<? extends Predicate<Material>>> onOkButtonAction,
            Runnable onCancelButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_filter");
        fxmlLoader.setControllerFactory(clazz -> new MaterialsFilterView(materialsFilterViewModel, onOkButtonAction, onCancelButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private TextField unitTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private AnchorPane root;

    @FXML
    private void initialize() {
        nameTextField.setText(materialsFilterViewModel.getNameSearchQuery());
        unitTextField.setText(materialsFilterViewModel.getUnitSearchQuery());
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<Material>>(2);
        final var nameText = nameTextField.getText();
        if (!Utils.isNullOrBlank(nameText)) {
            filters.add(m -> m.name().contains(nameText));
        }
        final var unitText = unitTextField.getText();
        if (!Utils.isNullOrBlank(unitText)) {
            filters.add(m -> m.unit().contains(unitText));
        }
        materialsFilterViewModel.setNameSearchQuery(nameText);
        materialsFilterViewModel.setUnitSearchQuery(unitText);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
