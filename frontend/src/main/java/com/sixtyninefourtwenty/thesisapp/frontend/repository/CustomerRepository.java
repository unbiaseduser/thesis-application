package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface CustomerRepository {

    @GET("customers")
    Call<List<Customer>> findAll();

    @GET("customers/{id}")
    Call<Customer> findById(@Path("id") long id);

    @POST("customers")
    Call<Customer> create(@Body Customer customer);

    @PUT("customers/{id}")
    Call<Customer> fullUpdate(@Body Customer customer, @Path("id") long id);

    @DELETE("customers/{id}")
    Call<Void> delete(@Path("id") long id);

    static CustomerRepository create() {
        return Utils.createRetrofitBackedRepository(CustomerRepository.class);
    }

}
