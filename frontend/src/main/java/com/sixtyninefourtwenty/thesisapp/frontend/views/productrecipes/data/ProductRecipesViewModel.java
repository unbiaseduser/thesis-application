package com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.data;

import com.sixtyninefourtwenty.thesisapp.common.data.ProductRecipePart;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductRecipePartRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductRecipesViewModel extends CommonViewModel<ProductRecipePart> {

    private final ProductRecipePartRepository productRecipePartRepository;

    public ProductRecipesViewModel(ProductRecipePartRepository productRecipePartRepository, List<ProductRecipePart> items) {
        super(items);
        this.productRecipePartRepository = productRecipePartRepository;
    }

    public void tryCreate(
            ProductRecipePart productRecipePart,
            Consumer<ProductRecipePart> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productRecipePartRepository.tryCreate(productRecipePart).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductRecipePart>> call, Response<Result<ProductRecipePart>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductRecipePart> s -> {
                        final var newItem = s.data();
                        preFilteredList.add(newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<ProductRecipePart> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductRecipePart>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void tryFullUpdate(
            ProductRecipePart productRecipePart,
            long id,
            Consumer<ProductRecipePart> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productRecipePartRepository.tryFullUpdate(productRecipePart, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductRecipePart>> call, Response<Result<ProductRecipePart>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductRecipePart> s -> {
                        final var newItem = s.data();
                        preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<ProductRecipePart> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductRecipePart>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void delete(
            ProductRecipePart productRecipePart,
            Runnable onSuccess,
            Runnable onFailure
    ) {
        productRecipePartRepository.delete(productRecipePart.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                preFilteredList.remove(productRecipePart);
                onSuccess.run();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
