package com.sixtyninefourtwenty.thesisapp.frontend.views.login.impl;

import com.sixtyninefourtwenty.thesisapp.frontend.MyApplication;
import com.sixtyninefourtwenty.thesisapp.frontend.views.login.LoginValidator;
import lombok.SneakyThrows;

import java.util.Properties;

public class LoginValidatorImpl implements LoginValidator {
    @Override
    @SneakyThrows
    public boolean isLoginValid(String userName, String password) {
        final var props = new Properties();
        props.load(MyApplication.class.getResourceAsStream("login.properties"));
        for (final var account : props.entrySet()) {
            if (userName.equals(account.getKey()) && password.equals(account.getValue())) {
                return true;
            }
        }
        return false;
    }
}
