package com.sixtyninefourtwenty.thesisapp.frontend.views.customers.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.CustomerContactRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customers.addedit.CustomerAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customers.filter.CustomerFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customers.filter.CustomerFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;

public class CustomersView extends CommonDataView<Customer> {

    private final CustomersViewModel customersViewModel;
    private final CustomerFilterViewModel customerFilterViewModel;
    private final CustomerContactRepository customerContactRepository;

    public CustomersView(
            ExcelExporter excelExporter,
            List<Customer> items,
            PopupHandler popupHandler,
            CustomersViewModel customersViewModel,
            CustomerFilterViewModel customerFilterViewModel,
            CustomerContactRepository customerContactRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forCustomer(),
                items
        );
        this.customersViewModel = customersViewModel;
        this.customerFilterViewModel = customerFilterViewModel;
        this.customerContactRepository = customerContactRepository;
        customersViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));

        final var contactsButton = new Button("Contacts");
        contactsButton.setOnAction(event -> {
            final var selectedItems = getSelectedItems();
            if (selectedItems.size() != 1) {
                showChooseOneItemPopup();
                return;
            }
            customerContactRepository.getContactsForCustomer(selectedItems.getFirst().id()).enqueue(new Callback<>() {
                @Override
                public void onResponse(Call<List<CustomerContact>> call, Response<List<CustomerContact>> response) {
                    final var popup = popupHandler.createPopup();
                    final var dialog = DataDialog.create(
                            TableColumns.forCustomerContact(),
                            requireNonNull(response.body()),
                            popup::close
                    );
                    final var reportButton = new Button("Report");
                    reportButton.setOnAction(ae -> {
                        popup.close();
                        popupHandler.createPopupAndShow(popup1 -> new SimpleMessageDialogView(
                                "Do you want to save a report of these items?",
                                () -> {
                                    popup1.close();
                                    try {
                                        excelExporter.exportExcel(
                                                "customer_contacts_" + System.currentTimeMillis(),
                                                "CustomerContacts",
                                                List.of("ID", "Customer name", "Phone number", "Email", "Address"),
                                                dialog.getItems(),
                                                item -> List.of(
                                                        Long.toString(item.id()),
                                                        item.customer().name(),
                                                        requireNonNullElse(item.phoneNumber(), "N/A"),
                                                        requireNonNullElse(item.email(), "N/A"),
                                                        requireNonNullElse(item.address(), "N/A")
                                                )
                                        );
                                    } catch (IOException e) {
                                        popupHandler.createPopupAndShow(popup2 -> GenericErrorView.create(
                                                "Can't create report.",
                                                popup2::close
                                        ).getRoot());
                                    }
                                },
                                popup1::close
                        ).getRoot());

                    });
                    dialog.addButtons(reportButton);
                    popup.setContentView(dialog.getRoot());
                    popup.show();
                }

                @Override
                public void onFailure(Call<List<CustomerContact>> call, Throwable t) {

                }
            });
        });
        addButton(contactsButton);
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showCustomerAddEditView(null, customer -> {
            customersViewModel.create(
                    customer,
                    c -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, Customer item) {
        showCustomerAddEditView(item, customer -> {
            customersViewModel.fullUpdate(
                    customer,
                    customer.id(),
                    c -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showCustomerAddEditView(@Nullable Customer customer, Consumer<Customer> onOkButtonClick) {
        final var fxmlLoader = Utils.fxmlLoader("customer_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new CustomerAddEditView(popupHandler, customer, onOkButtonClick, this::clearBottomPanel));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<CustomerAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, Customer item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    customersViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup,
                            () -> {
                                Platform.runLater(() -> {
                                    final var popup2 = popupHandler.createPopup();
                                    popup2.setContentView(GenericErrorView.create(
                                            "Can't delete this item.\nIt might have some data associated with it.",
                                            popup2::close
                                    ).getRoot());
                                    popup2.show();
                                });
                            }
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = CustomerFilterView.create(
                customerFilterViewModel,
                filters -> {
                    customersViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<Customer> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "customers_" + System.currentTimeMillis(),
                                "Customers",
                                List.of("ID", "Name"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.name()
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
