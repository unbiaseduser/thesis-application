package com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.CustomerRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.addedit.CustomerContactAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.filter.CustomerContactsFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.filter.CustomerContactsFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;

public class CustomerContactsView extends CommonDataView<CustomerContact> {

    private final CustomerRepository customerRepository;
    private final CustomerContactsViewModel customerContactsViewModel;
    private final CustomerContactsFilterViewModel customerContactsFilterViewModel;

    public CustomerContactsView(
            ExcelExporter excelExporter,
            List<CustomerContact> items,
            PopupHandler popupHandler,
            CustomerRepository customerRepository,
            CustomerContactsViewModel customerContactsViewModel,
            CustomerContactsFilterViewModel customerContactsFilterViewModel
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forCustomerContact(),
                items
        );
        this.customerRepository = customerRepository;
        this.customerContactsViewModel = customerContactsViewModel;
        this.customerContactsFilterViewModel = customerContactsFilterViewModel;
        customerContactsViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showCustomerContactAddEditView(null, customerContact -> {
            customerContactsViewModel.create(
                    customerContact,
                    c -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, CustomerContact item) {
        showCustomerContactAddEditView(item, customerContact -> {
            customerContactsViewModel.fullUpdate(
                    customerContact,
                    customerContact.id(),
                    c -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showCustomerContactAddEditView(
            @Nullable CustomerContact customerContact,
            Consumer<CustomerContact> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("customer_contact_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new CustomerContactAddEditView(popupHandler, customerContact, onOkButtonClick,
                this::clearBottomPanel,
                view -> {
                    customerRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Customer>> call, @NonNull Response<List<Customer>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forCustomer(),
                                    (event, customer) -> {
                                        view.setPickedCustomer(customer);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            Platform.runLater(() -> setRightPanelContentView(v.getRoot()));
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Customer>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<CustomerContactAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, CustomerContact item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    customerContactsViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = CustomerContactsFilterView.create(
                customerContactsFilterViewModel,
                filters -> {
                    customerContactsViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel,
                view -> {
                    customerRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Customer>> call, @NonNull Response<List<Customer>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forCustomer(),
                                    (event, customer) -> {
                                        view.setPickedCustomer(customer);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            Platform.runLater(() -> setRightPanelContentView(v.getRoot()));
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Customer>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<CustomerContact> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "customer_contacts_" + System.currentTimeMillis(),
                                "CustomerContacts",
                                List.of("ID", "Customer name", "Phone number", "Email", "Address"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.customer().name(),
                                        requireNonNullElse(item.phoneNumber(), "N/A"),
                                        requireNonNullElse(item.email(), "N/A"),
                                        requireNonNullElse(item.address(), "N/A")
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
