package com.sixtyninefourtwenty.thesisapp.frontend.views.customers.filter;

import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Setter
@Getter
public class CustomerFilterViewModel {
    @Nullable
    private String nameSearchQuery;
}
