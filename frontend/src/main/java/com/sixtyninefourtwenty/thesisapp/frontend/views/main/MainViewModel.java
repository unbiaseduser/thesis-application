package com.sixtyninefourtwenty.thesisapp.frontend.views.main;

import com.sixtyninefourtwenty.thesisapp.frontend.Screen;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

public class MainViewModel {

    private Screen screen = Screen.LOGIN;

    public void setScreen(Screen screen) {
        this.screen = screen;
        if (screenListener != null) {
            screenListener.accept(screen);
        }
    }

    @Nullable
    private Consumer<Screen> screenListener;

    public void setScreenListener(Consumer<Screen> screenListener) {
        this.screenListener = screenListener;
        if (screenListener != null) {
            screenListener.accept(screen);
        }
    }

}
