package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces.View;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DataDialog<T> implements View {

    private final List<TableColumn<T, ?>> tableColumns;
    @Getter
    private final List<T> items;
    private final Runnable onOkButtonAction;
    private final Button[] additionalButtons;

    public static <T> DataDialog<T> create(
            List<TableColumn<T, ?>> tableColumns,
            List<T> items,
            Runnable onOkButtonAction,
            Button... additionalButtons
    ) {
        final var fxmlLoader = Utils.fxmlLoader("data_dialog");
        fxmlLoader.setControllerFactory(clazz -> new DataDialog<>(tableColumns, items, onOkButtonAction, additionalButtons));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private HBox buttonContainer;
    @FXML
    private TableView<T> table;
    @FXML
    private Button okButton;
    @FXML
    private BorderPane root;

    @FXML
    private void initialize() {
        table.getColumns().addAll(tableColumns);
        table.setItems(FXCollections.observableList(items));
        okButton.setOnAction(event -> onOkButtonAction.run());
        if (additionalButtons.length > 0) {
            final var list = new ArrayList<>(List.of(additionalButtons));
            list.removeFirst();
            addButtons(additionalButtons[0], list.toArray(new Button[0]));
        }
    }

    public void addButtons(Button button, Button... more) {
        HBox.setMargin(button, new Insets(5));
        for (final var additionalButton : more) {
            HBox.setMargin(additionalButton, new Insets(5));
        }
        final var buttonContainerChildren = buttonContainer.getChildren();
        buttonContainerChildren.add(button);
        buttonContainerChildren.addAll(more);
        buttonContainerChildren.remove(okButton);
        buttonContainerChildren.addLast(okButton);
    }

    @Override
    public BorderPane getRoot() {
        return root;
    }
}
