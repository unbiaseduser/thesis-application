package com.sixtyninefourtwenty.thesisapp.frontend.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;

public record MaterialAndStock(Material material, long stock) {
}
