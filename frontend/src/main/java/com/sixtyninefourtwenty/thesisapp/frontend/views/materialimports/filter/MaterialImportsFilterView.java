package com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class MaterialImportsFilterView extends CommonDialogView {

    private final MaterialImportsFilterViewModel materialImportsFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<MaterialImport>>> onOkButtonClick;
    private final Runnable onCancelButtonClick;
    private final Consumer<MaterialImportsFilterView> onChooseMaterialButtonClick;
    private final Consumer<MaterialImportsFilterView> onChooseMaterialSupplierButtonClick;
    @FXML
    private Button clearDateButton;
    @FXML
    private Button clearMaterialButton;
    @FXML
    private Button clearSupplierButton;

    public static MaterialImportsFilterView create(
            MaterialImportsFilterViewModel materialImportsFilterViewModel,
            Consumer<? super Collection<? extends Predicate<MaterialImport>>> onOkButtonClick,
            Runnable onCancelButtonClick,
            Consumer<MaterialImportsFilterView> onChooseMaterialButtonClick,
            Consumer<MaterialImportsFilterView> onChooseMaterialSupplierButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_import_filter");
        fxmlLoader.setControllerFactory(clazz -> new MaterialImportsFilterView(materialImportsFilterViewModel, onOkButtonClick, onCancelButtonClick, onChooseMaterialButtonClick, onChooseMaterialSupplierButtonClick));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @Nullable
    private Material pickedMaterial;
    @Nullable
    private MaterialSupplier pickedMaterialSupplier;

    @FXML
    private DatePicker datePicker;
    @FXML
    private Spinner<Integer> quantitySpinner;
    @FXML
    private TextArea materialTextArea;
    @FXML
    private Button chooseMaterialButton;
    @FXML
    private Button chooseMaterialSupplierButton;
    @FXML
    private TextArea materialSupplierTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedMaterial(@Nullable Material material) {
        this.pickedMaterial = material;
        materialTextArea.setText(material != null ? material.name() : null);
    }

    public void setPickedMaterialSupplier(@Nullable MaterialSupplier materialSupplier) {
        this.pickedMaterialSupplier = materialSupplier;
        materialSupplierTextArea.setText(materialSupplier != null ? materialSupplier.name() : null);
    }

    @FXML
    private void initialize() {
        setPickedMaterial(materialImportsFilterViewModel.getMaterialSearchQuery());
        setPickedMaterialSupplier(materialImportsFilterViewModel.getMaterialSupplierSearchQuery());
        quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, materialImportsFilterViewModel.getQuantitySearchQuery()));
        datePicker.setValue(materialImportsFilterViewModel.getDateSearchQuery());
        chooseMaterialButton.setOnAction(event -> onChooseMaterialButtonClick.accept(this));
        chooseMaterialSupplierButton.setOnAction(event -> onChooseMaterialSupplierButtonClick.accept(this));
        clearSupplierButton.setOnAction(event -> setPickedMaterialSupplier(null));
        clearMaterialButton.setOnAction(event -> setPickedMaterial(null));
        clearDateButton.setOnAction(event -> datePicker.setValue(null));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<MaterialImport>>(4);
        if (pickedMaterial != null) {
            filters.add(i -> pickedMaterial.equals(i.material()));
        }
        if (pickedMaterialSupplier != null) {
            filters.add(i -> pickedMaterialSupplier.equals(i.supplier()));
        }
        final var quantity = quantitySpinner.getValue();
        if (quantity > 0) {
            filters.add(i -> i.quantity() == quantity);
        }
        final var date = datePicker.getValue();
        if (date != null) {
            filters.add(i -> i.time().toLocalDate().isEqual(date));
        }
        materialImportsFilterViewModel.setMaterialSearchQuery(pickedMaterial);
        materialImportsFilterViewModel.setMaterialSupplierSearchQuery(pickedMaterialSupplier);
        materialImportsFilterViewModel.setQuantitySearchQuery(quantity);
        materialImportsFilterViewModel.setDateSearchQuery(date);
        onOkButtonClick.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }
}
