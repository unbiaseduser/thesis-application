package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface CustomerContactRepository {

    @GET("customercontacts")
    Call<List<CustomerContact>> findAll();

    @GET("customercontacts")
    Call<List<CustomerContact>> getContactsForCustomer(@Query("customerId") long customerId);

    @GET("customercontacts/{id}")
    Call<CustomerContact> findById(@Path("id") long id);

    @POST("customercontacts")
    Call<CustomerContact> create(@Body CustomerContact customerContact);

    @PUT("customercontacts/{id}")
    Call<CustomerContact> fullUpdate(@Body CustomerContact customerContact, @Path("id") long id);

    @DELETE("customercontacts/{id}")
    Call<Void> delete(@Path("id") long id);

    static CustomerContactRepository create() {
        return Utils.createRetrofitBackedRepository(CustomerContactRepository.class);
    }

}
