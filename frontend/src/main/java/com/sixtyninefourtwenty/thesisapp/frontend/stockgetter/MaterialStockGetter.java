package com.sixtyninefourtwenty.thesisapp.frontend.stockgetter;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MaterialStockGetter {

    @GET("materialstock/{id}")
    Call<Long> calculateStockForMaterialId(@Path("id") long materialId);

    static MaterialStockGetter create() {
        return Utils.createRetrofitBackedRepository(MaterialStockGetter.class);
    }

}
