package com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Setter
@Getter
public class CustomerContactsFilterViewModel {
    @Nullable
    private Customer customerSearchQuery;
    @Nullable
    private String phoneNumberSearchQuery;
    @Nullable
    private String addressSearchQuery;
    @Nullable
    private String emailSearchQuery;
}
