package com.sixtyninefourtwenty.thesisapp.frontend.views.materials.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class MaterialsViewModel extends CommonViewModel<Material> {

    private final MaterialRepository materialRepository;

    public MaterialsViewModel(MaterialRepository materialRepository, List<Material> items) {
        super(items);
        this.materialRepository = materialRepository;
    }

    public void create(
            Material material,
            Consumer<Material> onResponse,
            Runnable onFailure
    ) {
        materialRepository.create(material).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Material> call, Response<Material> response) {
                preFilteredList.add(requireNonNull(response.body()));
                onResponse.accept(requireNonNull(response.body()));
            }

            @Override
            public void onFailure(Call<Material> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void fullUpdate(
            Material material,
            long id,
            Consumer<Material> onResponse,
            Runnable onFailure
    ) {
        materialRepository.fullUpdate(material, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Material> call, Response<Material> response) {
                final var newItem = requireNonNull(response.body());
                preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                onResponse.accept(newItem);
            }

            @Override
            public void onFailure(Call<Material> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void delete(
            Material material,
            Runnable onResponse,
            Runnable onFailure,
            Runnable onFailureResponse
    ) {
        materialRepository.delete(material.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    preFilteredList.remove(material);
                    onResponse.run();
                } else {
                    onFailureResponse.run();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
