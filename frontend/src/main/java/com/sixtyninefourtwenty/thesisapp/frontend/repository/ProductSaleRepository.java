package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ProductSaleRepository {

    @GET("productsales")
    Call<List<ProductSale>> findAll();

    @GET("productsales")
    Call<List<ProductSale>> getSalesForProduct(@Query("productId") long productId);

    @GET("productsales/{id}")
    Call<ProductSale> findById(@Path("id") long id);

    @POST("productsales")
    Call<Result<ProductSale>> tryCreate(@Body ProductSale productSale);

    @PUT("productsales/{id}")
    Call<Result<ProductSale>> tryFullUpdate(@Body ProductSale productSale, @Path("id") long id);

    @DELETE("productsales/{id}")
    Call<Void> delete(@Path("id") long id);

    static ProductSaleRepository create() {
        return Utils.createRetrofitBackedRepository(ProductSaleRepository.class);
    }

}
