package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.ProductRecipePart;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ProductRecipePartRepository {

    @GET("productrecipeparts")
    Call<List<ProductRecipePart>> findAll();

    @GET("productrecipeparts")
    Call<List<ProductRecipePart>> getRecipeForProduct(@Query("productId") long productId);

    @GET("productrecipeparts/{id}")
    Call<ProductRecipePart> findById(@Path("id") long id);

    @POST("productrecipeparts")
    Call<Result<ProductRecipePart>> tryCreate(@Body ProductRecipePart productRecipePart);

    @PUT("productrecipeparts/{id}")
    Call<Result<ProductRecipePart>> tryFullUpdate(@Body ProductRecipePart productRecipePart, @Path("id") long id);

    @DELETE("productrecipeparts/{id}")
    Call<Void> delete(@Path("id") long id);

    static ProductRecipePartRepository create() {
        return Utils.createRetrofitBackedRepository(ProductRecipePartRepository.class);
    }

}
