package com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces;

import javafx.scene.Node;

public interface View {
    Node getRoot();
}
