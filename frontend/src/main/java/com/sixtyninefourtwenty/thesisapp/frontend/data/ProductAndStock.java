package com.sixtyninefourtwenty.thesisapp.frontend.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;

public record ProductAndStock(Product product, long stock) {
}
