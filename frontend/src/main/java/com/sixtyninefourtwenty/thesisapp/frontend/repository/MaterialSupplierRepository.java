package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface MaterialSupplierRepository {

    @GET("materialsuppliers")
    Call<List<MaterialSupplier>> findAll();

    @GET("materialsuppliers/{id}")
    Call<MaterialSupplier> findById(@Path("id") long id);

    @POST("materialsuppliers")
    Call<MaterialSupplier> create(@Body MaterialSupplier materialSupplier);

    @PUT("materialsuppliers/{id}")
    Call<MaterialSupplier> fullUpdate(@Body MaterialSupplier materialSupplier, @Path("id") long id);

    @DELETE("materialsuppliers/{id}")
    Call<Void> delete(@Path("id") long id);

    static MaterialSupplierRepository create() {
        return Utils.createRetrofitBackedRepository(MaterialSupplierRepository.class);
    }

}
