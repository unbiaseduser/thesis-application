package com.sixtyninefourtwenty.thesisapp.frontend.views.customers.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.CustomerRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import org.jspecify.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class CustomersViewModel extends CommonViewModel<Customer> {

    private final CustomerRepository customerRepository;

    public CustomersViewModel(CustomerRepository customerRepository, List<Customer> items) {
        super(items);
        this.customerRepository = customerRepository;
    }

    public void create(
            Customer customer,
            Consumer<Customer> onResponse,
            Runnable onFailure
    ) {
        customerRepository.create(customer).enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<Customer> call, @NonNull Response<Customer> response) {
                preFilteredList.add(requireNonNull(response.body()));
                onResponse.accept(requireNonNull(response.body()));
            }

            @Override
            public void onFailure(@NonNull Call<Customer> call, @NonNull Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void fullUpdate(
            Customer customer,
            long id,
            Consumer<Customer> onResponse,
            Runnable onFailure
    ) {
        customerRepository.fullUpdate(customer, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                final var newItem = requireNonNull(response.body());
                preFilteredList.replaceAll(c -> c.id() != newItem.id() ? c : newItem);
                onResponse.accept(newItem);
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void delete(
            Customer customer,
            Runnable onResponse,
            Runnable onFailure,
            Runnable onFailureResponse
    ) {
        customerRepository.delete(customer.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    preFilteredList.remove(customer);
                    onResponse.run();
                } else {
                    onFailureResponse.run();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
