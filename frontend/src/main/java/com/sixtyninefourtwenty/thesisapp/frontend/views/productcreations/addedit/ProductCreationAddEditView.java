package com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.time.LocalDate;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class ProductCreationAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final ProductCreation productCreation;
    private final Consumer<ProductCreation> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<ProductCreationAddEditView> onChooseProductButtonAction;
    @Nullable
    private Product pickedProduct;

    @FXML
    private DatePicker datePicker;
    @FXML
    private Spinner<Integer> quantitySpinner;
    @FXML
    private Button chooseProductButton;
    @FXML
    private TextArea productTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedProduct(Product product) {
        this.pickedProduct = product;
        productTextArea.setText(product.name());
    }

    @FXML
    protected void initialize() {
        if (productCreation != null) {
            setPickedProduct(productCreation.product());
            quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, productCreation.quantity()));
            datePicker.setValue(productCreation.time().toLocalDate());
        } else {
            quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE));
            datePicker.setValue(LocalDate.now());
        }
        chooseProductButton.setOnAction(event -> onChooseProductButtonAction.accept(this));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        if (pickedProduct == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Product not picked",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonAction.accept(new ProductCreation(
                productCreation != null ? productCreation.id() : 0,
                pickedProduct,
                quantitySpinner.getValue(),
                datePicker.getValue().atStartOfDay()
        ));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
