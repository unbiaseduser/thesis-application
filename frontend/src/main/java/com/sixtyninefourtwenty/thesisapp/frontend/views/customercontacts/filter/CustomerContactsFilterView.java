package com.sixtyninefourtwenty.thesisapp.frontend.views.customercontacts.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.CustomerContact;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class CustomerContactsFilterView extends CommonDialogView {

    @FXML
    private Button clearCustomerButton;

    public static CustomerContactsFilterView create(
            CustomerContactsFilterViewModel customerContactsFilterViewModel,
            Consumer<? super Collection<? extends Predicate<CustomerContact>>> onOkButtonClick,
            Runnable onCancelButtonClick,
            Consumer<? super CustomerContactsFilterView> onChooseCustomerButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("customer_contact_filter");
        fxmlLoader.setControllerFactory(clazz -> new CustomerContactsFilterView(customerContactsFilterViewModel, onOkButtonClick, onCancelButtonClick, onChooseCustomerButtonClick));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    private final CustomerContactsFilterViewModel customerContactsFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<CustomerContact>>> onOkButtonClick;
    private final Runnable onCancelButtonClick;
    private final Consumer<? super CustomerContactsFilterView> onChooseCustomerButtonClick;
    @Nullable
    private Customer pickedCustomer;

    @FXML
    private AnchorPane root;

    @FXML
    private Button chooseCustomerButton;

    @FXML
    private TextArea customerTextArea;

    @FXML
    private TextField addressTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField phoneNumberTextField;

    public void setPickedCustomer(@Nullable Customer customer) {
        this.pickedCustomer = customer;
        customerTextArea.setText(customer != null ? customer.name() : null);
    }

    @FXML
    private void initialize() {
        setPickedCustomer(customerContactsFilterViewModel.getCustomerSearchQuery());
        phoneNumberTextField.setText(customerContactsFilterViewModel.getPhoneNumberSearchQuery());
        addressTextField.setText(customerContactsFilterViewModel.getAddressSearchQuery());
        emailTextField.setText(customerContactsFilterViewModel.getEmailSearchQuery());
        chooseCustomerButton.setOnAction(event -> onChooseCustomerButtonClick.accept(this));
        clearCustomerButton.setOnAction(event -> setPickedCustomer(null));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {

        final BiPredicate<@Nullable String, @Nullable String> f = (arg1, arg2) -> {
            if (arg1 == null && arg2 == null) {
                return true;
            }
            if (arg1 == null || arg2 == null) {
                return false;
            }
            return arg1.contains(arg2);
        };

        final var filters = new ArrayList<Predicate<CustomerContact>>();
        if (pickedCustomer != null) {
            filters.add(cc -> cc.customer().equals(pickedCustomer));
        }
        final var phoneNumberText = phoneNumberTextField.getText();
        if (!Utils.isNullOrBlank(phoneNumberText)) {
            filters.add(cc -> f.test(cc.phoneNumber(), phoneNumberText));
        }
        final var addressText = addressTextField.getText();
        if (!Utils.isNullOrBlank(addressText)) {
            filters.add(cc -> f.test(cc.address(), addressText));
        }
        final var emailText = emailTextField.getText();
        if (!Utils.isNullOrBlank(emailText)) {
            filters.add(cc -> f.test(cc.email(), emailText));
        }
        customerContactsFilterViewModel.setCustomerSearchQuery(pickedCustomer);
        customerContactsFilterViewModel.setPhoneNumberSearchQuery(phoneNumberText);
        customerContactsFilterViewModel.setAddressSearchQuery(addressText);
        customerContactsFilterViewModel.setEmailSearchQuery(emailText);
        onOkButtonClick.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonClick.run();
    }
}
