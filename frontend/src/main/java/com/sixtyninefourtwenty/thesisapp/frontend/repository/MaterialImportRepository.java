package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface MaterialImportRepository {

    @GET("materialimports")
    Call<List<MaterialImport>> findAll();

    @GET("materialimports")
    Call<List<MaterialImport>> getImportsForMaterial(@Query("materialId") long materialId);

    @GET("materialimports/{id}")
    Call<MaterialImport> findById(@Path("id") long id);

    @POST("materialimports")
    Call<Result<MaterialImport>> tryCreate(@Body MaterialImport materialImport);

    @PUT("materialimports/{id}")
    Call<Result<MaterialImport>> tryFullUpdate(@Body MaterialImport materialImport, @Path("id") long id);

    @DELETE("materialimports/{id}")
    Call<Result<MaterialImport>> tryDelete(@Path("id") long id);

    static MaterialImportRepository create() {
        return Utils.createRetrofitBackedRepository(MaterialImportRepository.class);
    }

}
