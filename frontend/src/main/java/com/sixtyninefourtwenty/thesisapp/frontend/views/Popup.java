package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.function.Function;

public class Popup extends StackPane {

    public static Popup createAndShow(Pane context, Function<? super Popup, ? extends Node> content) {
        final var popup = new Popup(context);
        popup.setContentView(content.apply(popup));
        popup.show();
        return popup;
    }

    private final Pane context;

    public Popup(Pane context) {
        this.context = context;
        this.setAlignment(Pos.CENTER);
        this.setStyle("-fx-background-color: #88888855;");
    }

    public Popup(Pane context, Node content) {
        this(context);
        setContentView(content);
    }

    public Popup(Pane context, Function<? super Popup, ? extends Node> content) {
        this(context);
        setContentView(content.apply(this));
    }

    public void setContentView(Node content) {
        Utils.runOnApplicationThread(() -> {
            content.setStyle("-fx-background-color: white; -fx-min-width: 400; -fx-min-height: 300;");
            ViewUtils.resetChildren(this, content);
        });
    }

    public void show() {
        Utils.runOnApplicationThread(() -> context.getChildren().add(this));
    }

    public boolean isShowing() {
        return context.getChildren().contains(this);
    }

    public void close() {
        Utils.runOnApplicationThread(() -> context.getChildren().remove(this));
    }

}
