package com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Getter
@Setter
public class ProductRecipesFilterViewModel {
    @Nullable
    private Material materialSearchQuery;
    @Nullable
    private Product productSearchQuery;
    private int materialQuantitySearchQuery = 0;
}
