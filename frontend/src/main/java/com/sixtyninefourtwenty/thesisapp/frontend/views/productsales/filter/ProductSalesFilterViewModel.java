package com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

import java.time.LocalDate;

@Getter
@Setter
public class ProductSalesFilterViewModel {
    @Nullable
    private Product productSearchQuery;
    @Nullable
    private Customer customerSearchQuery;
    private int quantitySearchQuery = 0;
    @Nullable
    private LocalDate dateSearchQuery;
}
