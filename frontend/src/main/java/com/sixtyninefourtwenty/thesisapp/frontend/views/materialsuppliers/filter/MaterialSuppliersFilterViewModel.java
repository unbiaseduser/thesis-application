package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.filter;

import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Setter
@Getter
public class MaterialSuppliersFilterViewModel {
    @Nullable
    private String nameSearchQuery;
}
