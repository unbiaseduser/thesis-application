package com.sixtyninefourtwenty.thesisapp.frontend.repository;

import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ProductCreationRepository {

    @GET("productcreations")
    Call<List<ProductCreation>> findAll();

    @GET("productcreations")
    Call<List<ProductCreation>> getCreationsForProduct(@Query("productId") long productId);

    @GET("productcreations/{id}")
    Call<ProductCreation> findById(@Path("id") long id);

    @POST("productcreations")
    Call<Result<ProductCreation>> tryCreate(@Body ProductCreation productCreation);

    @PUT("productcreations/{id}")
    Call<Result<ProductCreation>> tryFullUpdate(@Body ProductCreation productCreation, @Path("id") long id);

    @DELETE("productcreations/{id}")
    Call<Result<ProductCreation>> tryDelete(@Path("id") long id);

    static ProductCreationRepository create() {
        return Utils.createRetrofitBackedRepository(ProductCreationRepository.class);
    }

}
