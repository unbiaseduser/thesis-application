package com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Material;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductRecipePart;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.addedit.ProductRecipeAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.filter.ProductRecipesFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productrecipes.filter.ProductRecipesFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductRecipesView extends CommonDataView<ProductRecipePart> {

    private final ProductRecipesViewModel productRecipesViewModel;
    private final ProductRecipesFilterViewModel productRecipesFilterViewModel;
    private final ProductRepository productRepository;
    private final MaterialRepository materialRepository;

    public ProductRecipesView(
            ExcelExporter excelExporter,
            List<ProductRecipePart> items,
            PopupHandler popupHandler,
            ProductRecipesViewModel productRecipesViewModel,
            ProductRecipesFilterViewModel productRecipesFilterViewModel,
            ProductRepository productRepository,
            MaterialRepository materialRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forProductRecipe(),
                items
        );
        this.productRecipesViewModel = productRecipesViewModel;
        this.productRecipesFilterViewModel = productRecipesFilterViewModel;
        this.productRepository = productRepository;
        this.materialRepository = materialRepository;
        productRecipesViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showProductRecipeAddEditView(null, productRecipePart -> {
            productRecipesViewModel.tryCreate(
                    productRecipePart,
                    p -> {
                    },
                    msg -> {
                        Platform.runLater(() -> {
                            final var popup = popupHandler.createPopup();
                            popup.setContentView(GenericErrorView.create(
                                    msg, popup::close
                            ).getRoot());
                            popup.show();
                        });
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, ProductRecipePart item) {
        showProductRecipeAddEditView(item, productRecipePart -> {
            productRecipesViewModel.tryFullUpdate(
                    productRecipePart,
                    productRecipePart.id(),
                    p -> {
                    },
                    msg -> {
                        Platform.runLater(() -> {
                            final var popup = popupHandler.createPopup();
                            popup.setContentView(GenericErrorView.create(
                                    msg, popup::close
                            ).getRoot());
                            popup.show();
                        });
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showProductRecipeAddEditView(
            @Nullable ProductRecipePart productRecipePart,
            Consumer<ProductRecipePart> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_recipe_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new ProductRecipeAddEditView(popupHandler,
                productRecipePart,
                onOkButtonClick,
                this::clearBottomPanel,
                view -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forProduct(),
                                    (event, product) -> {
                                        view.setPickedProduct(product);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                },
                view -> {
                    materialRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Material>> call, @NonNull Response<List<Material>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterial(),
                                    (event, material) -> {
                                        view.setPickedMaterial(material);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Material>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        ));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<ProductRecipeAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, ProductRecipePart item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    productRecipesViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = ProductRecipesFilterView.create(
                productRecipesFilterViewModel,
                filters -> {
                    productRecipesViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel,
                view -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forProduct(),
                                    (event, product) -> {
                                        view.setPickedProduct(product);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                },
                view -> {
                    materialRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Material>> call, @NonNull Response<List<Material>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forMaterial(),
                                    (event, material) -> {
                                        view.setPickedMaterial(material);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Material>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<ProductRecipePart> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "product_recipes_" + System.currentTimeMillis(),
                                "ProductRecipes",
                                List.of("ID", "Product name", "Material name", "Material quantity"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.product().name(),
                                        item.material().name(),
                                        Integer.toString(item.materialQuantity())
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
