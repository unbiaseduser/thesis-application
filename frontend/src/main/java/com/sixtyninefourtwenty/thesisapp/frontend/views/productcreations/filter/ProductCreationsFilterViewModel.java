package com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

import java.time.LocalDate;

@Setter
@Getter
public class ProductCreationsFilterViewModel {
    @Nullable
    private Product productSearchQuery;
    private int quantitySearchQuery = 0;
    @Nullable
    private LocalDate dateSearchQuery;
}
