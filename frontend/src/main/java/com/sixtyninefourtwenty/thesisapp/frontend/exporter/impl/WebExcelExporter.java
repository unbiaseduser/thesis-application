package com.sixtyninefourtwenty.thesisapp.frontend.exporter.impl;

import com.jpro.webapi.WebAPI;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.nio.file.Path;

@AllArgsConstructor
public class WebExcelExporter extends ExcelExporter {

    private final WebAPI webAPI;

    @Override
    protected void handleSavingFile(Path path) throws IOException {
        webAPI.downloadURL(path.toUri().toURL());
    }

}
