package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import lombok.Getter;
import lombok.Setter;
import org.jspecify.annotations.Nullable;

@Setter
@Getter
public class MaterialSupplierContactsFilterViewModel {
    @Nullable
    private MaterialSupplier supplierSearchQuery;
    @Nullable
    private String addressSearchQuery;
    @Nullable
    private String emailSearchQuery;
    @Nullable
    private String phoneNumberQuery;
}
