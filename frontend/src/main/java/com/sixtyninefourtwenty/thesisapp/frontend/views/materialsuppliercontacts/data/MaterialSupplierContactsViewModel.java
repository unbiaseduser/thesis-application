package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.data;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialSupplierContactRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class MaterialSupplierContactsViewModel extends CommonViewModel<MaterialSupplierContact> {

    private final MaterialSupplierContactRepository materialSupplierContactRepository;

    public MaterialSupplierContactsViewModel(MaterialSupplierContactRepository materialSupplierContactRepository, List<MaterialSupplierContact> items) {
        super(items);
        this.materialSupplierContactRepository = materialSupplierContactRepository;
    }

    public void create(
            MaterialSupplierContact contact,
            Consumer<MaterialSupplierContact> onResponse,
            Runnable onFailure
    ) {
        materialSupplierContactRepository.create(contact).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<MaterialSupplierContact> call, Response<MaterialSupplierContact> response) {
                preFilteredList.add(requireNonNull(response.body()));
                onResponse.accept(requireNonNull(response.body()));
            }

            @Override
            public void onFailure(Call<MaterialSupplierContact> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void fullUpdate(
            MaterialSupplierContact contact,
            long id,
            Consumer<MaterialSupplierContact> onResponse,
            Runnable onFailure
    ) {
        materialSupplierContactRepository.fullUpdate(contact, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<MaterialSupplierContact> call, Response<MaterialSupplierContact> response) {
                final var newItem = requireNonNull(response.body());
                preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                onResponse.accept(newItem);
            }

            @Override
            public void onFailure(Call<MaterialSupplierContact> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void delete(
            MaterialSupplierContact contact,
            Runnable onResponse,
            Runnable onFailure
    ) {
        materialSupplierContactRepository.delete(contact.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                preFilteredList.remove(contact);
                onResponse.run();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
