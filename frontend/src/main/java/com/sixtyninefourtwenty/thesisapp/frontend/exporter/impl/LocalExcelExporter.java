package com.sixtyninefourtwenty.thesisapp.frontend.exporter.impl;

import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@AllArgsConstructor
public class LocalExcelExporter extends ExcelExporter {

    private final Window window;

    @Override
    protected void handleSavingFile(Path path) throws IOException {
        final var chooser = new FileChooser();
        chooser.setInitialFileName(path.getFileName().toString());
        final var dest = chooser.showSaveDialog(window);
        if (dest != null) {
            Files.copy(path, dest.toPath());
        }
    }

}
