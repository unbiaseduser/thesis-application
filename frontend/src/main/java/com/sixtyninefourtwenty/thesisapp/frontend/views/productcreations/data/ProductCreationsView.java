package com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.addedit.ProductCreationAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.filter.ProductCreationsFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.filter.ProductCreationsFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductCreationsView extends CommonDataView<ProductCreation> {

    private final ProductCreationsViewModel productCreationsViewModel;
    private final ProductCreationsFilterViewModel productCreationsFilterViewModel;
    private final ProductRepository productRepository;

    public ProductCreationsView(
            ExcelExporter excelExporter,
            List<ProductCreation> items,
            PopupHandler popupHandler,
            ProductCreationsViewModel productCreationsViewModel,
            ProductCreationsFilterViewModel productCreationsFilterViewModel,
            ProductRepository productRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forProductCreation(),
                items
        );
        this.productCreationsViewModel = productCreationsViewModel;
        this.productCreationsFilterViewModel = productCreationsFilterViewModel;
        this.productRepository = productRepository;
        productCreationsViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showProductCreationAddEditView(null, productCreation -> {
            productCreationsViewModel.tryCreate(
                    productCreation,
                    c -> {
                    },
                    msg -> {
                        Platform.runLater(() -> {
                            final var popup = popupHandler.createPopup();
                            popup.setContentView(GenericErrorView.create(
                                    msg, popup::close
                            ).getRoot());
                            popup.show();
                        });
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, ProductCreation item) {
        showProductCreationAddEditView(item, productCreation -> {
            productCreationsViewModel.tryFullUpdate(
                    productCreation,
                    productCreation.id(),
                    c -> {
                    },
                    msg -> {
                        Platform.runLater(() -> {
                            final var popup = popupHandler.createPopup();
                            popup.setContentView(GenericErrorView.create(
                                    msg, popup::close
                            ).getRoot());
                            popup.show();
                        });
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showProductCreationAddEditView(
            @Nullable ProductCreation productCreation,
            Consumer<ProductCreation> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_creation_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new ProductCreationAddEditView(popupHandler,
                productCreation,
                onOkButtonClick,
                this::clearBottomPanel,
                view -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forProduct(),
                                    (event, product) -> {
                                        view.setPickedProduct(product);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        ));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<ProductCreationAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, ProductCreation item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    productCreationsViewModel.tryDelete(
                            item,
                            () -> {
                            },
                            msg -> {
                                Platform.runLater(() -> {
                                    final var popup2 = popupHandler.createPopup();
                                    popup2.setContentView(GenericErrorView.create(
                                            msg, popup2::close
                                    ).getRoot());
                                    popup2.show();
                                });
                            },
                            () -> {
                            },
                            this::showServerFailurePopup
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = ProductCreationsFilterView.create(
                productCreationsFilterViewModel,
                filters -> {
                    productCreationsViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel,
                view -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forProduct(),
                                    (event, product) -> {
                                        view.setPickedProduct(product);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<ProductCreation> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "product_creations_" + System.currentTimeMillis(),
                                "ProductCreations",
                                List.of("ID", "Product name", "Quantity", "Date"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.product().name(),
                                        Integer.toString(item.quantity()),
                                        item.time().toLocalDate().toString()
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
