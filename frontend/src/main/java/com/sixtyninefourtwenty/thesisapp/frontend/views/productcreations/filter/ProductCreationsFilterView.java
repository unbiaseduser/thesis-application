package com.sixtyninefourtwenty.thesisapp.frontend.views.productcreations.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductCreation;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class ProductCreationsFilterView extends CommonDialogView {

    private final ProductCreationsFilterViewModel productCreationsFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<ProductCreation>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<ProductCreationsFilterView> onChooseProductButtonAction;
    @FXML
    private Button clearProductButton;
    @FXML
    private Button clearDateButton;
    @Nullable
    private Product pickedProduct;

    public static ProductCreationsFilterView create(
            ProductCreationsFilterViewModel productCreationsViewModel,
            Consumer<? super Collection<? extends Predicate<ProductCreation>>> onOkButtonAction,
            Runnable onCancelButtonAction,
            Consumer<ProductCreationsFilterView> onChooseProductButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_creation_filter");
        fxmlLoader.setControllerFactory(clazz -> new ProductCreationsFilterView(productCreationsViewModel, onOkButtonAction, onCancelButtonAction, onChooseProductButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private DatePicker datePicker;
    @FXML
    private Spinner<Integer> quantitySpinner;
    @FXML
    private Button chooseProductButton;
    @FXML
    private TextArea productTextArea;
    @FXML
    private AnchorPane root;

    public void setPickedProduct(@Nullable Product product) {
        this.pickedProduct = product;
        productTextArea.setText(product != null ? product.name() : null);
    }

    @FXML
    private void initialize() {
        setPickedProduct(productCreationsFilterViewModel.getProductSearchQuery());
        quantitySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, productCreationsFilterViewModel.getQuantitySearchQuery()));
        datePicker.setValue(productCreationsFilterViewModel.getDateSearchQuery());
        chooseProductButton.setOnAction(event -> onChooseProductButtonAction.accept(this));
        clearProductButton.setOnAction(event -> setPickedProduct(null));
        clearDateButton.setOnAction(event -> datePicker.setValue(null));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<ProductCreation>>(3);
        if (pickedProduct != null) {
            filters.add(c -> pickedProduct.equals(c.product()));
        }
        final var quantity = quantitySpinner.getValue();
        if (quantity > 0) {
            filters.add(c -> c.quantity() == quantity);
        }
        final var date = datePicker.getValue();
        if (date != null) {
            filters.add(c -> c.time().toLocalDate().isEqual(date));
        }
        productCreationsFilterViewModel.setProductSearchQuery(pickedProduct);
        productCreationsFilterViewModel.setQuantitySearchQuery(quantity);
        productCreationsFilterViewModel.setDateSearchQuery(date);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }
}
