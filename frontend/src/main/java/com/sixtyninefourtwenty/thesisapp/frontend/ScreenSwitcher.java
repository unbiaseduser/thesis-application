package com.sixtyninefourtwenty.thesisapp.frontend;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public final class ScreenSwitcher {

    private final List<Consumer<Screen>> listeners = new ArrayList<>();

    public void addListener(Consumer<Screen> listener) {
        listeners.add(listener);
    }

    public void switchScreen(Screen screen) {
        for (final var listener : listeners) {
            listener.accept(screen);
        }
    }

}
