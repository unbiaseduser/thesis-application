package com.sixtyninefourtwenty.thesisapp.frontend.views.customers.filter;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class CustomerFilterView extends CommonDialogView {

    private final CustomerFilterViewModel customerFilterViewModel;
    private final Consumer<? super Collection<? extends Predicate<Customer>>> onOkButtonAction;
    private final Runnable onCancelButtonAction;

    public static CustomerFilterView create(
            CustomerFilterViewModel customerFilterViewModel,
            Consumer<? super Collection<? extends Predicate<Customer>>> onOkButtonAction,
            Runnable onCancelButtonAction
    ) {
        final var fxmlLoader = Utils.fxmlLoader("customer_filter");
        fxmlLoader.setControllerFactory(clazz -> new CustomerFilterView(customerFilterViewModel, onOkButtonAction, onCancelButtonAction));
        Utils.loadQuietly(fxmlLoader);
        return fxmlLoader.getController();
    }

    @FXML
    private TextField nameTextField;

    @FXML
    private Node root;

    @FXML
    protected void initialize() {
        nameTextField.setText(customerFilterViewModel.getNameSearchQuery());
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        final var filters = new ArrayList<Predicate<Customer>>(1);
        final var nameText = nameTextField.getText();
        if (!Utils.isNullOrBlank(nameText)) {
            filters.add(c -> c.name().contains(nameText));
        }
        customerFilterViewModel.setNameSearchQuery(nameText);
        onOkButtonAction.accept(filters);
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
