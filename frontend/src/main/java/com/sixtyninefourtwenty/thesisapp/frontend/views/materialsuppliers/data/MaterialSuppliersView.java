package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.data;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialSupplierContactRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.addedit.MaterialSupplierAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.filter.MaterialSuppliersFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliers.filter.MaterialSuppliersFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;

public class MaterialSuppliersView extends CommonDataView<MaterialSupplier> {

    private final MaterialSuppliersViewModel materialSuppliersViewModel;
    private final MaterialSuppliersFilterViewModel materialSuppliersFilterViewModel;
    private final MaterialSupplierContactRepository materialSupplierContactRepository;

    public MaterialSuppliersView(
            ExcelExporter excelExporter,
            List<MaterialSupplier> items,
            PopupHandler popupHandler,
            MaterialSuppliersViewModel materialSuppliersViewModel,
            MaterialSuppliersFilterViewModel materialSuppliersFilterViewModel,
            MaterialSupplierContactRepository materialSupplierContactRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forMaterialSupplier(),
                items
        );
        this.materialSuppliersViewModel = materialSuppliersViewModel;
        this.materialSuppliersFilterViewModel = materialSuppliersFilterViewModel;
        this.materialSupplierContactRepository = materialSupplierContactRepository;
        materialSuppliersViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));

        final var contactsButton = new Button("Contacts");
        contactsButton.setOnAction(event -> {
            final var selectedItems = getSelectedItems();
            if (selectedItems.size() != 1) {
                showChooseOneItemPopup();
                return;
            }
            materialSupplierContactRepository.getContactsForMaterialSupplier(selectedItems.getFirst().id()).enqueue(new Callback<>() {
                @Override
                public void onResponse(Call<List<MaterialSupplierContact>> call, Response<List<MaterialSupplierContact>> response) {
                    final var popup = popupHandler.createPopup();
                    final var dialog = DataDialog.create(
                            TableColumns.forMaterialSupplierContact(),
                            requireNonNull(response.body()),
                            popup::close
                    );
                    final var reportButton = new Button("Report");
                    reportButton.setOnAction(ae -> {
                        popup.close();
                        popupHandler.createPopupAndShow(popup1 -> new SimpleMessageDialogView(
                                "Do you want to save a report of these items?",
                                () -> {
                                    popup1.close();
                                    try {
                                        excelExporter.exportExcel(
                                                "material_supplier_contacts_" + System.currentTimeMillis(),
                                                "MaterialSupplierContacts",
                                                List.of("ID", "Customer name", "Phone number", "Email", "Address"),
                                                dialog.getItems(),
                                                item -> List.of(
                                                        Long.toString(item.id()),
                                                        item.supplier().name(),
                                                        requireNonNullElse(item.phoneNumber(), "N/A"),
                                                        requireNonNullElse(item.email(), "N/A"),
                                                        requireNonNullElse(item.address(), "N/A")
                                                )
                                        );
                                    } catch (IOException e) {
                                        popupHandler.createPopupAndShow(popup2 -> GenericErrorView.create(
                                                "Can't create report.",
                                                popup2::close
                                        ).getRoot());
                                    }
                                },
                                popup1::close
                        ).getRoot());
                    });
                    dialog.addButtons(reportButton);
                    popup.setContentView(dialog.getRoot());
                    popup.show();
                }

                @Override
                public void onFailure(Call<List<MaterialSupplierContact>> call, Throwable t) {

                }
            });
        });
        addButton(contactsButton);
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showMaterialSupplierAddEditView(null, materialSupplier -> {
            materialSuppliersViewModel.create(
                    materialSupplier,
                    s -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, MaterialSupplier item) {
        showMaterialSupplierAddEditView(item, materialSupplier -> {
            materialSuppliersViewModel.fullUpdate(
                    materialSupplier,
                    materialSupplier.id(),
                    s -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showMaterialSupplierAddEditView(
            @Nullable MaterialSupplier materialSupplier,
            Consumer<MaterialSupplier> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("material_supplier_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new MaterialSupplierAddEditView(popupHandler, materialSupplier, onOkButtonClick, this::clearBottomPanel));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<MaterialSupplierAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, MaterialSupplier item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    materialSuppliersViewModel.delete(
                            item,
                            () -> {
                            },
                            this::showServerFailurePopup,
                            () -> {
                                Platform.runLater(() -> {
                                    final var popup2 = popupHandler.createPopup();
                                    popup2.setContentView(GenericErrorView.create(
                                            "Can't delete this item.\nIt might have some data associated with it.",
                                            popup2::close
                                    ).getRoot());
                                    popup2.show();
                                });
                            }
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = MaterialSuppliersFilterView.create(
                materialSuppliersFilterViewModel,
                filters -> {
                    materialSuppliersViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<MaterialSupplier> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "material_suppliers_" + System.currentTimeMillis(),
                                "MaterialSuppliers",
                                List.of("ID", "Name"),
                                finalItems,
                                item -> List.of(
                                        Long.toString(item.id()),
                                        item.name()
                                )
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
