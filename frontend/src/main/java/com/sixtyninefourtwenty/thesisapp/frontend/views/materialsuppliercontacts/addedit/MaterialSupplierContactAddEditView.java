package com.sixtyninefourtwenty.thesisapp.frontend.views.materialsuppliercontacts.addedit;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplier;
import com.sixtyninefourtwenty.thesisapp.common.data.MaterialSupplierContact;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonDialogView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.GenericErrorView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.PopupHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import lombok.RequiredArgsConstructor;
import org.jspecify.annotations.Nullable;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class MaterialSupplierContactAddEditView extends CommonDialogView {

    private final PopupHandler popupHandler;
    @Nullable
    private final MaterialSupplierContact materialSupplierContact;
    private final Consumer<MaterialSupplierContact> onOkButtonAction;
    private final Runnable onCancelButtonAction;
    private final Consumer<MaterialSupplierContactAddEditView> onChooseSupplierButtonClick;
    @Nullable
    private MaterialSupplier pickedSupplier;

    @FXML
    private Button chooseSupplierButton;
    @FXML
    private TextArea supplierTextArea;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField phoneNumberTextField;
    @FXML
    private AnchorPane root;

    public void setPickedSupplier(MaterialSupplier materialSupplier) {
        this.pickedSupplier = materialSupplier;
        supplierTextArea.setText(materialSupplier.name());
    }

    @FXML
    protected void initialize() {
        if (materialSupplierContact != null) {
            setPickedSupplier(materialSupplierContact.supplier());
            phoneNumberTextField.setText(materialSupplierContact.phoneNumber());
            emailTextField.setText(materialSupplierContact.email());
            addressTextField.setText(materialSupplierContact.address());
        }
        chooseSupplierButton.setOnAction(event -> onChooseSupplierButtonClick.accept(this));
        setContentView(root);
    }

    @Override
    protected void onOkButtonAction(ActionEvent event) {
        if (pickedSupplier == null) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "Supplier not picked",
                    popup::close
            ).getRoot());
            return;
        }
        final var phoneNumberText = ViewUtils.getTextOrEmpty(phoneNumberTextField);
        final var emailText = ViewUtils.getTextOrEmpty(emailTextField);
        final var addressText = ViewUtils.getTextOrEmpty(addressTextField);
        if (phoneNumberText.isBlank() && emailText.isBlank() && addressText.isBlank()) {
            popupHandler.createPopupAndShow(popup -> GenericErrorView.create(
                    "One of phone number, email and address must be present",
                    popup::close
            ).getRoot());
            return;
        }
        onOkButtonAction.accept(new MaterialSupplierContact(
                materialSupplierContact != null ? materialSupplierContact.id() : 0,
                pickedSupplier,
                phoneNumberText,
                emailText,
                addressText
        ));
    }

    @Override
    protected void onCancelButtonAction(ActionEvent event) {
        onCancelButtonAction.run();
    }

}
