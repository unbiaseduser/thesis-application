package com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.data;

import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductSaleRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductSalesViewModel extends CommonViewModel<ProductSale> {

    private final ProductSaleRepository productSaleRepository;

    @SneakyThrows
    public ProductSalesViewModel(ProductSaleRepository productSaleRepository, List<ProductSale> items) {
        super(items);
        this.productSaleRepository = productSaleRepository;
    }

    public void tryCreate(
            ProductSale sale,
            Consumer<ProductSale> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productSaleRepository.tryCreate(sale).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductSale>> call, Response<Result<ProductSale>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductSale> s -> {
                        final var newItem = s.data();
                        preFilteredList.add(newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<ProductSale> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductSale>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void tryFullUpdate(
            ProductSale sale,
            long id,
            Consumer<ProductSale> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        productSaleRepository.tryFullUpdate(sale, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<ProductSale>> call, Response<Result<ProductSale>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<ProductSale> s -> {
                        final var newItem = s.data();
                        preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<ProductSale> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<ProductSale>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void delete(
            ProductSale sale,
            Runnable onResponse,
            Runnable onFailure
    ) {
        productSaleRepository.delete(sale.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                preFilteredList.remove(sale);
                onResponse.run();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

}
