package com.sixtyninefourtwenty.thesisapp.frontend.views;

import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.ViewUtils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.views.interfaces.View;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.util.List;

import static java.util.Objects.requireNonNullElse;

public abstract class CommonDataView<T> implements View {

    @SuppressWarnings("unchecked")
    protected CommonDataView(
            ExcelExporter excelExporter,
            PopupHandler popupHandler,
            List<TableColumn<T, ?>> tableColumns,
            List<T> initialItems
    ) {
        this.excelExporter = excelExporter;
        this.popupHandler = popupHandler;
        final var fxmlLoader = Utils.fxmlLoader("common_data");
        Utils.loadQuietly(fxmlLoader);
        final var root = fxmlLoader.<Parent>getRoot();
        rightPanel = (Pane) ViewUtils.findNodeById(root, "rightPanel");
        leftPanel = (Pane) ViewUtils.findNodeById(root, "leftPanel");
        table = (TableView<T>) ViewUtils.findNodeById(root, "table");
        buttonContainer = (HBox) ViewUtils.findNodeById(root, "buttonContainer");
        editButton = (Button) ViewUtils.findNodeById(root, "editButton");
        deleteButton = (Button) ViewUtils.findNodeById(root, "deleteButton");
        filterButton = (Button) ViewUtils.findNodeById(root, "filterButton");
        addButton = (Button) ViewUtils.findNodeById(root, "addButton");
        reportButton = (Button) ViewUtils.findNodeById(root, "reportButton");
        bottomPanel = (Pane) ViewUtils.findNodeById(root, "bottomPanel");
        this.root = root;
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.getColumns().addAll(tableColumns);
        table.setItems(FXCollections.observableList(initialItems));
        addButton.setOnAction(this::onAddButtonAction);
        editButton.setOnAction(event -> {
            final var items = table.getSelectionModel().getSelectedItems();
            if (items.size() != 1) {
                showChooseOneItemPopup();
                return;
            }
            onEditButtonAction(event, items.getFirst());
        });
        deleteButton.setOnAction(event -> {
            final var items = table.getSelectionModel().getSelectedItems();
            if (items.size() != 1) {
                showChooseOneItemPopup();
                return;
            }
            onDeleteButtonAction(event, items.getFirst());
        });
        filterButton.setOnAction(this::onFilterButtonAction);
        reportButton.setOnAction(event -> {
            if (getItems().isEmpty()) {
                popupHandler.createNoItemsPopupAndShow();
                return;
            }
            onReportButtonAction(event, table.getSelectionModel().getSelectedItems());
        });
    }

    protected abstract void onAddButtonAction(ActionEvent event);
    protected abstract void onEditButtonAction(ActionEvent event, T item);
    protected abstract void onDeleteButtonAction(ActionEvent event, T item);
    protected abstract void onFilterButtonAction(ActionEvent event);
    protected abstract void onReportButtonAction(ActionEvent event, List<T> items);

    protected final ExcelExporter excelExporter;
    protected final PopupHandler popupHandler;
    private final Pane rightPanel;
    private final Pane leftPanel;
    private final TableView<T> table;
    private final HBox buttonContainer;
    private final Button editButton;
    private final Button deleteButton;
    private final Button addButton;
    private final Button filterButton;
    private final Button reportButton;
    private final Pane bottomPanel;
    private final Node root;

    protected final void addButton(Button button) {
        HBox.setMargin(button, new Insets(5));
        buttonContainer.getChildren().add(button);
    }

    protected final List<T> getSelectedItems() {
        return table.getSelectionModel().getSelectedItems();
    }

    protected final List<T> getItems() {
        return requireNonNullElse(table.getItems(), List.of());
    }

    protected final void setItems(List<T> items) {
        Platform.runLater(() -> table.setItems(FXCollections.observableList(items)));
    }

    protected final void clearLeftPanel() {
        Platform.runLater(() -> leftPanel.getChildren().clear());
    }

    protected final void setLeftPanelContentView(Node node) {
        Platform.runLater(() -> ViewUtils.resetChildren(leftPanel, node));
    }

    protected final void clearRightPanel() {
        Platform.runLater(() -> rightPanel.getChildren().clear());
    }

    protected final void setRightPanelContentView(Node node) {
        Platform.runLater(() -> ViewUtils.resetChildren(rightPanel, node));
    }

    protected final void clearBottomPanel() {
        Platform.runLater(() -> bottomPanel.getChildren().clear());
    }

    protected final void setBottomPanelContentView(Node node) {
        Platform.runLater(() -> ViewUtils.resetChildren(bottomPanel, node));
    }

    protected final void showServerFailurePopup() {
        Platform.runLater(popupHandler::createCantConnectToServerPopupAndShow);
    }

    protected final void showChooseOneItemPopup() {
        Platform.runLater(popupHandler::createPleaseChooseOneItemPopupAndShow);
    }

    @Override
    public final Node getRoot() {
        return root;
    }

}
