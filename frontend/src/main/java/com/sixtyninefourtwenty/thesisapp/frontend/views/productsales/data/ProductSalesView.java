package com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.data;

import com.sixtyninefourtwenty.thesisapp.common.data.Customer;
import com.sixtyninefourtwenty.thesisapp.common.data.Product;
import com.sixtyninefourtwenty.thesisapp.common.data.ProductSale;
import com.sixtyninefourtwenty.thesisapp.frontend.TableColumns;
import com.sixtyninefourtwenty.thesisapp.frontend.Utils;
import com.sixtyninefourtwenty.thesisapp.frontend.exporter.ExcelExporter;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.CustomerRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.ProductRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.*;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.addedit.ProductSaleAddEditView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.filter.ProductSalesFilterView;
import com.sixtyninefourtwenty.thesisapp.frontend.views.productsales.filter.ProductSalesFilterViewModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import org.jspecify.annotations.NonNull;
import org.jspecify.annotations.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ProductSalesView extends CommonDataView<ProductSale> {

    private final ProductSalesViewModel productSalesViewModel;
    private final ProductSalesFilterViewModel productSalesFilterViewModel;
    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;

    public ProductSalesView(
            ExcelExporter excelExporter,
            List<ProductSale> items,
            PopupHandler popupHandler,
            ProductSalesViewModel productSalesViewModel,
            ProductSalesFilterViewModel productSalesFilterViewModel,
            ProductRepository productRepository,
            CustomerRepository customerRepository
    ) {
        super(
                excelExporter,
                popupHandler,
                TableColumns.forProductSale(),
                items
        );
        this.productSalesViewModel = productSalesViewModel;
        this.productSalesFilterViewModel = productSalesFilterViewModel;
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
        productSalesViewModel.addDataListener(c -> Platform.runLater(() -> setItems(Utils.cast(c.getList()))));
    }

    @Override
    protected void onAddButtonAction(ActionEvent event) {
        showProductSaleAddEditView(null, productSale -> {
            productSalesViewModel.tryCreate(
                    productSale,
                    s -> {},
                    msg -> {
                        Platform.runLater(() -> {
                            final var popup = popupHandler.createPopup();
                            popup.setContentView(GenericErrorView.create(
                                    msg, popup::close
                            ).getRoot());
                            popup.show();
                        });
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    @Override
    protected void onEditButtonAction(ActionEvent event, ProductSale item) {
        showProductSaleAddEditView(item, productSale -> {
            productSalesViewModel.tryFullUpdate(
                    productSale,
                    productSale.id(),
                    s -> {},
                    msg -> {
                        Platform.runLater(() -> {
                            final var popup = popupHandler.createPopup();
                            popup.setContentView(GenericErrorView.create(
                                    msg, popup::close
                            ).getRoot());
                            popup.show();
                        });
                    },
                    () -> Platform.runLater(this::clearBottomPanel),
                    this::showServerFailurePopup
            );
        });
    }

    private void showProductSaleAddEditView(
            @Nullable ProductSale productSale,
            Consumer<ProductSale> onOkButtonClick
    ) {
        final var fxmlLoader = Utils.fxmlLoader("product_sale_add_edit");
        fxmlLoader.setControllerFactory(clazz -> new ProductSaleAddEditView(popupHandler,
                productSale,
                onOkButtonClick,
                this::clearBottomPanel,
                view -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forProduct(),
                                    (event, product) -> {
                                        view.setPickedProduct(product);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                },
                view -> {
                    customerRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Customer>> call, @NonNull Response<List<Customer>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forCustomer(),
                                    (event, customer) -> {
                                        view.setPickedCustomer(customer);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Customer>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        ));
        Utils.loadQuietly(fxmlLoader);
        setBottomPanelContentView(fxmlLoader.<ProductSaleAddEditView>getController().getRoot());
    }

    @Override
    protected void onDeleteButtonAction(ActionEvent event, ProductSale item) {
        final var popup = popupHandler.createPopup();
        popup.setContentView(SimpleMessageDialogView.deleteItem(
                () -> {
                    popup.close();
                    productSalesViewModel.delete(
                            item,
                            () -> {},
                            this::showServerFailurePopup
                    );
                },
                popup::close
        ).getRoot());
        popup.show();
    }

    @Override
    protected void onFilterButtonAction(ActionEvent event) {
        final var v = ProductSalesFilterView.create(
                productSalesFilterViewModel,
                filters -> {
                    productSalesViewModel.setFilters(filters);
                    clearLeftPanel();
                },
                this::clearLeftPanel,
                view -> {
                    productRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Product>> call, @NonNull Response<List<Product>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forProduct(),
                                    (event, product) -> {
                                        view.setPickedProduct(product);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                },
                view -> {
                    customerRepository.findAll().enqueue(new Callback<>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Customer>> call, @NonNull Response<List<Customer>> response) {
                            final var v = SingleChooserDialogView.create(
                                    requireNonNull(response.body()),
                                    TableColumns.forCustomer(),
                                    (event, customer) -> {
                                        view.setPickedCustomer(customer);
                                        clearRightPanel();
                                    },
                                    () -> clearRightPanel()
                            );
                            setRightPanelContentView(v.getRoot());
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Customer>> call, @NonNull Throwable throwable) {
                            showServerFailurePopup();
                        }
                    });
                }
        );
        setLeftPanelContentView(v.getRoot());
    }

    @Override
    protected void onReportButtonAction(ActionEvent event, List<ProductSale> items) {
        final var finalItems = items.isEmpty() ? getItems() : items;
        popupHandler.createPopupAndShow(popup -> new SimpleMessageDialogView(
                "Do you want to save a report of " + finalItems.size() + " items?",
                () -> {
                    popup.close();
                    try {
                        excelExporter.exportExcel(
                                "product_sales_" + System.currentTimeMillis(),
                                "ProductSales",
                                List.of("ID", "Product name", "Quantity", "Customer name", "Date"),
                                finalItems,
                                item -> {
                                    final var customer = item.customer();
                                    final var customerName = customer != null ? customer.name() : "N/A";
                                    return List.of(
                                            Long.toString(item.id()),
                                            item.product().name(),
                                            Integer.toString(item.productQuantity()),
                                            customerName,
                                            item.time().toLocalDate().toString()
                                    );
                                }
                        );
                    } catch (IOException e) {
                        popupHandler.createPopupAndShow(popup1 -> GenericErrorView.create(
                                "Can't create report.",
                                popup1::close
                        ).getRoot());
                    }
                },
                popup::close
        ).getRoot());
    }

}
