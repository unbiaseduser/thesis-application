package com.sixtyninefourtwenty.thesisapp.frontend.views.materialimports.data;

import com.sixtyninefourtwenty.thesisapp.common.data.MaterialImport;
import com.sixtyninefourtwenty.thesisapp.common.data.Result;
import com.sixtyninefourtwenty.thesisapp.frontend.repository.MaterialImportRepository;
import com.sixtyninefourtwenty.thesisapp.frontend.views.CommonViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class MaterialImportsViewModel extends CommonViewModel<MaterialImport> {

    private final MaterialImportRepository materialImportRepository;

    public MaterialImportsViewModel(MaterialImportRepository materialImportRepository, List<MaterialImport> items) {
        super(items);
        this.materialImportRepository = materialImportRepository;
    }

    public void tryCreate(
            MaterialImport materialImport,
            Consumer<MaterialImport> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        materialImportRepository.tryCreate(materialImport).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<MaterialImport>> call, Response<Result<MaterialImport>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<MaterialImport> s -> {
                        final var newItem = s.data();
                        preFilteredList.add(newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<MaterialImport> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<MaterialImport>> call, Throwable throwable) {
                onFailure.run();
            }
        });
    }

    public void tryFullUpdate(
            MaterialImport materialImport,
            long id,
            Consumer<MaterialImport> onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        materialImportRepository.tryFullUpdate(materialImport, id).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<MaterialImport>> call, Response<Result<MaterialImport>> response) {
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<MaterialImport> s -> {
                        final var newItem = s.data();
                        preFilteredList.replaceAll(m -> m.id() != newItem.id() ? m : newItem);
                        onSuccessResponse.accept(newItem);
                    }
                    case Result.Rejection<MaterialImport> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<MaterialImport>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

    public void tryDelete(
            MaterialImport materialImport,
            Runnable onSuccessResponse,
            Consumer<String> onFailureResponse,
            Runnable onCommonResponse,
            Runnable onFailure
    ) {
        materialImportRepository.tryDelete(materialImport.id()).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<Result<MaterialImport>> call, Response<Result<MaterialImport>> response) {
                System.out.println(response.code());
                final var result = requireNonNull(response.body());
                switch (result) {
                    case Result.Success<MaterialImport> s -> {
                        preFilteredList.remove(materialImport);
                        onSuccessResponse.run();
                    }
                    case Result.Rejection<MaterialImport> r -> onFailureResponse.accept(r.message());
                }
                onCommonResponse.run();
            }

            @Override
            public void onFailure(Call<Result<MaterialImport>> call, Throwable t) {
                onFailure.run();
            }
        });
    }

}
